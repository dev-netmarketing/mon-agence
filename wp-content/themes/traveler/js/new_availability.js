jQuery(document).ready(function ($) {

    // on change availibility
    $("#st-field-sf_disponiblite").change(function(){
        if(this.value == "unavailable"){
            $(".sf_periode_bloc :input:not([type=hidden])").prop("disabled", true);
            $("#st-field-sf_saison").prop("disabled", false);
            $("#sf_periode").prop("disabled", false);

            $(this).prop("disabled", false);
            $(".add_periode").prop("disabled", false);
            $('input[name="periode_id"]').prop("disabled", false);
            $('input[name="calendar_post_id"]').prop("disabled", false);

        } else {
            $(".sf_periode_bloc :input").prop("disabled", false);
        }
      });
      
    // on change periode input
    function setInputDates(start, end) {
        $('#calendar_check_in').val(start.format('DD/MM/YYYY'));
        $('#calendar_check_out').val(end.format('DD/MM/YYYY'));
    }

    function clearPeriodeForm(){
        $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
        $(':checkbox, :radio').prop('checked', false);
    }

    var t = $(this);
    var dateToday = new Date("MM/DD/YYYY");
    var periodeDateInput = t.find('#sf_periode');
    periodeDateInput.val('');
    var locale_daterangepicker = {
        direction: 'ltr',
        applyLabel: 'Apply',
        cancelLabel: 'Cancel',
        daysOfWeek: ['LU', 'MA', 'ME', 'JE', 'VE', 'SA','DI'],
        monthNames: ['Janvier', 'Févrie', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        firstDay: 1,
        today: 'Today',
        format: 'DD/MM/YYYY'
    }; 
    var DateRangeOptions = {
        singleDatePicker: false,
        autoApply: true,
        disabledPast: true,
        widthSingle: 500,
        dateFormat: 'DD/MM/YYYY',
        format: 'DD/MM/YYYY',
        showDropdowns: true,
        timePicker: false,
        locale: locale_daterangepicker, 
    };

    periodeDateInput.daterangepicker(DateRangeOptions, setInputDates);

    $('input[name="sf_periode"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });
  
    $('input[name="sf_periode"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    /** 
     * NOTE handling the new feature adding periods to database
     */
    $( ".add_periode" ).click(function(e){
        $(".sf_periode_bloc").addClass("loading__"); 
        e.preventDefault();
        var PeriodeFormData = $('input, select', '.calendar-wrapper-inner').serializeArray();
        var flag_submit = !1;
        var accommodation = $('input[name ="sf_pension_accommodation[]"]').val();
        PeriodeFormData.push({
            name:   'action',
            value:  'st_add_custom_price_hotel'
        });
        // console.log('PeriodeFormData', PeriodeFormData);
        
        if (flag_submit) return !1;
        flag_submit = !0;
        if(accommodation != ''){
            $.post(
                ajaxurl, 
                PeriodeFormData, 
                function(data, status) {
                    console.log('add_periode click');
                    flag_submit = !1
            }, 'json')
            .done(function() {
                $(".sf_periode_bloc").removeClass("loading__"); 
                clearPeriodeForm();
            })
            .fail(function() {
                alert( "Erreur lors de l'insertion, merci de ré-essayer" );
            })
            .always(function() {
                clearPeriodeForm();
                $(".sf_periode_bloc").removeClass("loading__"); 
            });
        } else {
            alert('Merci de ré-essayer!');
            $(".sf_periode_bloc").removeClass("loading__"); 
        }
        
        // This return prevents the submit event to refresh the page.
        return false;

    })

})