jQuery(document).ready(function ($) {
    // on change availibility
    $("#st-field-sf_disponiblite_mod").change(function(){
        if(this.value == "unavailable"){
            $(".sf_periode_bloc_mod :input:not([type=hidden])").prop("disabled", true);
            $("#st-field-sf_saison_mod").prop("disabled", false);
            $("#sf_periode_mod").prop("disabled", false);

            $(this).prop("disabled", false);
            $(".add_periode").prop("disabled", false);
            $('input[name="periode_id_mod"]').prop("disabled", false);
            $('input[name="calendar_post_id_mod"]').prop("disabled", false);

        } else {
            $(".sf_periode_bloc_mod :input").prop("disabled", false);
        }
      });
      
    // on change periode input
    function setInputDates(start, end) {
        $('#calendar_check_in_mod').val(start.format('DD/MM/YYYY'));
        $('#calendar_check_out_mod').val(end.format('DD/MM/YYYY'));
    }

    function clearPeriodeForm(){
        $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
        $(':checkbox, :radio').prop('checked', false);
    }

    var t = $(this);
    var dateToday = new Date("MM/DD/YYYY");
    var periodeDateInput = t.find('#sf_periode_mod');
    periodeDateInput.val('');
    var locale_daterangepicker = {
        direction: 'ltr',
        applyLabel: 'Apply',
        cancelLabel: 'Cancel',
        daysOfWeek: ['LU', 'MA', 'ME', 'JE', 'VE', 'SA','DI'],
        monthNames: ['Janvier', 'Févrie', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        firstDay: 1,
        today: 'Today',
        format: 'DD/MM/YYYY'
    }; 
    var DateRangeOptions = {
        singleDatePicker: false,
        autoApply: true,
        disabledPast: true,
        widthSingle: 500,
        dateFormat: 'DD/MM/YYYY',
        format: 'DD/MM/YYYY',
        showDropdowns: true,
        timePicker: false,
        locale: locale_daterangepicker, 
    };

    periodeDateInput.daterangepicker(DateRangeOptions, setInputDates);

    $('input[name="sf_periode_mod"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });
  
    $('input[name="sf_periode_mod"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    /** 
     * NOTE handling the new feature adding periods to database
     */
    
})