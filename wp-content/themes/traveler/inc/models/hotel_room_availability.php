<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 19/04/2018
 * Time: 13:53 CH
 */
class ST_Hotel_Room_Availability extends ST_Model
{
    protected $table_version='1.2';
    protected $table_name='st_room_availability';

    protected static $_inst;

    public function __construct()
    {
        /* $this->columns=[
            'id'           => [
                'type'           => 'bigint',
                'length'         => 9,
                'AUTO_INCREMENT' => TRUE
            ],
            'post_id'      => [
                'type' => 'INT',
                'UNIQUE'=>true
            ],
            'check_in'     => [
                'type'   => 'INT',
                'length' => 11,
                'UNIQUE'=>true
            ],
            'check_out'    => [
                'type'   => 'INT',
                'length' => 11
            ],
            'number'       => [
                'type'   => 'varchar',
                'length' => 255
            ],
            'post_type'       => [
	            'type'   => 'varchar',
	            'length' => 255
            ],
            'price'        => [
                'type'   => 'varchar',
                'length' => 255
            ],
            'status'       => [
                'type'   => 'varchar',
                'length' => 255
            ],
            'priority'     => [
                'type' => 'INT'
            ],
            'number_booked' => [
                'type' => 'INT',
                'length' => 11,
                'default' => 0
            ],
            'parent_id' => [
                'type' => 'bigint',
                'length' => 9
            ],
            'allow_full_day' => [
                'type' => 'varchar',
                'length' => 10
            ],
            'number_end' => [
                'type' => 'INT',
                'length' => 11
            ],
            'booking_period' => [
                'type' => 'INT',
                'length' => 11
            ],
            'is_base' => [
                'type' => 'INT',
                'length' => 2
            ],
            'adult_number'=>[
                'type' => 'INT',
                'length' => 11
            ],
            'child_number'=>[
                'type' => 'INT',
                'length' => 11
            ],
        ]; */
        $this->columns=[
            'id'           => [
                'type'           => 'bigint',
                'length'         => 20,
                'AUTO_INCREMENT' => TRUE
            ],
            'post_id'      => [
                'type' => 'INT',
                'UNIQUE'=>true
            ],
            'periode_id'      => [
                'type' => 'INT',
            ],
            'periode_from'     => [
                'type'   => 'INT',
                'length' => 11,
            ],
            'periode_to'    => [
                'type'   => 'INT',
                'length' => 11
            ],
            'saison'       => [
                'type'   => 'varchar',
                'length' => 50
            ],
            'post_type'       => [
	            'type'   => 'varchar',
	            'length' => 250
            ],
            'status'       => [
                'type'   => 'varchar',
                'length' => 250
            ],
            'marge'    => [
                'type'   => 'INT',
                'length' => 10
            ],
            'accommodation'    => [
                'type'   => 'longtext'
            ],
            'discount'    => [
                'type'   => 'INT',
                'length' => 11
            ],
            'min_age_1'    => [
                'type'   => 'varchar',
                'length' => 250
            ],
            'max_age_1' => [
                'type'   => 'varchar',
                'length' => 250
            ],
            'min_age_2' => [
                'type'   => 'varchar',
                'length' => 250
            ],
            'max_age_2' => [
                'type'   => 'varchar',
                'length' => 250
            ],
            'adult_enfant' => [
                'type' => 'longtext'
            ],
            'retrocession' => [
                'type' => 'INT',
                'length' => 10
            ],
            'min_stay' => [
                'type' => 'INT',
                'length' => 10
            ],
            'prix_single'=>[
                'type'   => 'varchar',
                'length' => 250
            ],
            'prix_3_lits'=>[
                'type'   => 'varchar',
                'length' => 250
            ],
            'extra_price'=>[
                'type'   => 'longtext'
            ],
            'stock'=>[
                'type'   => 'longtext'
            ],
        ];
        /* $this->columns = array_merge( $this->columns, [
            'adult_price' => [
                'type'   => 'varchar',
                'length' => 255
            ],
            'saison' => [
                'type'   => 'varchar',
                'length' => 100
            ],
            'groupe_count' => [
                'type'   => 'INT',
                'length' => 10
            ],
            'groupe_id' => [
                'type'   => 'varchar',
                'length' => 50
            ]
        ]); */
        parent::__construct();
    }

    public function insertOrUpdate($data)
    {
        $data = wp_parse_args(
            $data,
            array(
                'post_id'       => '',
                'periode_id'    => '',
                'periode_from'  => '',
                'periode_to'    => '',
                'saison'        => '',
                'post_type'     => '',
                'status'        => '',
                'marge'         => '',
                'accommodation'	=> '',	
                'discount'      => '',
                'msg_promotion' => '',
                'min_age_1'     => '',
                'max_age_1'     => '',
                'min_age_2'     => '',
                'max_age_2'     => '',
                'adult_enfant'  => '',
                'retrocession'  => '',
                'min_stay'      => '',
                'prix_single'   => '',
                'prix_3_lits'   => '',
                'extra_price'   => '',
                'stock'         => ''
            )
        );
		
        /* $where = [
            'post_id'           => $data['post_id'],
            'periode_from'      => $data['periode_from'],
			
        ];
        $check=$this->where($where)->get(1)->row();
        if($check) {
                unset($data['post_id']);
                unset($data['periode_from']);
            return $this->where($where)->update($data);
        } else {

            return $this->insert($data);
        } */
		 
    	return $this->insert($data);
    }
    public static function inst()
    {
        if(!self::$_inst) self::$_inst=new self();
        return self::$_inst;
    }
}

ST_Hotel_Room_Availability::inst();
