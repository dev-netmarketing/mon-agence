<?php
/**
 * @package    WordPress
 * @subpackage Traveler
 * @since      1.0
 *
 * Class STHotel
 *
 * Created by ShineTheme
 *
 */
if (!class_exists('STHotel')) {
    class STHotel extends TravelerObject
    {
        static $_inst;
        static $_instance;
        //Current Hotel ID
        private $hotel_id;
        protected $orderby;
        protected $post_type = 'st_hotel';
        protected $template_folder = 'hotel';
        function __construct($hotel_id = false)
        {
            $this->hotel_id = $hotel_id;
            $this->orderby = [
                'new' => [
                    'key' => 'new',
                    'name' => __('New', ST_TEXTDOMAIN)
                ],
                'price_asc' => [
                    'key' => 'price_asc',
                    'name' => __('Price ', ST_TEXTDOMAIN) . ' (<i class="fa fa-long-arrow-up"></i>)'
                ],
                'price_desc' => [
                    'key' => 'price_desc',
                    'name' => __('Price ', ST_TEXTDOMAIN) . ' (<i class="fa fa-long-arrow-down"></i>)'
                ],
                'name_asc' => [
                    'key' => 'name_asc',
                    'name' => __('Name (A-Z)', ST_TEXTDOMAIN)
                ],
                'name_desc' => [
                    'key' => 'name_desc',
                    'name' => __('Name (Z-A)', ST_TEXTDOMAIN)
                ],
            ];
        }
        /**
         * @return array
         */
        public function getOrderby()
        {
            return $this->orderby;
        }
        /**
         *
         *
         * @update 1.1.3
         * */
        function init()
        {
            if (!$this->is_available()) return;
            parent::init();
            //Filter the search hotel
            //custom search hotel template
            add_filter('template_include', [$this, 'choose_search_template']);
            //Sidebar Pos for SEARCH
            add_filter('st_hotel_sidebar', [$this, 'change_sidebar']);
            //add Widget Area
            add_action('widgets_init', [$this, 'add_sidebar']);
            //Create Hotel Booking Link
            add_action('wp', [$this, 'hotel_add_to_cart'], 20);
            // Change hotel review arg
            add_filter('st_hotel_wp_review_form_args', [$this, 'comment_args'], 10, 2);
            //Save Hotel Review Stats
            add_action('comment_post', [$this, 'save_review_stats']);
            //Reduce total stats of posts after comment_delete
            add_action('delete_comment', [$this, 'save_post_review_stats']);
            //Filter change layout of hotel detail if choose in metabox
            add_filter('st_hotel_detail_layout', [$this, 'custom_hotel_layout']);
            add_action('wp_enqueue_scripts', [$this, 'add_localize']);
            add_action('wp_ajax_ajax_search_room', [$this, 'ajax_search_room']);
            add_action('wp_ajax_nopriv_ajax_search_room', [$this, 'ajax_search_room']);
            add_filter('st_real_comment_post_id', [$this, '_change_comment_post_id']);
            add_filter('st_search_preload_page', [$this, '_change_preload_search_title']);
            add_filter('st_checkout_form_validate', [$this, '_check_booking_period']);
            add_filter('st_st_hotel_search_result_link', [$this, '_change_search_result_link'], 10, 2);
            // Woocommerce cart item information
            add_action('st_wc_cart_item_information_st_hotel', [$this, '_show_wc_cart_item_information']);
            add_action('st_wc_cart_item_information_btn_st_hotel', [$this, '_show_wc_cart_item_information_btn']);
            add_action('st_before_cart_item_st_hotel', [$this, '_show_wc_cart_post_type_icon']);
            add_action('wp_ajax_st_fetch_inventory', [$this, 'st_fetch_inventory']);
            add_action('wp_ajax_add_price_inventory', [$this, 'add_price_inventory_hotels']);
            //xsearch Load post hotel filter ajax
            add_action('wp_ajax_st_filter_hotel_ajax', [$this, 'st_filter_hotel_ajax']);
            add_action('wp_ajax_nopriv_st_filter_hotel_ajax', [$this, 'st_filter_hotel_ajax']);
            add_action('wp_ajax_st_filter_hotel_map', [$this, '__getMapFilterAjax']);
            add_action('wp_ajax_nopriv_st_filter_hotel_map', [$this, '__getMapFilterAjax']);
            add_action('wp_ajax_st_add_room_number_inventory', [$this, 'st_add_room_number_inventory']);
            //xsearch Load post hotel filter ajax location
            add_action('wp_ajax_st_filter_hotel_ajax_location', [$this, 'st_filter_hotel_ajax_location']);
            add_action('wp_ajax_nopriv_st_filter_hotel_ajax_location', [$this, 'st_filter_hotel_ajax_location']);
            // hotel booking
            add_action('wp_ajax_hotel_add_to_cart', [$this, 'ajax_hotel_add_to_cart']);
            add_action('wp_ajax_nopriv_hotel_add_to_cart', [$this, 'ajax_hotel_add_to_cart']);
            add_action('wp_ajax_nopriv_st_load_more_service_by_id', [$this, 'st_load_more_service_by_id']);
            add_action('wp_ajax_st_load_more_service_by_id', [$this, 'st_load_more_service_by_id']);
        }
        public function st_load_more_service_by_id()
        {
            $posts_per_page = STInput::request('posts_per_page');
            $st_paged = STInput::request('paged') + 1;
            $data_id_service = STInput::request('data_id_service');
            $index = STInput::request('index');
            $check_all = STInput::request('check_all');
            $args = array(
                'post_type' => 'st_hotel',
                'posts_per_page' => (int)$posts_per_page,
                'paged' => $st_paged,
                'post__in' => explode(',', $data_id_service),
            );
            if (!empty($select_category)) {
                $st_list_cat = explode(",", $select_category);
                if ($check_all === 'true') {
                    $st_list_cat = $st_list_cat;
                } else {
                    $st_list_cat = $select_category;
                }
            }
            $return = array(
                'status' => 0
            );
            $return['paged'] = $st_paged;
            $return['html'] = "";
            global $post;
            $old_post = $post;
            $st_pride_query = new WP_Query($args);
            while ($st_pride_query->have_posts()):
                $st_pride_query->the_post();
                $return['html'] .= st()->load_template('layouts/modern/hotel/loop/grid', '');
            endwhile;
            wp_reset_postdata();
            $post = $old_post;
            $return['status'] = $count_item;
            $return['index'] = $index;
            echo json_encode($return);
            die();
        }
        /**
         * NOTE main search function
         */
        public function setQueryHotelSearch()
        {
            $page_number = STInput::get('page');
            global $wp_query, $st_search_query;
            //dd($st_search_query, "st_search_query");
            $current_lang = TravelHelper::current_lang();
            $main_lang = TravelHelper::primary_lang();
            if (TravelHelper::is_wpml()) {
                global $sitepress;
                $sitepress->switch_lang($main_lang, true);
            }
            $hotel = $this;
            $hotel->alter_search_query();
            set_query_var('paged', $page_number);
            $paged = $page_number;
            $args = [
                'post_type'     => 'st_hotel',
                's'             => '',
                'post_status'   => ['publish'],
                'paged'         => $paged
            ];
            query_posts($args);
            $st_search_query = $wp_query;
            $hotel->remove_alter_search_query();
            if (TravelHelper::is_wpml()) {
                global $sitepress;
                $sitepress->switch_lang($current_lang, true);
            }
        }
        public function __getMapFilterAjax()
        {
            //$this->checkSecurity();
            global $st_search_query;
            $this->setQueryHotelSearch();
            $query = $st_search_query;
            $map_lat_center = 0;
            $map_lng_center = 0;
            if (STInput::request('location_id')) {
                $map_lat_center = get_post_meta(STInput::request('location_id'), 'map_lat', true);
                $map_lng_center = get_post_meta(STInput::request('location_id'), 'map_lng', true);
            }
            $data_map = [];
            $stt = 0;
            if ($query->have_posts()) {
                while ($query->have_posts()) {
                    $query->the_post();
                    $map_lat = get_post_meta(get_the_ID(), 'map_lat', true);
                    $map_lng = get_post_meta(get_the_ID(), 'map_lng', true);
                    if (!empty($map_lat) and !empty($map_lng)) {
                        if (empty($map_lat_center)) $map_lat_center = $map_lat;
                        if (empty($map_lng_center)) $map_lng_center = $map_lng;
                        $post_type = get_post_type();
                        $data_map[$stt]['id'] = get_the_ID();
                        $data_map[$stt]['name'] = get_the_title();
                        $data_map[$stt]['post_type'] = $post_type;
                        $data_map[$stt]['lat'] = $map_lat;
                        $data_map[$stt]['lng'] = $map_lng;
                        $post_type_name = get_post_type_object($post_type);
                        $post_type_name->label;
                        $data_map[$stt]['content_html'] = preg_replace('/^\s+|\n|\r|\s+$/m', '', st()->load_template('layouts/modern/hotel/elements/content/map-popup'));
                        $data_map[$stt]['content_adv_html'] = preg_replace('/^\s+|\n|\r|\s+$/m', '', st()->load_template('vc-elements/st-list-map/loop-adv/hotel', false, ['post_type' => $post_type_name->label]));
                        $stt++;
                    }
                }
            }
            wp_reset_query();
            wp_reset_postdata();
            $map_icon = st()->get_option('st_hotel_icon_map_marker', '');
            if (empty($map_icon))
                $map_icon = get_template_directory_uri() . '/v2/images/markers/ico_mapker_hotel.png';
            $data_tmp = [
                'data_map' => $data_map,
                'map_lat_center' => $map_lat_center,
                'map_lng_center' => $map_lng_center,
                'map_icon' => $map_icon
            ];
            echo json_encode($data_tmp);
            die;
        }
        public function st_add_room_number_inventory()
        {
            $room_id = STInput::post('room_id', '');
            $number_room = STInput::post('number_room', '');
            $current_user = wp_get_current_user();
            $roles = $current_user->roles;
            $role = array_shift($roles);
            if ($role != 'administrator' && $role != 'partner') {
                $return = [
                    'status' => 0,
                    'message' => esc_html__('Can not set number for room', ST_TEXTDOMAIN)
                ];
                echo json_encode($return);
                die;
            } else {
                if ($role == 'partner') {
                    $current_user_id = $current_user->ID;
                    $post = get_post($room_id);
                    $authid = $post->post_author;
                    if ($current_user_id != $authid) {
                        $return = [
                            'status' => 0,
                            'message' => esc_html__('Can not set number for room', ST_TEXTDOMAIN)
                        ];
                        echo json_encode($return);
                        die;
                    }
                }
            }
            if (get_post_type($room_id) != 'hotel_room') {
                $return = [
                    'status' => 0,
                    'message' => esc_html__('Can not set number for room', ST_TEXTDOMAIN)
                ];
                echo json_encode($return);
                die;
            }
            if ($room_id < 0 || $room_id == '' || !is_numeric($room_id)) {
                $return = [
                    'status' => 0,
                    'message' => esc_html__('Room is invalid!', ST_TEXTDOMAIN),
                ];
                echo json_encode($return);
                die;
            }
            if ($number_room < 0 || $number_room == '' || !is_numeric($number_room)) {
                $return = [
                    'status' => 0,
                    'message' => esc_html__('Number of room is invalid!', ST_TEXTDOMAIN),
                ];
                echo json_encode($return);
                die;
            }
            $res = update_post_meta($room_id, 'number_room', $number_room);
            //Update number room in available table
            $update_number_room = ST_Hotel_Room_Availability::inst()
                ->where('post_id', $room_id)
                ->update(['number' => $number_room]);
            if ($res && $update_number_room > 0) {
                $return = [
                    'status' => 1,
                    'message' => esc_html__('Update success!', ST_TEXTDOMAIN),
                ];
                echo json_encode($return);
                die;
            } else {
                $return = [
                    'status' => 0,
                    'message' => esc_html__('Can not set number for room', ST_TEXTDOMAIN)
                ];
                echo json_encode($return);
                die;
            }
        }
        public function removeSearchServiceLocationByAuthor($query)
        {
            $query->set('author', '');
            return $query;
        }
        public function st_filter_hotel_ajax_location()
        {
            $page_number = STInput::get('page');
            $posts_per_page = STInput::get('posts_per_page');
            $id_location = STInput::get('id_location');
            $_REQUEST['location_id'] = STInput::get('id_location');
            global $wp_query, $st_search_query;
            add_filter('pre_get_posts', [$this, 'removeSearchServiceLocationByAuthor']);
            $this->setQueryHotelSearch();
            add_filter('pre_get_posts', [$this, 'removeSearchServiceLocationByAuthor']);
            $query_service = $st_search_query;
            ob_start();
            ?>
            <div class="row row-wrapper">
                <?php if ($query_service->have_posts()) {
                    while ($query_service->have_posts()) {
                        $query_service->the_post();
                        echo st()->load_template('layouts/modern/location/elements/loop/hotel/grid', '');
                    }
                } else {
                    echo '<div class="col-xs-12">';
                    echo st()->load_template('layouts/modern/hotel/elements/none');
                    echo '</div>';
                }
                wp_reset_postdata(); ?>
            </div>
            <?php
            $ajax_filter_content = ob_get_contents();
            ob_clean();
            ob_end_flush();
            ob_start();
            TravelHelper::paging(false, false); ?>
            <span class="count-string">
                    <?php
                    if ($query_service->found_posts):
                        $posts_per_page = $posts_per_page;
                        if (!$page_number) {
                            $page = 1;
                        } else {
                            $page = $page_number;
                        }
                        $last = (int)$posts_per_page * (int)($page);
                        if ($last > $query_service->found_posts) $last = $query_service->found_posts;
                        echo sprintf(__('%d - %d sur %d ', ST_TEXTDOMAIN), (int)$posts_per_page * ($page - 1) + 1, $last, $query_service->found_posts);
                        echo ($query_service->found_posts == 1) ? __('hôtel', ST_TEXTDOMAIN) : __('hôtels', ST_TEXTDOMAIN);
                    endif;
                    ?>
                </span>
            <?php
            $ajax_filter_pag = ob_get_contents();
            ob_clean();
            ob_end_flush();
            $result = [
                'content' => $ajax_filter_content,
                'pag' => $ajax_filter_pag,
                'page' => $page_number,
            ];
            wp_reset_query();
            wp_reset_postdata();
            echo json_encode($result);
            die;
        }
        public function st_filter_hotel_ajax()
        {
            //$this->checkSecurity();
            $page_number = STInput::get('page');
            $style = STInput::get('layout');
            $format = STInput::get('format');
            $is_popup_map = STInput::get('is_popup_map');
            $half_map_show = STInput::get('half_map_show');
            $fullwidth = STInput::get('fullwidth');
            if (empty($half_map_show))
                $half_map_show = 'yes';
            $popup_map = '';
            if ($is_popup_map) {
                $popup_map = '<div class="row list-style">';
            }
            if (!in_array($format, ['normal', 'halfmap', 'popupmap']))
                $format = 'normal';
            global $wp_query, $st_search_query;
            $this->setQueryHotelSearch();
            $query = $st_search_query;
            //dd($query, "query alter");
            // Map
            $map_lat_center = 0;
            $map_lng_center = 0;
            if (STInput::request('location_id')) {
                $map_lat_center = get_post_meta(STInput::request('location_id'), 'map_lat', true);
                $map_lng_center = get_post_meta(STInput::request('location_id'), 'map_lng', true);
            }
            $data_map = [];
            $stt = 0;
            //End map
            ob_start();
            echo st()->load_template('layouts/modern/common/loader', 'content');
            if (!isset($style) || empty($style)) $style = 'grid';
            switch ($format) {
                case 'halfmap':
                    echo ($style == 'grid') ? '<div class="row">' : '<div class="row list-style">';
                    break;
                default:
                    echo ($style == 'grid') ? '<div class="row row-wrapper">' : '<div class="style-list">';
                    break;
            }
            if ($query->have_posts()) {
                
                while ($query->have_posts()) {
                    $query->the_post();
                    //dd(the_post(), 'post_filter_list');
                    if ($fullwidth) {
                        echo st()->load_template('layouts/modern/hotel/elements/loop/' . $format, $style, ['show_map' => $half_map_show, 'fullwidth' => true]);
                    } else {
                        echo st()->load_template('layouts/modern/hotel/elements/loop/' . $format, $style, ['show_map' => $half_map_show]);
                    }
                    if ($is_popup_map)
                        $popup_map .= st()->load_template('layouts/modern/hotel/elements/loop/popupmap');
                    //Map
                    $map_lat = get_post_meta(get_the_ID(), 'map_lat', true);
                    $map_lng = get_post_meta(get_the_ID(), 'map_lng', true);
                    if (!empty($map_lat) and !empty($map_lng)) {
                        if (empty($map_lat_center)) $map_lat_center = $map_lat;
                        if (empty($map_lng_center)) $map_lng_center = $map_lng;
                        $post_type = get_post_type();
                        $data_map[$stt]['id'] = get_the_ID();
                        $data_map[$stt]['name'] = get_the_title();
                        $data_map[$stt]['post_type'] = $post_type;
                        $data_map[$stt]['lat'] = $map_lat;
                        $data_map[$stt]['lng'] = $map_lng;
                        $post_type_name = get_post_type_object($post_type);
                        $post_type_name->label;
                        $data_map[$stt]['content_html'] = preg_replace('/^\s+|\n|\r|\s+$/m', '', st()->load_template('layouts/modern/hotel/elements/content/map-popup'));
                        $data_map[$stt]['content_adv_html'] = preg_replace('/^\s+|\n|\r|\s+$/m', '', st()->load_template('vc-elements/st-list-map/loop-adv/hotel', false, ['post_type' => $post_type_name->label]));
                        $stt++;
                    }
                    //End map
                }
            } else {
                if ($is_popup_map)
                    $popup_map .= '<div class="col-xs-12">' . st()->load_template('layouts/modern/hotel/elements/none') . '</div>';
                echo ($style == 'grid') ? '<div class="col-xs-12">' : '';
                echo st()->load_template('layouts/modern/hotel/elements/none');
                echo '</div>';
            }
            echo '</div>';
            $ajax_filter_content = ob_get_contents();
            ob_clean();
            ob_end_flush();
            if ($is_popup_map) {
                $popup_map .= '</div>';
            }
            ob_start();
            TravelHelper::paging(false, false, true); ?>
            <span class="count-string">
                    <?php
                    if (!empty($st_search_query)) {
                        $wp_query = $st_search_query;
                    }
                    if ($wp_query->found_posts):
                        $page = get_query_var('paged');
                        $posts_per_page = get_query_var('posts_per_page');
                        if (!$page) $page = 1;
                        $last = $posts_per_page * ($page);
                        if ($last > $wp_query->found_posts) $last = $wp_query->found_posts;
                        echo sprintf(__('%d - %d sur %d ', ST_TEXTDOMAIN), $posts_per_page * ($page - 1) + 1, $last, $wp_query->found_posts);
                        echo ($wp_query->found_posts == 1) ? __('hôtel', ST_TEXTDOMAIN) : __('hôtels', ST_TEXTDOMAIN);
                    endif;
                    ?>
                </span>
            <?php
            $ajax_filter_pag = ob_get_contents();
            ob_clean();
            ob_end_flush();
            $count = balanceTags($this->get_result_string()) . '<div id="btn-clear-filter" class="btn-clear-filter" style="display: none;">' . __('Clear filter', ST_TEXTDOMAIN) . '</div>';
            //Map
            $map_icon = st()->get_option('st_hotel_icon_map_marker', '');
            if (empty($map_icon))
                $map_icon = get_template_directory_uri() . '/v2/images/markers/ico_mapker_hotel.png';
            $data_tmp = [
                'data_map' => $data_map,
                'map_lat_center' => $map_lat_center,
                'map_lng_center' => $map_lng_center,
                'map_icon' => $map_icon
            ];
            //End map
            $result = [
                'content' => $ajax_filter_content,
                'pag' => $ajax_filter_pag,
                'count' => $count,
                'page' => $page_number,
                'content_popup' => $popup_map,
                'data_map' => $data_tmp
            ];
            wp_reset_query();
            wp_reset_postdata();
            echo json_encode($result);
            die;
        }
         static function st_fetch_inventory()
        {
            $post_id = STInput::post('post_id', '');
            if (get_post_type($post_id) == 'st_hotel') {
                $start = strtotime(STInput::post('start', ''));
                $end = strtotime(STInput::post('end', ''));
                if ($start > 0 && $end > 0) {
                    $args = [
                        'post_type' => 'hotel_room',
                        'posts_per_page' => -1,
                        'meta_query' => [
                            [
                                'key' => 'room_parent',
                                'value' => $post_id,
                                'compare' => '='
                            ]
                        ]
                    ];
                    if (!current_user_can('administrator')) {
                        $args['author'] = get_current_user_id();
                    }
                    $rooms = [];
                    $query = new WP_Query($args);
                    while ($query->have_posts()): $query->the_post();
                        $rooms[] = [
                            'id' => get_the_ID(),
                            'name' => get_the_title()
                        ];
                    endwhile;
                    wp_reset_postdata();
                    $datarooms = [];
                    if (!empty($rooms)) {
                        foreach ($rooms as $key => $value) {
                            $datarooms[] = $this->featch_dataroom($post_id, $value['id'], $value['name'], $start, $end);
                        }
                    }
                    echo json_encode([
                        'status' => 1,
                        'rooms' => $datarooms
                    ]);
                    die;
                }
            }
            echo json_encode([
                'status' => 0,
                'message' => __('Can not fetch data', ST_TEXTDOMAIN),
                'rooms' => ''
            ]);
            die;
        }
        function st_origin_id($post_id, $service_type = 'post')
        {
            if (function_exists('wpml_object_id_filter')) {
                global $sitepress;
                $a = wpml_object_id_filter($post_id, $service_type, true, $sitepress->get_default_language());
                return $a;
            } else {
                return $post_id;
            }
        }
        public function add_price_inventory_hotels()
        {
            $post_id = (int)STInput::post('post_id');
            $price = STInput::post('price');
            $status = STInput::post('status', 'available');
            $start = (float)STInput::post('start');
            $end = (float)STInput::post('end');
            $start /= 1000;
            $end /= 1000;
            $adult_price = STInput::post('adult_price');
            $child_price = STInput::post('child_price');
            $price_by_per_person = get_post_meta($post_id, 'price_by_per_person', true);
            $start = strtotime(date('Y-m-d', $start));
            $end = strtotime(date('Y-m-d', $end));
            if (get_post_type($post_id) != 'hotel_room') {
                echo json_encode([
                    'status' => 0,
                    'message' => esc_html__('Can not set price for this room', ST_TEXTDOMAIN)
                ]);
                die;
            }
            if ($price_by_per_person == 'on') {
                if (($status == 'available')
                    && ($adult_price == '' && $child_price == '') && (($adult_price == '' || !is_numeric($adult_price) || (float)$adult_price < 0)
                        || ($child_price == '' || !is_numeric($child_price) || (float)$child_price < 0))) {
                    echo json_encode([
                        'status' => 0,
                        'message' => esc_html__('Price is incorrect', ST_TEXTDOMAIN)
                    ]);
                    die;
                }
            } else {
                if (($status == 'available') && ($price == '' || !is_numeric($price) || (float)$price < 0)) {
                    echo json_encode([
                        'status' => 0,
                        'message' => esc_html__('Price is incorrect', ST_TEXTDOMAIN)
                    ]);
                    die;
                }
            }
            $price = (float)$price;
            $adult_price = floatval($adult_price);
            $child_price = floatval($child_price);
            $base_id = (int)$this->st_origin_id($post_id, 'hotel_room');
            $new_item = $this->inventory_save_data($post_id, $base_id, $start, $end, $price, $status, $adult_price, $child_price);
            if ($new_item > 0) {
                echo json_encode([
                    'status' => 1,
                    'message' => esc_html__('Successffully added', ST_TEXTDOMAIN)
                ]);
                die;
            } else {
                echo json_encode([
                    'status' => 0,
                    'message' => esc_html__('Getting an error when adding new item.', ST_TEXTDOMAIN)
                ]);
                die;
            }
        }
        public function inventory_save_data($post_id, $base_id, $check_in, $check_out, $price, $status, $adult_price = '', $child_price = '')
        {
            global $wpdb;
            $result = $this->get_availability($base_id, $check_in, $check_out);
            $number = get_post_meta($base_id, 'number_room', true);
            $parent_id = get_post_meta($base_id, 'room_parent', true);
            $booking_period = get_post_meta($parent_id, 'hotel_booking_period', true);
            $allow_full_day = get_post_meta($base_id, 'allow_full_day', true);
            $adult_number = get_post_meta($base_id, 'adult_number', true);
            $child_number = get_post_meta($base_id, 'children_number', true);
            $string_insert = '';
            $check_total_update = 0;
            if (!empty($result)) {
                if (!empty($check_in) && !empty($check_out)) {
                    $arr_to_insert = [];
                    for ($i = $check_in; $i <= $check_out; $i = strtotime('+1 day', $i)) {
                        $check_available = ST_Hotel_Room_Availability::inst()
                            ->where('post_id', $base_id)
                            ->where('check_in', $i)
                            ->get()->result();
                        if (!empty($check_available)) {
                            $check_update = ST_Hotel_Room_Availability::inst()
                                ->where('post_id', $post_id)
                                ->where('check_in', $i)
                                ->update([
                                    'price' => $price,
                                    'post_type' => 'hotel_room',
                                    'number' => $number,
                                    'parent_id' => $parent_id,
                                    'allow_full_day' => $allow_full_day,
                                    'booking_period' => $booking_period,
                                    'adult_number' => $adult_number,
                                    'child_number' => $child_number,
                                    'status' => $status,
                                    'adult_price' => $adult_price,
                                    'child_price' => $child_price,
                                ]);
                            $check_total_update += $check_update;
                        } else {
                            array_push($arr_to_insert, $i);
                        }
                    }
                    if (!empty($arr_to_insert)) {
                        foreach ($arr_to_insert as $kk => $vv) {
                            $string_insert .= $wpdb->prepare("(null, %s, %s, %d, %d, %d, %s, %d, %d, %s, %s,%s, %s, %s, %s, %s),", 'hotel_room', '0', $number, $parent_id, $booking_period, $allow_full_day, $adult_number, $child_number, $post_id, $vv, $vv, $price, 'available', $adult_price, $child_price);
                        }
                    }
                }
            } else {
                for ($i = $check_in; $i <= $check_out; $i = strtotime('+1 day', $i)) {
                    $string_insert .= $wpdb->prepare("(null, %s, %s, %d, %d, %d, %s, %d, %d, %s, %s,%s, %s, %s, %s, %s),", 'hotel_room', '0', $number, $parent_id, $booking_period, $allow_full_day, $adult_number, $child_number, $post_id, $i, $i, $price, 'available', $adult_price, $child_price);
                }
            }
            if (!empty($string_insert) || $check_total_update > 0) {
                if (!empty($string_insert)) {
                    $string_insert = substr($string_insert, 0, -1);
                    $sql = "INSERT INTO {$wpdb->prefix}st_room_availability (id, post_type, is_base, `number`, parent_id, booking_period, allow_full_day, adult_number, child_number, post_id,check_in,check_out,price, status, adult_price, child_price ) VALUES {$string_insert}";
                    $result = $wpdb->query($sql);
                    return $result;
                } else {
                    return $check_total_update;
                }
            } else {
                return 0;
            }
        }
        public function get_availability($base_id = '', $check_in = '', $check_out = '')
        {
            global $wpdb;
            $table = $wpdb->prefix . 'st_room_availability';
            $sql = "SELECT * FROM {$table} WHERE post_id = {$base_id} AND ( ( CAST( `check_in` AS UNSIGNED ) >= CAST( {$check_in} AS UNSIGNED) AND CAST( `check_in` AS UNSIGNED ) <= CAST( {$check_out} AS UNSIGNED ) ) OR ( CAST( `check_out` AS UNSIGNED ) >= CAST( {$check_in} AS UNSIGNED ) AND ( CAST( `check_out` AS UNSIGNED ) <= CAST( {$check_out} AS UNSIGNED ) ) ) ) ";
            $result = $wpdb->get_results($sql, ARRAY_A);
            $return = [];
            if (!empty($result)) {
                foreach ($result as $item) {
                    $item_array = [
                        'id' => $item['id'],
                        'post_id' => $item['post_id'],
                        'start' => date('Y-m-d', $item['check_in']),
                        'end' => date('Y-m-d', strtotime('+1 day', $item['check_out'])),
                        'price' => (float)$item['price'],
                        'price_text' => TravelHelper::format_money($item['price']),
                        'status' => $item['status'],
                        'adult_price' => floatval($item['adult_price']),
                        'child_price' => floatval($item['child_price']),
                    ];
                    $return[] = $item_array;
                }
            }
            return $return;
        }
        public function st_fetch_rooms($post_id)
        {
			     global $wpdb;
					$table = $wpdb->prefix . 'hotel_room';
					$table1 = $wpdb->prefix . 'link_hotel_room';
					$query = "  select adult_number
					from {$table} as tb
					JOIN {$table1} as tbb on tb.post_id=tbb.post_id and tbb.room_parent=".$post_id."
					"; 
                   $results = $wpdb->get_results($query);
				   $maxresult  = "";
				    foreach (max($results) as $value) {
						$maxresult = $value; 
					}
				   return $maxresult;
        }
        public function sf_fetch_periodes($post_id){
            global $wpdb;
            $table = $wpdb->prefix . 'st_room_availability';
            $query = $wpdb->prepare(
                    "SELECT MAX(periode_id) FROM {$table} AS t WHERE t.post_id = %d",
                    $post_id
            );
            $result = $wpdb->get_var($query);
            return $result;
        }
        public function sf_fetch_periodes_rows($post_id){
            global $wpdb;
            $table = $wpdb->prefix . 'st_room_availability';
            $query = $wpdb->prepare(
                    "SELECT * FROM {$table} AS t WHERE t.post_id = %d ORDER BY t.timestamp ASC",
                    $post_id
            );
            $result = $wpdb->get_results($query);
            return $result;
        }
        public function sf_print_periode_saison($saison) {
            switch ($saison) {
                case 'basse_saison':
                    return "Basse Saison";
                    break;
                case 'moyenne_saison':
                    return "Moyenne Saison";
                    break;
                case 'haute_saison':
                    return "Haute Saison";
                    break;
                default:
                    return "Basse Saison";
                    break;
            }
        }
        public function sf_print_status($status) {
            switch ($status) {
                case 'available':
                    return "Disponible";
                    break;
                case 'unavailable':
                    return "Complet";
                    break;
                default:
                    return "Disponible";
                    break;
            }
        }
        public function featch_dataroom($hotel_id, $post_id, $post_name, $start, $end)
        {
            $number_room = (int)get_post_meta($post_id, 'number_room', true);
            $allow_fullday = get_post_meta($hotel_id, 'allow_full_day', true);
            $base_price = (float)get_post_meta($post_id, 'price', true);
            $adult_price = floatval(get_post_meta($post_id, 'adult_price', true));
            $child_price = floatval(get_post_meta($post_id, 'child_price', true));
            $price_by_per_person = (get_post_meta($post_id, 'price_by_per_person', true) == 'on') ? true : false;
            global $wpdb;
            $sql = "SELECT
                    *
                FROM
                    {$wpdb->prefix}st_room_availability AS avai
                WHERE
                    (
                        (
                            avai.check_in <= {$start}
                            AND avai.check_out >= {$start}
                        )
                        OR (
                            avai.check_in <= {$end}
                            AND avai.check_out >= {$end}
                        )
                        OR (
                            avai.check_in <= {$start}
                            AND avai.check_out >= {$end}
                        )
                        OR (
                            avai.check_in >= {$start}
                            AND avai.check_out <= {$end}
                        )
                    )
                and avai.post_id = {$post_id}";
            $avai_rs = $wpdb->get_results($sql);
            $column = 'st_booking_id';
            if (get_post_type($post_id) == 'hotel_room') {
                $column = 'room_id';
            }
            $sql = "SELECT
                    *
                FROM
                    {$wpdb->prefix}st_order_item_meta AS _order
                WHERE
                    (
                        (
                            _order.check_in_timestamp <= {$start}
                            AND _order.check_out_timestamp >= {$start}
                        )
                        OR (
                            _order.check_in_timestamp <= {$end}
                            AND _order.check_out_timestamp >= {$end}
                        )
                        OR (
                            _order.check_in_timestamp <= {$start}
                            AND _order.check_out_timestamp >= {$end}
                        )
                        OR (
                            _order.check_in_timestamp >= {$start}
                            AND _order.check_out_timestamp <= {$end}
                        )
                    )
                AND _order.{$column} = {$post_id} AND _order.`status` NOT IN ('cancelled', 'wc-cancelled')";
            $order_rs = $wpdb->get_results($sql);
            $return = [
                'name' => esc_html($post_name),
                'values' => [],
                'id' => $post_id,
                'price_by_per_person' => $price_by_per_person
            ];
            for ($i = $start; $i <= $end; $i = strtotime('+1 day', $i)) {
                $date = $i * 1000;
                $available = true;
                $price = $base_price;
                if (!empty($avai_rs)) {
                    foreach ($avai_rs as $key => $value) {
                        if ($i >= $value->check_in && $i <= $value->check_out) {
                            if ($value->status == 'available') {
                                if ($price_by_per_person) {
                                    $adult_price = floatval($value->adult_price);
                                    $child_price = floatval($value->child_price);
                                } else {
                                    $price = (float)$value->price;
                                }
                            } else {
                                $available = false;
                            }
                            break;
                        }
                    }
                }
                if ($available) {
                    $ordered = 0;
                    if (!empty($order_rs)) {
                        foreach ($order_rs as $key => $value) {
                            if ($allow_fullday == 'on') {
                                if ($i >= $value->check_in_timestamp && $i <= $value->check_out_timestamp) {
                                    $ordered += (int)$value->room_num_search;
                                }
                            } else {
                                if ($i >= $value->check_in_timestamp && $i == strtotime('-1 day', $value->check_out_timestamp)) {
                                    $ordered += (int)$value->room_num_search;
                                }
                            }
                        }
                    }
                    if ($number_room - $ordered > 0) {
                        $return['values'][] = [
                            'from' => "/Date({$date})/",
                            'to' => "/Date({$date})/",
                            'label' => $number_room - $ordered,
                            'desc' => sprintf(__('%s left', ST_TEXTDOMAIN), $number_room - $ordered),
                            'customClass' => 'ganttBlue',
                            'price' => TravelHelper::format_money($price, ['simple_html' => true]),
                            'adult_price' => TravelHelper::format_money($adult_price, ['simple_html' => true]),
                            'child_price' => TravelHelper::format_money($child_price, ['simple_html' => true]),
                            'price_by_per_person' => $price_by_per_person
                        ];
                    } else {
                        $return['values'][] = [
                            'from' => "/Date({$date})/",
                            'to' => "/Date({$date})/",
                            'label' => __('O', ST_TEXTDOMAIN),
                            'desc' => __('Out of stock', ST_TEXTDOMAIN),
                            'customClass' => 'ganttOrange',
                            'price' => TravelHelper::format_money($price, ['simple_html' => true]),
                            'adult_price' => TravelHelper::format_money($adult_price, ['simple_html' => true]),
                            'child_price' => TravelHelper::format_money($child_price, ['simple_html' => true]),
                            'price_by_per_person' => $price_by_per_person
                        ];
                    }
                } else {
                    $return['values'][] = [
                        'from' => "/Date({$date})/",
                        'to' => "/Date({$date})/",
                        'label' => __('N', ST_TEXTDOMAIN),
                        'desc' => __('Not Available', ST_TEXTDOMAIN),
                        'customClass' => 'ganttRed',
                        'price' => TravelHelper::format_money($price, ['simple_html' => true]),
                        'adult_price' => TravelHelper::format_money($adult_price, ['simple_html' => true]),
                        'child_price' => TravelHelper::format_money($child_price, ['simple_html' => true]),
                        'price_by_per_person' => $price_by_per_person
                    ];
                }
            }
            return $return;
        }
        /**
         *
         *
         * @since 1.1.1
         * */
        function _deposit_calculator($cart_data, $item_id)
        {
            $room_id = isset($cart_data['data']['room_id']) ? $cart_data['data']['room_id'] : false;
            if ($room_id) {
                $cart_data = parent::_deposit_calculator($cart_data, $room_id);
            }
            return $cart_data;
        }
        /**
         *
         *
         * @since 1.1.1
         * */
        function _show_wc_cart_post_type_icon()
        {
            echo '<span class="booking-item-wishlist-title"><i class="fa fa-building-o"></i> ' . __('hotel', ST_TEXTDOMAIN) . ' <span></span></span>';
        }
        /**
         *
         * Show cart item information for hotel booking
         *
         * @since 1.1.1
         * */
        function _show_wc_cart_item_information($st_booking_data = [])
        {
            echo st()->load_template('hotel/wc_cart_item_information', false, ['st_booking_data' => $st_booking_data]);
        }
        function _add_room_number_field($post_type = false)
        {
            if ($post_type == 'hotel_room') {
                echo st()->load_template('hotel/checkout_fields', null, ['key' => get_the_ID()]);
                return;
            }
        }
        function _is_hotel_booking()
        {
            $items = STCart::get_items();
            if (!empty($items)) {
                foreach ($items as $key => $value) {
                    if (get_post_type($key) == 'st_hotel') return true;
                }
            }
        }
        /**
         *
         *
         *
         * @since 1.0.9
         *
         * */
        function _check_booking_period($validate)
        {
            $cart = STCart::get_items();
            $hotel_id = '';
            $today = strtotime(date('m/d/Y'));
            $check_in = $today;
            foreach ($cart as $key => $val) {
                $hotel_id = $key;
                $check_in = strtotime($val['data']['check_in']);
            }
            $booking_period = intval(get_post_meta($hotel_id, 'hotel_booking_period', true));
            $period = STDate::date_diff($today, $check_in);
            if ($booking_period && $period < $booking_period) {
                STTemplate::set_message(sprintf(__('This hotel allow minimum booking is %d day(s)', ST_TEXTDOMAIN), $booking_period), 'danger');
                $validate = false;
            }
            return $validate;
        }
        function _add_validate_fields($validate)
        {
            $items = STCart::get_items();
            if (!empty($items)) {
                foreach ($items as $key => $value) {
                    if (get_post_type($key) == 'st_hotel') {
                        // validate
                        $default = [
                            'number' => 1
                        ];
                        $value = wp_parse_args($value, $default);
                        $room_num = $value['number'];
                        $room_data = STInput::request('room_data', []);
                        if ($room_num > 1) {
                            if (!is_array($room_data) or empty($room_data)) {
                                STTemplate::set_message(__('Room infomation is required', ST_TEXTDOMAIN), 'danger');
                                $validate = false;
                            } else {
                                for ($k = 1; $k <= $room_num; $k++) {
                                    $valid = true;
                                    if (!isset($room_data[$k]['adult_number']) or !$room_data[$k]['adult_number']) {
                                        STTemplate::set_message(__('Adult number in room is required!', ST_TEXTDOMAIN), 'danger');
                                        $valid = false;
                                    }
                                    if (!isset($room_data[$k]['host_name']) or !$room_data[$k]['host_name']) {
                                        STTemplate::set_message(__('Room Host Name is required!', ST_TEXTDOMAIN), 'danger');
                                        $valid = false;
                                    }
                                    if (isset($room_data[$k]['child_number'])) {
                                        $child_number = (int)$room_data[$k]['child_number'];
                                        if ($child_number > 0) {
                                            if (!isset($room_data[$k]['age_of_children']) or !is_array($room_data[$k]['age_of_children']) or empty($room_data[$k]['age_of_children'])) {
                                                STTemplate::set_message(__('Ages of Children is required!', ST_TEXTDOMAIN), 'danger');
                                                $valid = false;
                                            } else {
                                                foreach ($room_data[$k]['age_of_children'] as $k2 => $v2) {
                                                    if (!$v2) {
                                                        STTemplate::set_message(__('Ages of Children is required!', ST_TEXTDOMAIN), 'danger');
                                                        $valid = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (!$valid) {
                                        $validate = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return $validate;
        }
        function _change_preload_search_title($return)
        {
            if (get_query_var('post_type') == 'st_hotel' || is_page_template('template-hotel-search.php')) {
                $return = __(" Hotels in %s", ST_TEXTDOMAIN);
                if (STInput::get('location_id')) {
                    $return = sprintf($return, get_the_title(STInput::get('location_id')));
                } elseif (STInput::get('location_name')) {
                    $return = sprintf($return, STInput::get('location_name'));
                } elseif (STInput::get('address')) {
                    $return = sprintf($return, STInput::get('address'));
                } else {
                    $return = __(" Hotels", ST_TEXTDOMAIN);
                }
                $return .= '...';
            }
            return $return;
        }
        function _change_comment_post_id($id_item)
        {
            return $id_item;
        }
        function add_localize()
        {
            wp_localize_script('jquery', 'st_hotel_localize', [
                'booking_required_adult' => __('Please select adult number', ST_TEXTDOMAIN),
                'booking_required_children' => __('Please select children number', ST_TEXTDOMAIN),
                'booking_required_adult_children' => __('Please select Adult and  Children number', ST_TEXTDOMAIN),
                'room' => __('Room', ST_TEXTDOMAIN),
                'is_aoc_fail' => __('Please select the ages of children', ST_TEXTDOMAIN),
                'is_not_select_date' => __('Please select Check-in and Check-out date', ST_TEXTDOMAIN),
                'is_not_select_check_in_date' => __('Please select Check-in date', ST_TEXTDOMAIN),
                'is_not_select_check_out_date' => __('Please select Check-out date', ST_TEXTDOMAIN),
                'is_host_name_fail' => __('Please provide Host Name(s)', ST_TEXTDOMAIN)
            ]);
        }
        /**
         *
         *
         *
         *
         * @update 1.1.1
         * */
        static function get_search_fields_name()
        {
            return [
                'location' => [
                    'value' => 'location',
                    'label' => __('Location', ST_TEXTDOMAIN)
                ],
                'list_location' => [
                    'value' => 'list_location',
                    'label' => __('Location List', ST_TEXTDOMAIN)
                ],
                'checkin' => [
                    'value' => 'checkin',
                    'label' => __('Check in', ST_TEXTDOMAIN)
                ],
                'checkout' => [
                    'value' => 'checkout',
                    'label' => __('Check out', ST_TEXTDOMAIN)
                ],
                'adult' => [
                    'value' => 'adult',
                    'label' => __('Adult', ST_TEXTDOMAIN)
                ],
                'children' => [
                    'value' => 'children',
                    'label' => __('Children', ST_TEXTDOMAIN)
                ],
                'taxonomy' => [
                    'value' => 'taxonomy',
                    'label' => __('Taxonomy', ST_TEXTDOMAIN)
                ],
                'price_slider' => [
                    'value' => 'price_slider',
                    'label' => __('Price slider', ST_TEXTDOMAIN)
                ],
                'room_num' => [
                    'value' => 'room_num',
                    'label' => __('Room(s)', ST_TEXTDOMAIN)
                ],
                'taxonomy_room' => [
                    'value' => 'taxonomy_room',
                    'label' => __('Taxonomy Room', ST_TEXTDOMAIN)
                ],
            ];
        }
        function count_offers($post_id = false)
        {
            if (!$post_id) $post_id = $this->hotel_id;
            //Count Rooms
            global $wpdb;
            $query_count = $wpdb->get_results("
                select DISTINCT ID from {$wpdb->posts}
                join {$wpdb->postmeta}
                on {$wpdb->postmeta} .post_id = {$wpdb->posts}.ID
                and {$wpdb->postmeta} .meta_key = 'room_parent' and {$wpdb->postmeta} .meta_value =  {$post_id}
                and {$wpdb->posts}.post_status = 'publish'
            ");
            return (count($query_count));
        }
        function get_search_fields()
        {
            $fields = st()->get_option('hotel_search_fields');
            return $fields;
        }
        function get_search_adv_fields()
        {
            $fields = st()->get_option('hotel_search_advance');
            return $fields;
        }
        function custom_hotel_layout($old_layout_id)
        {
            if (is_singular($this->post_type)) {
                $meta = get_post_meta(get_the_ID(), 'st_custom_layout', true);
                if ($meta and get_post_type($meta) == 'st_layouts') {
                    return $meta;
                }
            }
            return $old_layout_id;
        }
        function save_review_stats($comment_id)
        {
            $comemntObj = get_comment($comment_id);
            $post_id = $comemntObj->comment_post_ID;
            if (get_post_type($post_id) == 'st_hotel') {
                $all_stats = $this->get_review_stats();
                $st_review_stats = STInput::post('st_review_stats');
                if (!empty($all_stats) and is_array($all_stats)) {
                    $total_point = 0;
                    foreach ($all_stats as $key => $value) {
                        if (isset($st_review_stats[$value['title']])) {
                            $total_point += $st_review_stats[$value['title']];
                            //Now Update the Each Stat Value
                            update_comment_meta($comment_id, 'st_stat_' . sanitize_title($value['title']), $st_review_stats[$value['title']]);
                        }
                    }
                    $avg = round($total_point / count($all_stats), 1);
                    //Update comment rate with avg point
                    $rate = wp_filter_nohtml_kses($avg);
                    if ($rate > 5) {
                        //Max rate is 5
                        $rate = 5;
                    }
                    update_comment_meta($comment_id, 'comment_rate', $rate);
                    //Now Update the Stats Value
                    update_comment_meta($comment_id, 'st_review_stats', $st_review_stats);
                }
            }
            if (STInput::post('comment_rate')) {
                update_comment_meta($comment_id, 'comment_rate', STInput::post('comment_rate'));
            }
            //review_stats
            $avg = STReview::get_avg_rate($post_id);
            update_post_meta($post_id, 'rate_review', $avg);
        }
        function save_post_review_stats($comment_id)
        {
            $comemntObj = get_comment($comment_id);
            $post_id = $comemntObj->comment_post_ID;
            $avg = STReview::get_avg_rate($post_id);
            update_post_meta($post_id, 'rate_review', $avg);
        }
        function get_review_stats()
        {
            $review_stat = st()->get_option('hotel_review_stats');
            return $review_stat;
        }
        function get_review_stats_metabox()
        {
            $review_stat = st()->get_option('hotel_review_stats');
            $result = [];
            if (!empty($review_stat)) {
                foreach ($review_stat as $key => $value) {
                    $result[] = [
                        'label' => $value['title'],
                        'value' => sanitize_title($value['title'])
                    ];
                }
            }
            return $result;
        }
        function comment_args($comment_form, $post_id = false)
        {
            if (!$post_id) $post_id = get_the_ID();
            if (get_post_type($post_id) == 'st_hotel') {
                $stats = $this->get_review_stats();
                if ($stats and is_array($stats)) {
                    $stat_html = '<ul class="list booking-item-raiting-summary-list stats-list-select">';
                    foreach ($stats as $key => $value) {
                        $stat_html .= '<li class=""><div class="booking-item-raiting-list-title">' . $value['title'] . '</div>
                                                    <ul class="icon-group booking-item-rating-stars">
                                                    <li class=""><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li class=""><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li class=""><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li class=""><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                </ul>
                                                <input type="hidden" class="st_review_stats" value="0" name="st_review_stats[' . $value['title'] . ']">
                                                    </li>';
                    }
                    $stat_html .= '</ul>';
                    $comment_form['comment_field'] = "
                        <div class='row'>
                            <div class=\"col-sm-8\">
                    ";
                    $comment_form['comment_field'] .= '<div class="form-group">
                                            <label>' . __('Review Title', ST_TEXTDOMAIN) . '</label>
                                            <input class="form-control" type="text" name="comment_title">
                                        </div>';
                    $comment_form['comment_field'] .= '<div class="form-group">
                                            <label>' . __('Review Text', ST_TEXTDOMAIN) . '</label>
                                            <textarea name="comment" id="comment" class="form-control" rows="6"></textarea>
                                        </div>
                                        </div><!--End col-sm-8-->
                                        ';
                    $comment_form['comment_field'] .= '<div class="col-sm-4">' . $stat_html . '</div></div><!--End Row-->';
                }
            }
            return $comment_form;
        }
        function hotel_add_to_cart()
        {
            if (STInput::request('action') == 'hotel_add_to_cart') {
                if ($this->do_add_to_cart()) {
                    $link = STCart::get_cart_link();
                    wp_safe_redirect($link);
                    die;
                }
            }
        }
        // NOTE 2/ this called by the function ajax_hotel_add_to_cart
        // REVIEW DUMP DATA HERE BEFORE CHECKOUT
        function do_add_to_cart() 
        {
            $pass_validate  = true;
            $item_id        = intval(STInput::request('item_id', ''));
            $check_in       = STInput::request('check_in', '');
            $check_in       = TravelHelper::convertDateFormat($check_in);
            $check_out      = STInput::request('check_out', '');
            $check_out      = TravelHelper::convertDateFormat($check_out);
            $room_num_search = intval(STInput::request('room_num_search', ''));
            if ($item_id <= 0) {
                STTemplate::set_message(__('Hotel indisponible!', ST_TEXTDOMAIN), 'danger');
                $pass_validate = false;
                return false;
            }
            if (empty($check_in)) {
                STTemplate::set_message(__('Veuillez choisir une date valide!', ST_TEXTDOMAIN), 'danger');
                $pass_validate = false;
                return false;
            }
            if (empty($check_out)) {
                STTemplate::set_message(__('Veuillez choisir une date valide!', ST_TEXTDOMAIN), 'danger');
                $pass_validate = false;
                return false;
            }
            if (empty($room_num_search))
                $room_num_search = intval(STInput::request('number_room', ''));
            if ($room_num_search <= 0) $room_num_search = 1;
            $today          = strtotime( date( 'Y-m-d' ) );
	        $discount_rate  = self::get_discount_by_period($item_id, $today); 
            $sale_price     = 0;
            $extra_id       = array();
            $extra_price    = array();
            $resultroom     = array();
            $extra_supp     = array();
            $rooms_ids_arr  = array();
            $period_ids_arr = json_decode(base64_decode(STInput::request('period_ids_arr')));
            $rooms_ids_arr  = json_decode(base64_decode(STInput::request('rooms_ids_arr')));
            $first_p_retro  = json_decode(base64_decode(STInput::request('retrocession')));
            // saving data for each room
            for ($i = 1; $i <= $room_num_search; $i++) {
                //dd(STInput::request(), "ALL INPUT REQUEST");
                $room_id        = intval(STInput::request('room-'.$i, ''));
                $price          = STInput::request('sale-price-'.$i, '');
                $arrangement    = STInput::request('arrangement-room-'.$i, '');
                $extra_id       = STInput::request('extra-room-'.$i, '');
                $extra_price    = STInput::request('extra-id-' . $extra_id . '-room-'.$i, '');
                $extra_supp_id  = STInput::request('def-extra-room-'.$i, '');
                $extra_supp     = STInput::request('def-extra-id-' . $extra_supp_id . '-room-'.$i, '');
                $discount_beds_id  = STInput::request('def-bed-discount-room-'.$i, '');
                $discount_beds     = STInput::request('def-bed-discount-id-' . $discount_beds_id . '-room-'.$i, '');
                $resultroom[$i]['room_id']          = $room_id;
                $resultroom[$i]['sale_price']       = $price;
                $resultroom[$i]['arrangement_id']   = $arrangement;
                $resultroom[$i]['extra_price']      = $extra_price;
                $resultroom[$i]['extra_id']         = $extra_id;
                $resultroom[$i]['extra_single']     = $extra_supp;
                $resultroom[$i]['extra_single_id']  = $extra_supp_id;
                $resultroom[$i]['discount_beds']    = $discount_beds;
                $resultroom[$i]['discount_beds_id'] = $discount_beds_id;
                $resultroom[$i]['result_adult_child']       = json_decode(base64_decode(STInput::request('result-room-'.$i, '')));
                $resultroom[$i]['result_adult_child_calc']  = json_decode(base64_decode(STInput::request('data_room-room-'.$i, '')));
                // Array room IDS 
                $rooms_ids_arr[$i]    = $room_id;
                $sale_price += $price + $extra_price["price"];
            }
            $rooms_ids_arr = array_count_values($rooms_ids_arr);
            /**
             * Check reservation status
             * Per room in reservation 
             * ex. 2 double rooms / 3 single room in 1 single reservation
             */
            $reservation = self::checkRoomsStock($rooms_ids_arr, $period_ids_arr, $check_out);
            $retrocession = self::checkRetrocession($check_in, $check_out, $reservation, $first_p_retro);
            $data = [
                'item_price'        => $sale_price,
                'ori_price'         => $sale_price,
                'check_in'          => $check_in,
                'check_out'         => $check_out,
                'room_num_search'   => $room_num_search,
                'commission'        => TravelHelper::get_commission($item_id),
                'discount_rate'     => $discount_rate,
                'guest_title'       => STInput::post('guest_title'),
                'guest_name'        => STInput::post('guest_name'),
                'result'            => $resultroom,
                'rooms_ids_arr'     => $rooms_ids_arr,
                'period_ids_arr'    => $period_ids_arr,
                'retrocession'      => $retrocession,
                'reservation'       => $reservation,
            ];
            if ($pass_validate) {
                $pass_validate = apply_filters('st_hotel_add_cart_validate', $pass_validate, $data);
            }
            if ($pass_validate) {
                STCart::add_cart($item_id, $room_num_search, $sale_price, $data);
            }
            return $pass_validate;
        }
        function is_booking_period($item_id = '', $t = '', $c = '')
        {
            $today = strtotime($t);
            $check_in = strtotime($c);
            $booking_period = intval(get_post_meta($item_id, 'hotel_booking_period', true));
            $period = STDate::date_diff($today, $check_in);
            if ($period < $booking_period) {
                return $booking_period;
            }
            return false;
        }
        function get_cart_item_html($item_id = false) 
        {
            return st()->load_template('layouts/modern/hotel/elements/cart-item', null, ['item_id' => $item_id]);
        }
        function add_sidebar()
        {
            register_sidebar([
                'name' => __('Hotel Search Sidebar 1', ST_TEXTDOMAIN),
                'id' => 'hotel-sidebar',
                'description' => __('Widgets in this area will be shown on Hotel', ST_TEXTDOMAIN),
                'before_title' => '<h4>',
                'after_title' => '</h4>',
                'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
                'after_widget' => '</div>',
            ]);
            register_sidebar([
                'name' => __('Hotel Search Sidebar 2', ST_TEXTDOMAIN),
                'id' => 'hotel-sidebar-2',
                'description' => __('Widgets in this area will be shown on Hotel', ST_TEXTDOMAIN),
                'before_title' => '<h4>',
                'after_title' => '</h4>',
                'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
                'after_widget' => '</div>',
            ]);
        }
        /**
         *
         *
         * @since  1.0.1
         * @update 1.0.9
         * */
        function change_sidebar($sidebar = false)
        {
            return st()->get_option('hotel_sidebar_pos', 'left');
        }
        /**
         * NOTE search page result strings
         */
        function get_result_string()
        {
            global $wp_query, $st_search_query;
            if ($st_search_query) {
                $query = $st_search_query;
            } else $query = $wp_query;
            $result_string = $p1 = $p2 = $p3 = $p4 = '';
            $location_id = STInput::get('location_id', '');
            $get_post =  get_post($location_id);
            if (!empty($location_id) and isset($get_post)) {
                $p1 = sprintf(__('%s: ', ST_TEXTDOMAIN), get_the_title($location_id));
            } elseif (STInput::request('address')) {
                $p1 = sprintf(__('%s: ', ST_TEXTDOMAIN), STInput::request('address',''));
            }
            if ($query->found_posts) {
                if ($query->found_posts > 1) {
                    $p2 = sprintf(__('%s hôtels trouvés', ST_TEXTDOMAIN), $query->found_posts);
                } else {
                    $p2 = sprintf(__('%s hôtel trouvé', ST_TEXTDOMAIN), $query->found_posts);
                }
            } else {
                $p2 = __('Aucun hôtel trouvé', ST_TEXTDOMAIN);
            }
            // check Right to left
            if (st()->get_option('right_to_left') == 'on' || is_rtl()) {
                return $p2 . $p1;
            }
            return esc_html($p1 . $p2);
        }
        function ajax_search_room()
        {
            global $wpdb;
            check_ajax_referer('st_frontend_security', 'security');
            $result = [
                'status' => 1,
                'data' => '',
            ];
            $post = STInput::request();
            $hotel_id = $post['room_parent'];
            $check_in = TravelHelper::convertDateFormat($post['start']);
            $check_out = TravelHelper::convertDateFormat($post['end']);
            $date_diff = STDate::dateDiff($check_in, $check_out);            
            $booking_periode = TravelHelper::getListNight($check_in, $check_out);
            $ranges                     = array();
            $prix_adult_enfant_final    = array();
            $periode_accommdation_IDS   = array();
            $periode_extra_IDS          = array();
            $prix_single_arr            = array();
            $prix_bed_discount_arr      = array();
            $extra_supp_arr             = array();
            $period_ids_arr             = array();
            $day                        = 1;
            $room_num                   = STInput::request( 'room_num_search' );
            $reservation_status         = true;
            $retrocession               = 0;
            $result['html'] .= '<h2 class="st-heading-section">Chambres disponibles</h2> ';
            // looping through number of rooms
            $extend_ranges_final = array();
            foreach($booking_periode as $date)
            {
                $sql = "SELECT *
                        FROM {$wpdb->prefix}st_room_availability
                        WHERE
                        post_id = ".$hotel_id."
                        AND 
                        (
                            periode_from <= ".strtotime($date)."
                            AND 
                            periode_to >= ".strtotime($date)."
                        )
                        AND
                        status LIKE 'available'";
                // checking if single date is available
                $periode_per_date = $wpdb->get_row( $sql );
                if($periode_per_date)
                {
                    $prix_adult_enfant_final[]  = json_decode($periode_per_date->adult_enfant);
                    // 1- Ault enfant JSON
                    $res_prix_adult_enfant      = json_decode($periode_per_date->adult_enfant, true);
                    // 2- Accomodations JSON
                    $periode_accomodation       = json_decode($periode_per_date->accommodation);
                    $periode_accomodation       = $this->remove_empty_accomotation($periode_accomodation);
                    $periode_accommdation_IDS   = $this->get_periode_accommodation_ids($periode_accomodation, $periode_accommdation_IDS);
                    $periode_extra              = json_decode($periode_per_date->extra_price);
                    $periode_extra              = $this->remove_empty_extra($periode_extra);
                    // INIT
                    $resultroom         = array();
                    $__pricing          = array();
                    $arr_adult_number   = array();
                    $arr_child_number   = array();
                    $arr_adult_children = array();
                    $pax                = "PAX = ";
                    $range_age_txt      = '';
                    $age_adult_txt      = '';
                    $age                = '';
                    $last_age           = '';
                    $max_age = 0;
                    if($periode_per_date->max_age_1){
                        $max_age = $periode_per_date->max_age_1;
                    }
                    if ($periode_per_date->max_age_2){
                        $max_age = $periode_per_date->max_age_2;
                    }
                    // nbR = number of rooms
                    for ($nbR = 1; $nbR <= $room_num; $nbR++)
                    {
                        $periode_extra              = json_decode($periode_per_date->extra_price);
                        $periode_extra              = $this->remove_empty_extra($periode_extra);
                        // INIT
                        $total_pax  = 0;
                        $nb_adult   = 0;
                        $nb_children = 0;
                        $all_ages = array();
                        $single_person[$nbR]['single_person'] = false;
                        $discount_beds[$nbR]['discount_beds'] = false;
                        if(STInput::request( 'child_number'.$nbR ) > 0) {
                            for ($x = 1; $x <= STInput::request( 'child_number'.$nbR ); $x++) {
                                $all_ages[] = STInput::request( 'age_number_'.$nbR.'_'.$x);
                            }
                        }
                        $total_pax = STInput::request( 'adult_number'.$nbR ) +  STInput::request( 'child_number'.$nbR );
                        $adult_number = 'adult_number_'.$nbR;
                        $child_number = 'child_number_'.$nbR;
                        $resultroom[$nbR]['room_num_search']    = STInput::request( 'room_num_search' );
                        $resultroom[$nbR][$adult_number]        = STInput::request('adult_number'.$nbR );
                        $__pricing[$nbR]['room_num'] = $nbR;
                        if( STInput::request('adult_number'.$nbR ) > 0 ) {
                            $arr_adult_number['number_adult'] += STInput::request('adult_number'.$nbR );
                            $pax    .= " + " . STInput::request('adult_number'.$nbR ) . " adulte";
                            $age_adult_txt = "-adult-" . STInput::request('adult_number'.$nbR );
                            $nb_adult += STInput::request('adult_number'.$nbR );
                            $__pricing[$nbR]['nb_adult'] += STInput::request('adult_number'.$nbR );
                        } else {
                            $age_adult_txt = "-adult-0";
                        }
                        $resultroom[$nbR][$child_number] = STInput::request('child_number'.$nbR );
                        if( STInput::request( 'child_number'.$nbR ) > 0) {
                            $arr_child_number['number_child'] += STInput::request( 'child_number'.$nbR );
                            // fetching childrens for age
                            for ($nbC = 1; $nbC <= STInput::request( 'child_number'.$nbR ); $nbC++) {
                                $range_age_txt  = '';
                                $age            = '';
                                $age_child      = 'age_number_'.$nbR.'_'.$nbC;
                                $resultroom[$nbR][$age_child] = STInput::request( 'age_number_'.$nbR.'_'.$nbC);
                                $pax .= " + " . STInput::request( 'child_number'.$nbR ) . " enfant (".STInput::request( 'age_number_'.$nbR.'_'.$nbC)." ans)";
                                if ( 
                                    STInput::request( 'age_number_'.$nbR.'_'.$nbC) >= $periode_per_date->min_age_1 && 
                                    STInput::request( 'age_number_'.$nbR.'_'.$nbC) <=  $periode_per_date->max_age_1
                                ){
                                        $range_age_txt = "age-".$periode_per_date->min_age_1."_".$periode_per_date->max_age_1;
                                } else if ( 
                                    STInput::request( 'age_number_'.$nbR.'_'.$nbC) >= $periode_per_date->min_age_2 && 
                                    STInput::request( 'age_number_'.$nbR.'_'.$nbC) <=  $periode_per_date->max_age_2
                                ){
                                        $range_age_txt = "age-".$periode_per_date->min_age_2."_".$periode_per_date->max_age_2;
                                } else if (STInput::request( 'age_number_'.$nbR.'_'.$nbC) > $periode_per_date->min_age_1){
                                        $nb_adult += 1;
                                        $__pricing[$nbR]['nb_adult'] += 1;
                                }
                                $age                            .= $range_age_txt;
                                $age                            .= "-enf-".$nbC;
                                // BUG FIX
                                // Checking on the adults number if it is defined or put 0
                                if(!STInput::request('adult_number'.$nbR )){
                                    $age .= $this->final_check_adult(0, $all_ages, $max_age);
                                } else {
                                    $age .= $this->final_check_adult(STInput::request('adult_number'.$nbR ), $all_ages, $max_age);
                                }
                                $last_age                       = $age;
                                $nb_children                    += $this->search_price_adult_children($res_prix_adult_enfant, $last_age);
                                $__pricing[$nbR]['nb_children'] += $this->search_price_adult_children($res_prix_adult_enfant, $last_age);
                                $arr_adult_children[]           = $last_age;
                            }
                        } // end if
                        /********************************** DUMP **************************************/
                        // $extend_ranges_final[$nbR][$day]['nbR']             = 'room_num => '.$nbR;
                        $total_pax_final = $nb_adult + $nb_children;
                        // check on 3/4 adults discount
                        if( ( $total_pax_final == 3 ) || ( $total_pax_final == 4 ) )
                        {
                            $total_pax_final = self::apply_discount_on_beds($total_pax_final, $periode_per_date->prix_3_lits);
                            $single_person[$nbR]['discount_beds']               = true;
                            $extend_ranges_final[$nbR][$day]['discount_beds']   = true;
                        }
                        if($periode_per_date->prix_3_lits)
                        {
                            $prix_bed_discount_arr[$nbR][$periode_per_date->id]   = $periode_per_date->prix_3_lits;
                        }
                        $extend_ranges_final[$nbR][$day]['day']             = $day;
                        $extend_ranges_final[$nbR][$day]['date']            = $date;
                        $extend_ranges_final[$nbR][$day]['periode_id']      = $periode_per_date->id;
                        $extend_ranges_final[$nbR][$day]['min_stay']        = $periode_per_date->min_stay;
                        $extend_ranges_final[$nbR][$day]['marge']           = $periode_per_date->marge;
                        $extend_ranges_final[$nbR][$day]['discount']        = $periode_per_date->discount;
                        $extend_ranges_final[$nbR][$day]['supp_single']     = $periode_per_date->prix_single;
                        $extend_ranges_final[$nbR][$day]['extra_supp']      = self::update_supp_extra_calculation($periode_extra, $total_pax_final);
                        // $extra_supp_arr[$nbR][$periode_per_date->id]        = $periode_per_date->prix_single;
                        $extend_ranges_final[$nbR][$day]['total_pax']       = $total_pax_final;
                        $extend_ranges_final[$nbR][$day]['retrocession']    = $periode_per_date->retrocession;
                        // check on single person condition
                        if($periode_per_date->prix_single){
                            $prix_single_arr[$nbR][$periode_per_date->id]   = self::marge_calculation($periode_per_date->prix_single, $periode_per_date->marge);
                        }
                        if($total_pax == 1) {
                            $single_person[$nbR]['single_person']               = true;
                            $extend_ranges_final[$nbR][$day]['single_person']   = true;
                        }
                        $period_ids_arr[$periode_per_date->id]              = $periode_per_date->id;
                        $ranges = $extend_ranges_final;
                        /********************************** DUMP **************************************/
                    } // END loop through room numbers
                    // 3- Extra lists JSON
                    $periode_extra_IDS          = $this->get_periode_extra_ids($periode_extra, $periode_extra_IDS);
                } else 
                {
                    // No periode found
                    $result = [
                        'status'    => 0,
                        'html'      => "",
                        'message'   => st()->load_template('layouts/modern/common/message', '', ['status' => 'danger', 'message' => __('L\'hotel ne posséde pas des chambres pendant cetter période', ST_TEXTDOMAIN)]),
                        'more-data' => "no_periode"
                    ];
                    echo json_encode($result);
                    die;
                } // END checking periode
                $day++;
            } // END loop through dates of periode
            // Checking min only for the 1st periode NESTED/SINGLE
            if( $extend_ranges_final[1][1]['min_stay'] > $date_diff ){
                $reservation_status = false;
                $result = [
                    'status'    => 0,
                    'html'      => "",
                    'message'   => st()->load_template('layouts/modern/common/message', '', ['status' => 'danger', 'message' => __('Minimum nombre de séjour est '. $extend_ranges_final[1]['min_stay'] .' jour(s)', ST_TEXTDOMAIN)]),
                    'more-data' => "no_periode"
                ];
                echo json_encode($result);
                die;
            }
            // Retrocession for the 1st period / to compare it later
            $retrocession = $extend_ranges_final[1][1]['retrocession'];
            // Ordring custom periodes if there is NESTED periodes
            $periode_accommdation_final = $this->get_duplicates_periodes($periode_accommdation_IDS, $date_diff);
            $periode_extra_final        = $this->get_duplicates_periodes($periode_extra_IDS, $date_diff);
            //dd($periode_extra_final, 'periode_extra_final');
            //dd($periode_accommdation_final, 'export periode_accommdation_final', true);
            for ($i = 1; $i <= $room_num; $i++)
            {   
                $total_pax = STInput::request( 'adult_number'.$i ) +  STInput::request( 'child_number'.$i );
                global $varmaxsize;
                $varmaxsize = $total_pax;
                global $post;
                $old_post = $post;
                // search room with condition number of pax in single hotel room = number total pax
                $query = $this->search_room();                
                if ($query->have_posts()) 
                {
                    while ($query->have_posts()) 
                    {
                        $query->the_post();
                        $data = array(
                            'hotel_id'              => $hotel_id,
                            'post_id'               => $post->ID,
                            'result'                => $resultroom,
                            'i'                     => $i,
                            'num'                   => $room_num,
                            'pricing'               => $__pricing,
                            'ranges_final'          => $ranges,
                            'shared_arrangements'   => $periode_accommdation_final,
                            'shared_extras'         => $periode_extra_final,
                            'single_person'         => $single_person,
                            'prix_single_arr'       => $prix_single_arr,
                            'prix_bed_discount_arr' => $prix_bed_discount_arr,
                            'period_ids_arr'        => $period_ids_arr,
                            'reservation_status'    => $reservation_status,
                            'retrocession'          => $retrocession,
                        );
                        // saving result rooms 
                        $result['html'] .= preg_replace('/^\s+|\n|\r|\s+$/m', '', st()->load_template('layouts/modern/hotel/loop/room_item', false , ['data' => $data]));
                        //$result['html'] .= '<pre style=" white-space: pre-wrap; width: 40em; width: 100%; "><h6 color="red">EXTENDED RESULT ROOM ' .$i. '</h6>' . json_encode($data) . '</pre>';
                    }
                }else 
                {
                    // there is no room
                    // Désolé! Pas de chambres disponibles trouvé
                    $result['html'] .= st()->load_template('layouts/modern/hotel/loop/room_item_none');
                    echo json_encode($result);
                    die;
                } // END no room available
                wp_reset_postdata();
                $post = $old_post;
                /*************************** SEARCH ROOM POST HERE *****************************/
            } // END second loop through room numbers
            $result['html'] .= '
            <div class="alerte-info">
                <ul>
                    <li>Prix : les tarifs ne sont affichés qu\'à titre indicatif. Les tarifs réels seront calculés et affichés sur la page suivante.</li>
                    <li>Prix par personne affiché : est calculé sur la base d\'une occupation maximale pour chaque type de chambre.</li>
                </ul>
            </div>';
            echo json_encode($result);
            die();
        }
        /** 
         * NOTE Function to sync Stock management
         */
        static function syncStock($order_id, $room_ids, $period_ids)
        {
            global $wpdb;
            $table = $wpdb->prefix . 'st_stock_availability';
                foreach($room_ids as $room_id => $number)
                {
                    foreach ($period_ids as $period_id) {
                        $value = [
                            'periode_id'        => $period_id,
                            'room_id'           => $room_id,
                            'reservation_id'    => $order_id,
                            'nb_rooms'          => $number
                        ];
                        $wpdb->insert( $table, $value );
                    }
                }
        }
        /**
         * NOTE function used to check retrocession of reservation
         */
        static function checkRetrocession($check_in, $check_out, $reservation, $retrocession)
        {
            $today = date('m/d/Y');
            $period = STDate::dateDiff($today, $check_in);
            $result = false; 
            if( $retrocession >= $period ) {
                $result = true; 
            } else {
                $result = false; 
            }
            return $result;
        }
        /**
         * NOTE function to return the stock of rooms per periods with the verification
         * of the check out date current reservation
         */
        static function checkRoomsStock($rooms_ids_arr, $period_ids_arr, $check_out)
        {
            $reservation = array();
            $reservation['status'] = 'on-attente';
            $reservation['raison'] = 'Demande approbation';
            foreach ($rooms_ids_arr as $room_id => $room_count) {
                $remain_stock = STHotel::checkStock($room_id, $period_ids_arr, $room_count, strtotime($check_out));
                if($room_count > $remain_stock){
                    $reservation['status'] = 'sur-demande';
                    $reservation['raison'] = 'Stock non disponible';
                    break; // exit when true
                }
            }
            
            return $reservation;
        }
        /**
         * NOTE count of rooms in a period in databse
         */
        static function checkStock($room_id, $period_ids, $room_count, $check_out){
            global $wpdb;
            $table      = $wpdb->prefix . 'st_stock_availability';
            $table_2    = $wpdb->prefix . 'st_order_item_meta';
            $txt = '';
            foreach ($period_ids as $period_id) {
                // GET the stock of rooms within reservations check_out > check_out current reservation 
                $sql = "
                    SELECT sum(`nb_rooms`)
                    FROM {$table} S
                        RIGHT JOIN {$table_2} C
                        ON S.reservation_id = C.order_item_id
                        AND C.check_out_timestamp >= ".$check_out."
                    WHERE S.room_id = ".$room_id." 
                    AND S.periode_id = ".$period_id." 
                ";
                // FEATURE We can add status verifications
                $nb_rooms        = $wpdb->get_var( $sql );
                // dd($sql, 'sql checkStock');
                $available_stock = self::get_room_id_stock($period_id, $room_id);
                $remain_stock    = $available_stock - $nb_rooms;
            }
            return $remain_stock;
        }
         /**
         * NOTE Function to get stock of a room Based on ID
         */
        static function get_room_id_stock($periode_id, $input_room_id)
        {
            $result = 0;
            global $wpdb;
            $sql = "
                SELECT stock
                FROM {$wpdb->prefix}st_room_availability
                WHERE
                id = ".$periode_id."
            ";
            $stock_ob  = $wpdb->get_row( $sql, OBJECT);
            $stock_rooms = json_decode($stock_ob->stock);
            foreach ($stock_rooms as $_stock) {
                $room_id_db = intval($_stock->room_id);
                if( $room_id_db == intval($input_room_id) ) {
                    $result = $_stock->stock;
                }
            }
            return $result;
        }
        /**
         * NOTE Function to combine all extras in periodes in one extra array with calculated prices
         */
        static function get_supp_sum($extra_supp, $shared_extras){
            $result = array();
            foreach ($shared_extras as $extra) {
                foreach ($extra_supp as $period_extra) {
                    //$result[$extra] += $period_extra['extra_supp'][$extra];
                    $result[$extra] += self::marge_calculation($period_extra['extra_supp'][$extra], $period_extra['marge']);
                }
            }
            return $result;
        }
        /**
         * NOTE Function to update extra prices calcul based on NB PAX
         */
        static function update_supp_extra_calculation($periode_extra, $total_pax)
        {
            $result = array();
            foreach( $periode_extra as $extra){
                   // $result[$extra->id_extra] = $extra->prix . " * " . $total_pax;
                    $result[$extra->id_extra] = $extra->prix * $total_pax;
            }
            return $result;
        }
        /**
         * NOTE Function to get price of Arrangement Based on ID
         */
        static function get_arrangement_id_price($periode_id, $arrangement_id)
        {
            global $wpdb;
            $sql = "
                SELECT accommodation
                FROM {$wpdb->prefix}st_room_availability
                WHERE
                id = ".$periode_id."
            ";
            $accommodation_ob  = $wpdb->get_row( $sql, OBJECT);
            $accomodation = json_decode($accommodation_ob->accommodation);
            foreach( $accomodation as $acc){
                if($acc->id_accomodation == $arrangement_id)
                    return $acc->prix;
            }
        }
        /**
         * NOTE Base Function to calcul prices
         */
        static function calcul_price_arrangement($ranges_final, $arrangements)
        {
            $output = array();
            $day = 1;
            $dump = '';
            foreach($ranges_final as $single_day)
            {
                $sub_output = array();
                $dump .= "Jour = " . $single_day['day'] . " | Marge = " . $single_day['marge'] . " | PID => " .$single_day['periode_id'] .  " x PAX " . $single_day['total_pax'];
                foreach($arrangements as $arrangement_id)
                {
                    $dump .= '<hr>';
                    // Init
                    $init_price_1 = self::get_arrangement_id_price($single_day['periode_id'], $arrangement_id);
                    $dump .= "<br>-- [".$arrangement_id."] Prix initial = " . $init_price_1;
                    // Apply marge
                    $init_price_1 = self::marge_calculation($init_price_1, $single_day['marge']);
                    $dump .= "<br>--- Prix avec marge = " . $init_price_1;
                    // Apply discount
                    $init_price_1 = self::discount_calculation($init_price_1, $single_day['discount']);
                    $dump .= "<br>---- Prix promotions = " . $init_price_1;
                    // $discount = STHotel::get_discount_by_period($item_id, $today);
                    // Apply Single supp
                    if($single_day['single_person']) {
                        $single_day_price = self::marge_calculation( $single_day['supp_single'], $single_day['marge'] );
                        $init_price_1 = self::supp_single_calculation( $init_price_1, $single_day_price );
                        $dump .= "<br>---- Prix init single = " . $single_day_price;
                        $dump .= "<br>----- Prix après single = " . $init_price_1;
                    }
                    $init_price_1 = $single_day['total_pax'] * $init_price_1;
                    //$sub_output[$arrangement_id] = $init_price_1;
                    $output[$arrangement_id] += $init_price_1;
                    $dump .= "<br>------ Prix pour " . $arrangement_id . " => " . $init_price_1;
                }
                $dump .= '<hr>';
                $day++;
                //$output[$day] = $sub_output;
            }
            //echo $dump;
            return $output;
        }
        /**
         * NOTE Function to remove duplicating periodes
         */
        static function get_duplicates_periodes($periode_accommdation_IDS, $nbDays) {
            $result = array();
            foreach(array_count_values($periode_accommdation_IDS) as $key => $val) {
                // if the ID occurs more than X times
                if ( $val == $nbDays ) {
                    $result[] = $key;
                }
            }
            return $result;
        }
        static function get_periode_accommodation_ids($periode_accomodation, $periode_accommdation_IDS){
            foreach($periode_accomodation as $accommodation){
                $periode_accommdation_IDS[] = $accommodation->id_accomodation;
            }
            return $periode_accommdation_IDS;
        }
        static function get_periode_extra_ids($periode_extra, $periode_extra_IDS){
            foreach($periode_extra as $extra){
                $periode_extra_IDS[] = $extra->id_extra;
            }
            return $periode_extra_IDS;
        }
        static function remove_empty_accomotation($periode_accomodation){
            $stdArray = array();
            foreach($periode_accomodation as $accommodation){
                if( $accommodation->prix != 0 ){
                    $stdArray[$accommodation->id_accomodation] = $accommodation;
                }
            }
            return $stdArray;
        }
        static function remove_empty_extra($periode_extra){
            $stdArray = array();
            foreach($periode_extra as $extra){
                if( $extra->prix != 0 ){
                    $stdArray[$extra->id_extra] = $extra;
                }
            }
            return $stdArray;
        }
        /**
         * NOTE Calcul price from accommodations * nb adults
         */
        static function calcul_price($periode_accomodation, $total_pax){
            $price = 0;
            foreach($periode_accomodation as $accommodation){
                $price += $accommodation->prix;
            }
            return $price;
        }
        /**
         * Get the Min age from single periode
         */
        static function get_min_age($periode_id, $interval)
        {
            $output = 0;
            if($interval == 1) {
                $output = 2;
            } else {
                $output = 5.99;
            }
            return $output;
        }
        /**
         * Get the Max age from single periode
         */
        static function get_max_age($periode_id, $interval)
        {
            $output = 0;
            if($interval == 1) {
                $output = 6;
            } else {
                $output = 11.99;
            }
            return $output;
        }
        /**
         * NOTE function to apply discount on adults number 
         */
        static function apply_discount_on_beds($adults_number, $discount)
        {
            $output = $adults_number;
            if( ($adults_number == 3) || ($adults_number == 4) ){
                $output = $adults_number - ($discount/100);
            }
            return $output;
        }
        /**
         * NOTE function to verify child age comparing to adult adult-0 / adult-1
         */
        static function final_check_adult($counter, $all_ages, $max_age){
            // $max_age = max($all_ages);
            if(max($all_ages) > $max_age){
                $counter++;
            }
            return "-adult-".$counter;
        }
        // NOTE function to get the max size of rooms
        static function get_max_size_room($hotel_id, $total_pax){
            global $wpdb;
            $table = $wpdb->prefix . 'hotel_room';
            $table1 = $wpdb->prefix . 'link_hotel_room';
            $sql =  "SELECT * FROM {$wpdb->prefix}posts";
            $sql .= " INNER JOIN {$table} as tb ON {$wpdb->prefix}posts.ID = tb.post_id ";
            $sql .= " INNER JOIN {$table1} as tbb ON {$wpdb->prefix}posts.ID = tbb.post_id AND tbb.room_parent = ".$hotel_id." AND tb.adult_number = ".$total_pax."";
            return $sql;
        }
        static function get_price_pension($accommodation_list, $id){
              for($i=0; $i<count($accommodation_list); $i++){
                    if($accommodation_list[$i]['id_accomodation'] == $id) {
                            return $accommodation_list[$i]['prix'];
                    }
            }
        }
        static function get_saved_stock($stock_list, $id){
            for($i=0; $i<count($stock_list); $i++){
                  if($stock_list[$i]['room_id'] == $id) {
                          return $stock_list[$i]['stock'];
                  }
          }
        }
        static function get_price_extra($accommodation_list, $id){
                for($i=0; $i<count($accommodation_list); $i++){
                    if($accommodation_list[$i]['id_extra'] == $id) {
                            return $accommodation_list[$i]['prix'];
                    }
            }
        }
        static function get_selected_saison($saison){
            switch ($saison) {
                case 'basse_saison':
                    return 'Basse Saison';
                    break;
                case 'moyenne_saison':
                    return 'Moyenne Saison';
                    break;
                case 'haute_saison':
                    return 'Haute Saison';
                        break;
                default:
                return 'Basse Saison';
                    break;
            }
        }
        static function get_adult_child($adult_enfant, $term){
            if (!is_array($adult_enfant)) {
                return 0;
              }
              foreach ($adult_enfant as $nested_array) {
                if(array_key_exists($term, $nested_array)){
                        return $nested_array[$term];
                    } else {
                    // return 0;
                    }          
            }
        }
        // NOTE function to get the discount of adult/children
        function search_price_adult_children($array, $condition){
            if (!is_array($array)) {
              return 0;
            }
            foreach ($array as $key => $nested_array) {
                if(array_key_exists($condition, $nested_array)){
                        return (100 - $nested_array[$condition]) / 100;
                      } else {
                        // return 0;
                      }          
            }
        }
        function get_min_stay_range($min){
            $res[] = $min;
            return $res;
        }
        function getDatesFromRange($start, $end) { 
            $range = [];
            $begin = new DateTime($start);
            $end = new DateTime($end);
            $interval = new DateInterval('P1D'); // 1 Day
            $dateRange = new DatePeriod($begin, $interval, $end);
            foreach ($dateRange as $date) {
                $range[] = $date->format('d/m/Y');
            }
            return $range;
        }
        /**
         * NOTE used in the ajax_search_room function
         */
        function search_room()
        {
            $current_lang = TravelHelper::current_lang();
            $main_lang = TravelHelper::primary_lang();
            if (TravelHelper::is_wpml()) {
                global $sitepress;
                $sitepress->switch_lang($main_lang, true);
            }
            $this->alter_search_room_query();
            $arg = apply_filters(
                'st_ajax_search_room_arg', 
                [
                    'post_type'         => 'hotel_room',
                    'posts_per_page'    => -1,
                ]
            );
            $query = new WP_Query($arg);
            $this->remove_search_room_query();
            if (TravelHelper::is_wpml()) {
                global $sitepress;
                $sitepress->switch_lang($current_lang, true);
            }			 
            return $query;
        }
        function alter_search_room_query()
        {
		    add_filter('pre_get_posts', [$this, '_change_room_pre_get_posts']);
            add_filter('posts_join', [$this, '_room_get_join_query_hotel_room']);
        }
        function remove_search_room_query()
        {
            remove_filter('pre_get_posts', [$this, '_change_room_pre_get_posts']);
            remove_filter('posts_join', [$this, '_room_get_join_query_hotel_room']);
        }
        public function _change_room_pre_get_posts($query){
            $query->set('author', '');
            return $query;
        }
        function _room_get_join_query_hotel_room ($join){
            // $query = self::get_max_size_room($hotel_id, $total_pax);
            global $varmaxsize;
            if (STInput::request('room_parent', 0) != 0){
            $hotel_id = STInput::request('room_parent', 0); 
            }else{
            $hotel_id = get_the_ID(); 
            }
            global $wpdb;
            $table = $wpdb->prefix . 'hotel_room';
            $table1 = $wpdb->prefix . 'link_hotel_room';
            $join .= " INNER JOIN {$table} as tb ON {$wpdb->prefix}posts.ID = tb.post_id ";
            $join .= " INNER JOIN {$table1} as tbb ON {$wpdb->prefix}posts.ID = tbb.post_id AND tbb.room_parent = ".$hotel_id." AND tb.adult_number = ".$varmaxsize."";
            return $join;
		}
        function _room_get_join_query($join)
        {
            //if (!TravelHelper::checkTableDuplicate('st_hotel')) return $join;
            global $wpdb;
            $table = $wpdb->prefix . 'st_room_availability';
            $join .= " INNER JOIN {$table} as tb ON {$wpdb->prefix}posts.ID = tb.post_id";
            return $join;
        }
        public function _room_change_post_fields($fields)
        {
            $fields .= ", SUM(CAST(CASE WHEN IFNULL(tb.adult_price, 0) = 0 THEN tb.price ELSE tb.adult_price END AS DECIMAL)) as st_price, COUNT(tb.id) as total_available ";
            return $fields;
        }
        public function _room_change_posts_groupby($groupby) 
        {
            global $wpdb;
            if (!$groupby or strpos($wpdb->posts . '.ID', $groupby) === false) {
                //$post_id        = get_the_ID();
                $post_id = STInput::post('room_parent', get_the_ID());
                $post_id = TravelHelper::post_origin($post_id);
                $check_in = strtotime(TravelHelper::convertDateFormat(STInput::request('start')));
                $check_out = strtotime(TravelHelper::convertDateFormat(STInput::request('end')));
                $allow_full_day = get_post_meta($post_id, 'allow_full_day', true);
                $diff = STDate::timestamp_diff_day($check_in, $check_out);
                $max_day = $allow_full_day != 'off' ? $diff + 1 : $diff;
                $groupby .= $wpdb->prepare($wpdb->posts . '.ID HAVING total_available >=%d ', $max_day);
            }
            return $groupby;
        }
        /**
         * NOTE Alter the main search query
         */     
        public function _alter_search_query_ajax($where)
        {
            global $wpdb;
            $hotel_id = STInput::post('room_parent', get_the_ID());
            $hotel_origin = TravelHelper::post_origin($hotel_id);
            $sql = $wpdb->prepare(' AND parent_id = %d ', $hotel_origin);
            if (STInput::request('start') and STInput::request('end')) {
                $check_in = strtotime(TravelHelper::convertDateFormat(STInput::request('start')));
                $check_out = strtotime(TravelHelper::convertDateFormat(STInput::request('end')));
                $adult_num = STInput::request('adult_number', 0);
                $child_num = STInput::request('child_number', 0);
                $number_room = STInput::request('room_num_search', 0);
                $allow_full_day = get_post_meta($hotel_origin, 'allow_full_day', true);
                // deprecated version
                // $whereNumber = " AND parent_id = ".$hotel_id." AND check_in <= %d AND (number  - IFNULL(number_booked, 0)) >= %d";
                $whereNumber = " AND parent_id = ".$hotel_id." AND periode_from <= %d AND (number  - IFNULL(number_booked, 0)) >= %d";
                if ($allow_full_day == 'off') {
                    $whereNumber = "AND periode_from < %d AND (number  - IFNULL(number_booked, 0) + IFNULL(number_end, 0)) >= %d";
                }
                $sql2 = "
                        AND periode_from >= %d
                        {$whereNumber}
                        AND status = 'available'
                    ";
                $sql .= $wpdb->prepare($sql2, $check_in, $check_out, $number_room);
            }
            $where .= $sql;
            return $where;
        }
        function get_search_arg($param)
        {
            $default = [
                's' => false
            ];
            extract(wp_parse_args($param, $default));
            $arg = [];
            return $arg;
        }
        function choose_search_template($template)
        {
            global $wp_query;
            $post_type = get_query_var('post_type');
            if ($wp_query->is_search && $post_type == 'st_hotel') {
                return locate_template('search-hotel.php');  //  redirect to archive-search.php
            }
            return $template;
        }
        function _alter_search_query($where)
        {
            if (is_admin()) return $where;
            global $wp_query;
            if (is_search()) {
                $post_type = $wp_query->query_vars['post_type'];
                if ($post_type == 'st_hotel' and is_search()) {
                    //Alter From NOW
                    global $wpdb;
                    $check_in = STInput::get('start');
                    $check_out = STInput::get('end');
                    //Alter WHERE for check in and check out
                    if ($check_in and $check_out) {
                        $check_in = @date('Y-m-d', strtotime(TravelHelper::convertDateFormat($check_in)));
                        $check_out = @date('Y-m-d', strtotime(TravelHelper::convertDateFormat($check_out)));
                        $check_in = esc_sql($check_in);
                        $check_out = esc_sql($check_out);
                        $where .= " AND $wpdb->posts.ID in ((SELECT {$wpdb->postmeta}.meta_value
                        FROM {$wpdb->postmeta}
                        WHERE {$wpdb->postmeta}.meta_key='room_parent'
                        AND  {$wpdb->postmeta}.post_id NOT IN(
                            SELECT room_id FROM (
                                SELECT count(st_meta6.meta_value) as total,
                                    st_meta5.meta_value as total_room,st_meta6.meta_value as room_id ,st_meta2.meta_value as check_in,st_meta3.meta_value as check_out
                                     FROM {$wpdb->posts}
                                            JOIN {$wpdb->postmeta}  as st_meta2 on st_meta2.post_id={$wpdb->posts}.ID and st_meta2.meta_key='check_in'
                                            JOIN {$wpdb->postmeta}  as st_meta3 on st_meta3.post_id={$wpdb->posts}.ID and st_meta3.meta_key='check_out'
                                            JOIN {$wpdb->postmeta}  as st_meta6 on st_meta6.post_id={$wpdb->posts}.ID and st_meta6.meta_key='room_id'
                                            JOIN {$wpdb->postmeta}  as st_meta5 on st_meta5.post_id=st_meta6.meta_value and st_meta5.meta_key='number_room'
                                            WHERE {$wpdb->posts}.post_type='st_order'
                                    GROUP BY st_meta6.meta_value HAVING total>=total_room AND (
                                                ( CAST(st_meta2.meta_value AS DATE)<'{$check_in}' AND  CAST(st_meta3.meta_value AS DATE)>'{$check_in}' )
                                                OR ( CAST(st_meta2.meta_value AS DATE)>='{$check_in}' AND  CAST(st_meta2.meta_value AS DATE)<='{$check_out}' )
                                    )
                            ) as room_booked
                        )
                    ))";
                    }
                    if ($price_range = STInput::request('price_range_')) {
                        // price_range_ ???
                        $price_obj = explode(';', $price_range);
                        // convert to default money
                        $price_obj[0] = TravelHelper::convert_money_to_default($price_obj[0]);
                        $price_obj[1] = TravelHelper::convert_money_to_default($price_obj[1]);
                        if (!isset($price_obj[1])) {
                            $price_from = 0;
                            $price_to = $price_obj[0];
                        } else {
                            $price_from = $price_obj[0];
                            $price_to = $price_obj[1];
                        }
                        global $wpdb;
                        $query = " AND {$wpdb->posts}.ID IN (
                                SELECT ID FROM
                                (
                                    SELECT ID, MIN(min_price) as min_price_new FROM
                                    (
                                    select {$wpdb->posts}.ID,
                                    IF(
                                        st_meta3.meta_value is not NULL,
                                        IF((st_meta2.meta_value = 'on' and CAST(st_meta5.meta_value as DATE)<=NOW() and CAST(st_meta4.meta_value as DATE)>=NOW()) or
                                        st_meta2.meta_value='off'
                                        ,
                                        st_meta1.meta_value-(st_meta1.meta_value/100)*st_meta3.meta_value,
                                        CAST(st_meta1.meta_value as DECIMAL)
                                        ),
                                        CAST(st_meta1.meta_value as DECIMAL)
                                    ) as min_price
                                    from {$wpdb->posts}
                                    JOIN {$wpdb->postmeta} on {$wpdb->postmeta}.meta_value={$wpdb->posts}.ID and {$wpdb->postmeta}.meta_key='room_parent'
                                    JOIN {$wpdb->postmeta} as st_meta1 on st_meta1.post_id={$wpdb->postmeta}.post_id AND st_meta1.meta_key='price'
                                    LEFT JOIN {$wpdb->postmeta} as st_meta2 on st_meta2.post_id={$wpdb->postmeta}.post_id AND st_meta2.meta_key='is_sale_schedule'
                                    LEFT JOIN {$wpdb->postmeta} as st_meta3 on st_meta3.post_id={$wpdb->postmeta}.post_id AND st_meta3.meta_key='discount_rate'
                                    LEFT JOIN {$wpdb->postmeta} as st_meta4 on st_meta4.post_id={$wpdb->postmeta}.post_id AND st_meta4.meta_key='sale_price_to'
                                    LEFT JOIN {$wpdb->postmeta} as st_meta5 on st_meta5.post_id={$wpdb->postmeta}.post_id AND st_meta5.meta_key='sale_price_from'
                                     )as min_price_table
                                    group by ID Having  min_price_new>=%d and min_price_new<=%d ) as min_price_table_new
                                ) ";
                        $query = $wpdb->prepare($query, $price_from, $price_to);
                        $where .= $query;
                    }
                }
            }
            return $where;
        }
        function _get_join_query($join)
        {
            //if (!TravelHelper::checkTableDuplicate('st_hotel')) return $join;
            global $wpdb;
            $table  = $wpdb->prefix . 'st_room_availability';
            $table2 = $wpdb->prefix . 'st_hotel';
            $table3 = $wpdb->prefix . 'hotel_room';
            $disable_avai_check = st()->get_option('disable_availability_check', 'on');
            if ($disable_avai_check == 'on') {
                $join .= " INNER JOIN {$table} as tb ON {$wpdb->prefix}posts.ID = tb.post_id AND status = 'available'";
               // $join .= " INNER JOIN {$table3} as tb3 ON tb.post_id = tb3.post_id";
            }
            $join .= " INNER JOIN {$table2} as tb2 ON {$wpdb->prefix}posts.ID = tb2.post_id";
            return $join;
        }
        /**
         * NOTE main search query
         */
        function _get_where_query($where)
        {
            global $wpdb, $st_search_args;
            if (!$st_search_args) $st_search_args = $_REQUEST;
            if (!empty($st_search_args['st_location'])) {
                if (empty($st_search_args['only_featured_location']) or $st_search_args['only_featured_location'] == 'no')
                    $st_search_args['location_id'] = $st_search_args['st_location'];
            }
            if (isset($st_search_args['location_id']) && !empty($st_search_args['location_id'])) {
                $location_id = $st_search_args['location_id'];
                $where = TravelHelper::_st_get_where_location($location_id, ['st_hotel'], $where);
            } elseif (isset($_REQUEST['location_name']) && !empty($_REQUEST['location_name'])) {
                $location_name = STInput::request('location_name', '');
                $ids_location = TravelerObject::_get_location_by_name($location_name);
                if (!empty($ids_location) && is_array($ids_location)) {
                    $where .= TravelHelper::_st_get_where_location($ids_location, ['st_hotel'], $where);
                } else {
                    $where .= " AND (tb2.address LIKE '%{$location_name}%'";
                    $where .= " OR {$wpdb->prefix}posts.post_title LIKE '%{$location_name}%')";
                }
            }
            if (isset($_REQUEST['item_name']) && !empty($_REQUEST['item_name'])) {
                $item_name = STInput::request('item_name', '');
                $where .= " AND {$wpdb->prefix}posts.post_title LIKE '%{$item_name}%'";
            }
            if (isset($_REQUEST['item_id']) and !empty($_REQUEST['item_id'])) {
                $item_id = STInput::request('item_id', '');
                $where .= " AND ({$wpdb->prefix}posts.ID = '{$item_id}')";
            }
            $check_in = STInput::get('start', '');
            $check_out = STInput::get('end', '');
            if(!empty($check_in) && !empty($check_out)){
                $check_in = date('Y-m-d', strtotime(TravelHelper::convertDateFormat($check_in)));
                $check_out = date('Y-m-d', strtotime(TravelHelper::convertDateFormat($check_out)));
                $check_in_stamp = strtotime($check_in);
                $check_out_stamp = strtotime($check_out);
            }else{
                $check_in = date('Y-m-d');
                $check_out = date('Y-m-d', strtotime('+1 day'));
                $check_in_stamp = strtotime($check_in);
                $check_out_stamp = strtotime($check_out);
            }
            if ($check_in && $check_out) {
                $today = date('m/d/Y');
                $period = STDate::dateDiff($today, $check_in);
                $room_num_search = STInput::get('room_num_search', 0);
                $resultroom = array();
                $arr_adult_number = array();
                $arr_child_number = array();
                $total_pax  = array();
                for ($nbR = 1; $nbR <= $room_num_search; $nbR++)
                {
                    $nb_adult   = 0;
                    $nb_children = 0;
                    $all_ages = array();
                    if(STInput::request( 'child_number'.$nbR ) > 0) {
                        for ($x = 1; $x <= STInput::request( 'child_number'.$nbR ); $x++) {
                            $all_ages[] = STInput::request( 'age_number_'.$nbR.'_'.$x);
                        }
                    }
                    $total_pax[$nbR] = STInput::request( 'adult_number'.$nbR ) +  STInput::request( 'child_number'.$nbR );
                    /* 
                    $adult_number = 'adult_number_'.$nbR;
                    $child_number = 'child_number_'.$nbR;
                    $resultroom[$nbR]['room_num_search']    = STInput::request( 'room_num_search' );
                    $resultroom[$nbR][$adult_number]        = STInput::request('adult_number'.$nbR );
                    if( STInput::request('adult_number'.$nbR ) > 0 ) {
                        $arr_adult_number['number_adult'] += STInput::request('adult_number'.$nbR );
                        $nb_adult += STInput::request('adult_number'.$nbR );
                    } 
                    $resultroom[$nbR][$child_number] = STInput::request('child_number'.$nbR );
                    if( STInput::request( 'child_number'.$nbR ) > 0) {
                        $arr_child_number['number_child'] += STInput::request( 'child_number'.$nbR );
                        // fetching childrens for age
                        for ($nbC = 1; $nbC <= STInput::request( 'child_number'.$nbR ); $nbC++) {
                            $age_child      = 'age_number_'.$nbR.'_'.$nbC;
                            $resultroom[$nbR][$age_child] = STInput::request( 'age_number_'.$nbR.'_'.$nbC);
                        }
                    } // end if
                    */
                }
                $disable_avai_check = st()->get_option('disable_availability_check', 'on');
                // the pax logic search / room
                if($total_pax){
                    $include_list_hotel = $this->get_rooms_hotel($total_pax);
                    if( !empty($include_list_hotel) ){
                        $where .= " AND {$wpdb->prefix}posts.ID IN ({$include_list_hotel}) ";
                    }
                }
                $where .= " AND tb.periode_from <= {$check_in_stamp} AND tb.periode_to >= {$check_out_stamp} ";
                $where .= " AND CAST(tb2.hotel_booking_period AS UNSIGNED) <= {$period}";
            } else {
                $disable_avai_check = st()->get_option('disable_availability_check', 'on');
                if ($disable_avai_check == 'on') {
                    $where .= " AND check_in >= UNIX_TIMESTAMP(CURRENT_DATE) ";
                }
            }
            if (isset($_REQUEST['range']) and isset($_REQUEST['location_id'])) {
                $range = STInput::get('range', '0;5');
                $rangeobj = explode(';', $range);
                $range_min = $rangeobj[0];
                $range_max = $rangeobj[1];
                $location_id = STInput::request('location_id');
                $post_type = get_query_var('post_type');
                $map_lat = (float)get_post_meta($location_id, 'map_lat', true);
                $map_lng = (float)get_post_meta($location_id, 'map_lng', true);
                global $wpdb;
                $where .= "
                AND $wpdb->posts.ID IN (
                        SELECT ID FROM (
                            SELECT $wpdb->posts.*,( 6371 * acos( cos( radians({$map_lat}) ) * cos( radians( mt1.meta_value ) ) *
                                            cos( radians( mt2.meta_value ) - radians({$map_lng}) ) + sin( radians({$map_lat}) ) *
                                            sin( radians( mt1.meta_value ) ) ) ) AS distance
                                                FROM $wpdb->posts, $wpdb->postmeta as mt1,$wpdb->postmeta as mt2
                                                WHERE $wpdb->posts.ID = mt1.post_id
                                                and $wpdb->posts.ID=mt2.post_id
                                                AND mt1.meta_key = 'map_lat'
                                                and mt2.meta_key = 'map_lng'
                                                AND $wpdb->posts.post_status = 'publish'
                                                AND $wpdb->posts.post_type = '{$post_type}'
                                                AND $wpdb->posts.post_date < NOW()
                                                GROUP BY $wpdb->posts.ID HAVING distance >= {$range_min} and distance <= {$range_max}
                                                ORDER BY distance ASC
                        ) as st_data
	            )";
            }
            return $where;
        }
        static function get_rooms_hotel($total_pax){
            global $wpdb;
            $total_pax = max($total_pax);
            $sql ="SELECT L.room_parent FROM `{$wpdb->prefix}link_hotel_room` as L INNER JOIN `{$wpdb->prefix}hotel_room` as R ON L.post_id = R.post_id WHERE R.adult_number >= {$total_pax}";
            $list_hotel = $wpdb->get_results($sql, ARRAY_A);
            $included_hotels = array();
            foreach($list_hotel as $hotel){
                $included_hotels[] = $hotel["room_parent"];
            }
            $included_hotels = implode(", ", $included_hotels);
            return $included_hotels;
        }
        /**
         * @update 1.1.8
         */
        function _get_where_query_tab_location($where)
        {
            $location_id = get_the_ID();
            if (!TravelHelper::checkTableDuplicate('st_hotel')) return $where;
            if (!empty($location_id)) {
                $where = TravelHelper::_st_get_where_location($location_id, ['st_hotel'], $where);
            }
            return $where;
        }
        // NOTE Second main search query
        function alter_search_query()
        {
            add_action('pre_get_posts', [$this, 'change_search_hotel_arg']);
            // No need to do this
            add_action('posts_fields', [$this, '_change_posts_fields']);
            add_filter('posts_where', [$this, '_get_where_query']);
            add_filter('posts_join', [$this, '_get_join_query']);
            add_filter('posts_orderby', [$this, '_get_order_by_query']);
            add_filter('posts_groupby', [$this, '_change_posts_groupby']);
        }
        function remove_alter_search_query()
        {
            remove_action('pre_get_posts', [$this, 'change_search_hotel_arg']);
            remove_action('posts_fields', [$this, '_change_posts_fields']);
            remove_filter('posts_where', [$this, '_get_where_query']);
            remove_filter('posts_join', [$this, '_get_join_query']);
            remove_filter('posts_orderby', [$this, '_get_order_by_query']);
            remove_filter('posts_groupby', [$this, '_change_posts_groupby']);
        }
        public function _change_posts_fields($fields)
        {
            global $wpdb;
            $disable_avai_check = st()->get_option('disable_availability_check', 'off');
            if ($disable_avai_check == 'off') {
                $fields .= ', min(CAST(CASE WHEN IFNULL(tb.adult_price, 0) = 0 THEN tb.price ELSE tb.adult_price END AS DECIMAL) ) as st_price';
            } else {
                if (self::is_show_min_price()) {
                    // NOTE changed to match the new database structure
                    $fields .= ', min(CAST(tb.price as DECIMAL)) as st_price';
                } else {
                    $fields .= ', min(CAST(tb.price as DECIMAL)) as st_price';
                }
            }
            return $fields;
        }
        public function _change_posts_groupby($groupby)
        {
            global $wpdb;
            //if ( !$groupby or strpos( $wpdb->posts . '.ID', $groupby ) === false ) {
            $groupby = $wpdb->posts . '.ID ';
            if (isset($_REQUEST['price_range']) && !empty($_REQUEST['price_range'])) {
                $groupby .= " HAVING ";
                $meta_key = st()->get_option('hotel_show_min_price', 'avg_price');
                if ($meta_key == 'avg_price') $meta_key = "price_avg";
                $price = STInput::get('price_range', '0;0');
                $priceobj = explode(';', $price);
                // convert to default money
                $priceobj[0] = TravelHelper::convert_money_to_default($priceobj[0]);
                $priceobj[1] = TravelHelper::convert_money_to_default($priceobj[1]);
                $groupby .= $wpdb->prepare(" st_price >= %f ", $priceobj[0]);
                if (isset($priceobj[1])) {
                    $groupby .= $wpdb->prepare(" AND st_price <= %f ", $priceobj[1]);
                }
            }
            // }
            return $groupby;
        }
        function change_search_hotel_arg($query)
        {
            if (is_admin() and empty($_REQUEST['is_search_map']) and empty($_REQUEST['is_search_page'])) return $query;
            /**
             * Global Search Args used in Element list and map display
             * @since 1.2.5
             */
            global $st_search_args;
            if (!$st_search_args) $st_search_args = $_REQUEST;
            $post_type = get_query_var('post_type');
            $posts_per_page = st()->get_option('hotel_posts_per_page', 12);
            if ($post_type == 'st_hotel') {
                $query->set('author', '');
                if (STInput::get('item_name')) {
                    $query->set('s', STInput::get('item_name'));
                }
                if ((empty($_REQUEST['is_search_map']) && empty($query->query['is_st_location_list_hotel'])) or !empty($_REQUEST['is_search_page'])) {
                    $query->set('posts_per_page', $posts_per_page);
                }
                $has_tax_in_element = [];
                if (is_array($st_search_args)) {
                    foreach ($st_search_args as $key => $val) {
                        if (strpos($key, 'taxonomies--') === 0 && !empty($val)) {
                            $has_tax_in_element[$key] = $val;
                        }
                    }
                }
                if (!empty($has_tax_in_element)) {
                    $tax_query = [];
                    foreach ($has_tax_in_element as $tax => $value) {
                        $tax_name = str_replace('taxonomies--', '', $tax);
                        if (!empty($value)) {
                            $value = explode(',', $value);
                            $tax_query[] = [
                                'taxonomy' => $tax_name,
                                'terms' => $value,
                                'operator' => 'IN',
                            ];
                        }
                    }
                    if (!empty($tax_query)) {
                        $query->set('tax_query', $tax_query);
                    }
                }
                $tax = STInput::request('taxonomy');
                if (!empty($tax) and is_array($tax)) {
                    $tax_query = [];
                    foreach ($tax as $key => $value) {
                        if ($value) {
                            $value = explode(',', $value);
                            if (!empty($value) and is_array($value)) {
                                foreach ($value as $k => $v) {
                                    if (!empty($v)) {
                                        $ids[] = $v;
                                    }
                                }
                            }
                            if (!empty($ids)) {
                                $tax_query[] = [
                                    'taxonomy' => $key,
                                    'terms' => $ids,
                                    //'COMPARE'=>"IN",
                                    'operator' => 'IN',
                                ];
                            }
                            $ids = [];
                        }
                    }
                    $query->set('tax_query', $tax_query);
                }
                /**
                 * Post In and Post Order By from Element
                 * @since  1.2.5
                 * @author quandq
                 */
                if (!empty($st_search_args['st_number_ht'])) {
                    $query->set('posts_per_page', $st_search_args['st_number_ht']);
                }
                if (!empty($st_search_args['st_ids'])) {
                    $query->set('post__in', explode(',', $st_search_args['st_ids']));
                    $query->set('orderby', 'post__in');
                }
                if (!empty($st_search_args['st_orderby']) and $st_orderby = $st_search_args['st_orderby']) {
                    if ($st_orderby == 'sale') {
                        $query->set('meta_key', 'total_sale_number');
                        $query->set('orderby', 'meta_value_num');
                    }
                    if ($st_orderby == 'rate') {
                        $query->set('meta_key', 'rate_review');
                        $query->set('orderby', 'meta_value');
                    }
                    if ($st_orderby == 'discount') {
                        $query->set('meta_key', 'get_hotel_price_rate');
                        $query->set('orderby', 'meta_value_num');
                    }
                    if ($st_orderby == 'featured') {
                        $query->set('meta_key', 'is_featured');
                        $query->set('orderby', 'meta_value');
                        $query->set('order', 'DESC');
                    }
                }
                if (!empty($st_search_args['sort_taxonomy']) and $sort_taxonomy = $st_search_args['sort_taxonomy']) {
                    if (isset($st_search_args["id_term_" . $sort_taxonomy])) {
                        $id_term = $st_search_args["id_term_" . $sort_taxonomy];
                        $tax_query[] = [
                            [
                                'taxonomy' => $sort_taxonomy,
                                'field' => 'id',
                                'terms' => explode(',', $id_term),
                                'include_children' => false
                            ],
                        ];
                    }
                }
                if (!empty($meta_query)) {
                    $query->set('meta_query', $meta_query);
                }
                if (!empty($tax_query)) {
                    $query->set('tax_query', $tax_query);
                }
            }
        }
        /**
         * since 1.2.4
         *
         *
         */
        function _get_order_by_query($orderby)
        {
            if(strpos($orderby, "FIELD(") !== false && (strpos($orderby, "posts.ID") !== false)){
                return $orderby;
            }
            if ($check = STInput::get('orderby')) {
                global $wpdb;
                $meta_key = st()->get_option('hotel_show_min_price', 'avg_price');
                if ($meta_key == 'avg_price') $meta_key = "price_avg";
                switch ($check) {
                    case "price_asc":
                        $orderby = ' st_price asc';
                        break;
                    case "price_desc":
                        $orderby = ' st_price desc';
                        break;
                    case "name_asc":
                        $orderby = $wpdb->posts . '.post_title';
                        break;
                    case "name_desc":
                        $orderby = $wpdb->posts . '.post_title desc';
                        break;
                    case "rand":
                        $orderby = ' rand()';
                        break;
                    case "new":
                        $orderby = $wpdb->posts . '.post_modified desc';
                        break;
                    default:
                        $is_featured = st()->get_option('is_featured_search_hotel', 'off');
                        if (!empty($is_featured) and $is_featured == 'on') {
                            $orderby = 'tb2.is_featured desc';
                        } else {
                            $orderby = $wpdb->posts . '.post_modified desc';
                        }
                        break;
                }
            } else {
                global $wpdb;
                $is_featured = st()->get_option('is_featured_search_hotel', 'off');
                if (!empty($is_featured) and $is_featured == 'on') {
                    $orderby = 'tb2.is_featured desc';
                } else {
                    $orderby = $wpdb->posts . '.post_modified desc';
                }
            }
            return $orderby;
        }
        //Helper class
        function get_last_booking()
        {
            if ($this->hotel_id == false) {
                $this->hotel_id = get_the_ID();
            }
            global $wpdb;
            $query = "SELECT * from " . $wpdb->postmeta . "
                where meta_key='item_id'
                and meta_value in (
                    SELECT ID from {$wpdb->posts}
                    join " . $wpdb->postmeta . " on " . $wpdb->posts . ".ID=" . $wpdb->postmeta . ".post_id and " . $wpdb->postmeta . ".meta_key='room_parent'
                    where post_type='hotel_room'
                    and " . $wpdb->postmeta . ".meta_value='" . $this->hotel_id . "'
                )
                order by meta_id
                limit 0,1";
            $data = $wpdb->get_results($query, OBJECT);
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    return human_time_diff(get_the_time('U', $value->post_id), current_time('timestamp')) . __(' ago', ST_TEXTDOMAIN);
                }
            }
        }
        static function count_meta_key($key, $value, $post_type = 'st_hotel', $location_key = 'multi_location')
        {
            $arg = [
                'post_type' => $post_type,
                'posts_per_page' => 1,
            ];
            if (STInput::request('location_id')) {
                $arg['meta_query'][] = [
                    'key' => $location_key,
                    'value' => STInput::request('location_id')
                ];
            }
            if ($key == 'rate_review') {
                $arg['meta_query'][] = [
                    'key' => $key,
                    'value' => $value,
                    'type' => 'DECIMAL',
                    'compare' => '>='
                ];
            } else {
                $arg['meta_key'] = $key;
                $arg['meta_value'] = $value;
            }
            $query = new WP_Query(
                $arg
            );
            $count = $query->found_posts;
            wp_reset_query();
            return $count;
        }
        static function get_avg_price($post_id = false)
        {
            if (!$post_id) {
                $post_id = get_the_ID();
            }
            $price = get_post_meta($post_id, 'price_avg', true);
            $price = apply_filters('st_apply_tax_amount', $price);
            return $price;
        }
        /**
         * Get Hotel price for listing and single page
         * 
         * @since 1.1.1
         * */
        static function get_price($hotel_id = false)  
        {
            if (!$hotel_id) $hotel_id = get_the_ID();
            if (self::is_show_min_price()) {
                return (float)get_post_meta($hotel_id, 'min_price', true);
                $min_price = HotelHelper::get_minimum_price_hotel($hotel_id);
                $min_price = apply_filters('st_apply_tax_amount', $min_price);
                return $min_price;
            } else {
                return (float)get_post_meta($hotel_id, 'price_avg', true);
                return HotelHelper::get_avg_price_hotel($hotel_id);
            }
        }
        /**
         * NOTE this function shows the new price according to the new database structure
        */
		static function get_hotel_price($hotel_id = false)  
        {
            if (!$hotel_id) $hotel_id = get_the_ID();

            $min_price = HotelHelper::get_minimum_price_hotel_rom($hotel_id); 
            $min_price = apply_filters('st_apply_tax_amount', $min_price);
            return $min_price;    
        }
        /**
         * NOTE get discount
         * with comparing dates to those stored in the database
         */
        static function get_discount_by_period($hotel_id, $today = false){
            global $wpdb;
            if ( empty( $hotel_id ) ) $hotel_id = get_the_ID();
                $sql = "
                    SELECT discount
                    FROM {$wpdb->prefix}st_room_availability
                    WHERE
                    post_id = '".$hotel_id."'
                    AND 
                    status = 'available'
                    AND
                    discount != ''
                ";
            // manage the discount of actual period
            if($today){
                // DUMP
                // $today = strtotime(date("05/01/2021"));
                $sql = "
                    SELECT discount
                    FROM {$wpdb->prefix}st_room_availability
                    WHERE
                    post_id = '".$hotel_id."'
                    AND 
                    status = 'available'
                    AND
                    discount != ''
                    AND 
                    (
                        periode_from <= ".$today."
                        AND 
                        periode_to >= ".$today."
                    )
                ";
            }
            $discount = $wpdb->get_row( $sql );
            if($discount){
                return $discount->discount;
            }
        }
        /**
         * NOTE function used to get the stock of a single periode id
         */
        static function get_stock_periode_id($period_id){
            global $wpdb;
            if ( empty( $hotel_id ) ) $hotel_id = get_the_ID();
                $sql = "
                    SELECT stock_chambres
                    FROM {$wpdb->prefix}st_room_availability
                    WHERE
                    id = '".$period_id."'
                ";
                $stock_chambres = $wpdb->get_col( $sql );
                return $stock_chambres[0];
        }
        /**
         * NOTE function used to update stock in database
         */
        static function update_stock_periode_id($period_id, $num_rooms){
            global $wpdb;
            if ( empty( $hotel_id ) ) $hotel_id = get_the_ID();
                $new_stock = self::get_stock_periode_id($period_id);
                $new_stock -= $num_rooms;
                $sql = "
                    UPDATE {$wpdb->prefix}st_room_availability
                    SET `stock_chambres` = '".$new_stock."'
                    WHERE
                    id = '".$period_id."'
                ";
                $new_stock = $wpdb->query( $sql );
                return $new_stock;
        }
        /**
         * NOTE get msg_promotion
         * with comparing dates to those stored in the database
         */
        static function get_msg_promotion($hotel_id, $today = false){
            global $wpdb;
            if ( empty( $hotel_id ) ) $hotel_id = get_the_ID();
                $sql = "
                    SELECT msg_promotion
                    FROM {$wpdb->prefix}st_room_availability
                    WHERE
                        post_id = '".$hotel_id."'
                    AND 
                        status = 'available'
                    AND
                        discount != ''
                ";
            // manage the discount of actual period
            if($today){
                // DUMP
                // $today = strtotime(date("05/01/2021"));
                $sql = "
                    SELECT msg_promotion
                    FROM {$wpdb->prefix}st_room_availability
                    WHERE
                        post_id = '".$hotel_id."'
                    AND 
                        status = 'available'
                    AND
                        discount != ''
                    AND 
                    (
                        periode_from <= ".$today."
                        AND 
                        periode_to >= ".$today."
                    )
                ";
            }
            $msg_promotion = $wpdb->get_row( $sql );
            if($msg_promotion){
                return $msg_promotion->msg_promotion;
            }
        }
        /**
         * NOTE apply discount on price
         */
        static function supp_single_calculation($price, $supp_single){
            if(($supp_single != '' ) || ($supp_single > 0))
            {
                $price = $price + $supp_single;
            }
            return $price;
        }
        /**
         * NOTE apply discount on price
         */
        static function discount_calculation($price, $discount){
            if(($discount != '' ) || ($discount > 0))
            {
                $price = $price - ($price * ($discount/100));
            }
            return $price;
        }
        static function marge_calculation($price, $marge){
            if($marge > 0)
            {
                $price =  $price + ($price * ($marge/100));
            }
            return $price;
        }
        /**
         * NOTE get all hotel data
         */
        static function dumpHotelData($post_id){
            global $wpdb;
            $sql = "SELECT * FROM {$wpdb->prefix}st_room_availability WHERE post_id = {$post_id}";
            $data = $wpdb->get_results($sql);
            return $data;
        }
        /**
         * Check if Traveler Setting show min price instead avg price
         *
         * @since 1.1.1
         * */
        static function is_show_min_price()
        {
            $show_min_or_avg = st()->get_option('hotel_show_min_price', 'avg_price');
            if ($show_min_or_avg == 'min_price') return true;
            return false;
        }
        /**
         * NOTE get status
         */
        static function get_status_order($order_id){
            global $wpdb;
            if ( empty( $order_id ) ) $order_id = get_the_ID();
            $sql = "
                SELECT status
                FROM {$wpdb->prefix}st_order_item_meta
                WHERE
                order_item_id = '".$order_id."'
            ";
            $status = $wpdb->get_row( $sql );
            if($status){
                return $status->status;
            }
        }
        /**
         *
         * Base on all room price
         *
         * @deprecate this function is no longer work
         *
         *
         * */
        static function get_min_price($post_id = false)
        {
            if (!$post_id) {
                $post_id = get_the_ID();
            }
            $query = [
                'post_type' => 'hotel_room',
                'posts_per_page' => 100,
                'meta_key' => 'room_parent',
                'meta_value' => $post_id
            ];
            $q = new WP_Query($query);
            $min_price = 0;
            $i = 1;
            while ($q->have_posts()) {
                $q->the_post();
                $price = get_post_meta(get_the_ID(), 'price', true);
                if ($i == 1) {
                    $min_price = $price;
                } else {
                    if ($price < $min_price) {
                        $min_price = $price;
                    }
                }
                $i++;
            }
            wp_reset_postdata();
            return apply_filters('st_apply_tax_amount', $min_price);
        }
        function _change_search_result_link($url)
        {
            $page_id = st()->get_option('hotel_search_result_page');
            if ($page_id) {
                $url = get_permalink($page_id);
            }
            return $url;
        }
        static function get_min_max_price($post_type = 'st_hotel')
        {
            $meta_key = st()->get_option('hotel_show_min_price', 'avg_price');
            if ($meta_key == 'avg_price') $meta_key = "price_avg";
            if (empty($post_type) || !TravelHelper::checkTableDuplicate($post_type)) {
                return ['price_min' => 0, 'price_max' => 500];
            }
            global $wpdb;
            $sql = "
                select
                    min(CAST({$meta_key} as DECIMAL)) as min,
                    max(CAST({$meta_key} as DECIMAL)) as max
                from {$wpdb->prefix}st_hotel";
            $results = $wpdb->get_results($sql, OBJECT);
            $price_min = $results[0]->min;
            $price_max = $results[0]->max;
            if (empty($price_min)) $price_min = 0;
            if (empty($price_max)) $price_max = 500;
            return ['min' => ceil($price_min), 'max' => ceil($price_max)];
        }
        static function get_price_slider()
        {
            global $wpdb;
            $query = "SELECT min(orgin_price) as min_price,MAX(orgin_price) as max_price from
                (SELECT
                 IF( st_meta3.meta_value is not NULL,
                    IF((st_meta2.meta_value = 'on' and CAST(st_meta5.meta_value as DATE)<=NOW() and CAST(st_meta4.meta_value as DATE)>=NOW())
                      or st_meta2.meta_value='off' ,
                      {$wpdb->postmeta}.meta_value-({$wpdb->postmeta}.meta_value/100)*st_meta3.meta_value,
                      CAST({$wpdb->postmeta}.meta_value as DECIMAL) ),
                  CAST({$wpdb->postmeta}.meta_value as DECIMAL) ) as orgin_price
                  FROM {$wpdb->postmeta}
                  JOIN {$wpdb->postmeta} as st_meta1 on st_meta1.post_id={$wpdb->postmeta}.post_id
                  LEFT JOIN {$wpdb->postmeta} as st_meta2 on st_meta2.post_id={$wpdb->postmeta}.post_id AND st_meta2.meta_key='is_sale_schedule'
                  LEFT JOIN {$wpdb->postmeta} as st_meta3 on st_meta3.post_id={$wpdb->postmeta}.post_id AND st_meta3.meta_key='discount_rate'
                  LEFT JOIN {$wpdb->postmeta} as st_meta4 on st_meta4.post_id={$wpdb->postmeta}.post_id AND st_meta4.meta_key='sale_price_to'
                  LEFT JOIN {$wpdb->postmeta} as st_meta5 on st_meta5.post_id={$wpdb->postmeta}.post_id AND st_meta5.meta_key='sale_price_from'
                  WHERE st_meta1.meta_key='room_parent' AND {$wpdb->postmeta}.meta_key='price')
        as orgin_price_table";
            $data = $wpdb->get_row($query);
            $min = apply_filters('st_apply_tax_amount', $data->min_price);
            $max = apply_filters('st_apply_tax_amount', $data->max_price);
            return ['min' => floor($min), 'max' => ceil($max)];
        }
        static function get_owner_email($hotel_id = false)
        {
            $theme_option = st()->get_option('partner_show_contact_info');
            $metabox = get_post_meta($hotel_id, 'show_agent_contact_info', true);
            $use_agent_info = false;
            if ($theme_option == 'on') $use_agent_info = true;
            if ($metabox == 'user_agent_info') $use_agent_info = true;
            if ($metabox == 'user_item_info') $use_agent_info = false;
            if ($use_agent_info) {
                $post = get_post($hotel_id);
                if ($post) {
                    return get_the_author_meta('user_email', $post->post_author);
                }
            }
            return get_post_meta($hotel_id, 'email', true);
        }
        /**
         * @since 1.1.0
         **/
        static function getStar($post_id = false)
        {
            if (!$post_id) {
                $post_id = get_the_ID();
            }
            return intval(get_post_meta($post_id, 'hotel_star', true));
        }
        static function listTaxonomy()
        {
            $terms = get_object_taxonomies('hotel_room', 'objects');
            $listTaxonomy = [];
            if (!is_wp_error($terms) and !empty($terms))
                foreach ($terms as $key => $val) {
                    $listTaxonomy[$val->labels->name] = $key;
                }
            return $listTaxonomy;
        }
        /** from 1.1.7*/
        static function get_taxonomy_and_id_term_tour()
        {
            $list_taxonomy = st_list_taxonomy('st_hotel');
            $list_id_vc = [];
            $param = [];
            foreach ($list_taxonomy as $k => $v) {
                $param[] = [
                    "type" => "st_checkbox",
                    "holder" => "div",
                    "heading" => $k,
                    "param_name" => "id_term_" . $v,
                    'stype' => 'list_terms',
                    'sparam' => $v,
                    'dependency' => [
                        'element' => 'sort_taxonomy',
                        'value' => [$v]
                    ],
                ];
                $list_value = "";
                $list_id_vc["id_term_" . $v] = "";
            }
            return [
                "list_vc" => $param,
                'list_id_vc' => $list_id_vc
            ];
        }
        static function get_list_hotel_by_location_or_address($locations, $address)
        {
            $location_ids = implode(',', $locations);
            global $wpdb;
            $select = "";
            $where = "";
            $group_by = " GROUP BY {$wpdb->prefix}posts.ID ";
            $order_by = " ORDER BY {$wpdb->prefix}postmeta.meta_value DESC ";
            $limit = "";
            $select .= "SELECT SQL_CALC_FOUND_ROWS {$wpdb->prefix}posts.ID
                                FROM {$wpdb->prefix}posts
                                INNER JOIN {$wpdb->prefix}postmeta
                                ON ( {$wpdb->prefix}posts.ID = {$wpdb->prefix}postmeta.post_id )
                                INNER JOIN {$wpdb->prefix}st_hotel as tb ON {$wpdb->prefix}posts.ID = tb.post_id ";
            $where .= " WHERE 1=1 ";
            $user_id = get_current_user_id();
            if (!is_super_admin($user_id)) {
                $where .= " AND {$wpdb->prefix}posts.post_author IN ({$user_id}) ";
            }
            $where .= " AND {$wpdb->prefix}posts.post_type = 'st_hotel' AND {$wpdb->prefix}posts.post_status = 'publish' ";
            if (!empty($locations)) {
                $where .= " AND {$wpdb->prefix}posts.ID IN (SELECT post_id FROM {$wpdb->prefix}st_location_relationships WHERE 1=1 AND location_from IN ({$location_ids}) AND post_type IN ('st_hotel')) ";
            } else {
                if ($address != '') {
                    $where .= " AND (tb.address LIKE '%{$address}%' ";
                    $where .= " OR {$wpdb->prefix}posts.post_title LIKE '%{$address}%') ";
                }
            }
            $where .= " AND {$wpdb->prefix}posts.ID IN ( SELECT ID FROM ( SELECT meta1.meta_value as ID FROM {$wpdb->prefix}posts INNER JOIN {$wpdb->prefix}postmeta as meta1 ON {$wpdb->prefix}posts.ID = meta1.post_id and meta1.meta_key='room_parent' WHERE 1=1 AND {$wpdb->prefix}posts.post_type = 'hotel_room' GROUP BY meta1.meta_value ) as ids ) ";
            $sql = "
                         {$select}
                         {$where}
                         {$group_by}
                         {$order_by}
                         {$limit}
                        ";
            $res = $wpdb->get_results($sql, ARRAY_A);
            return $res;
        }
        /**
         * NOTE fuction to get all hotels as array
         */
        function get_all_hotels(){
            global $wpdb;
            $select = "";
            $where = "";
            $group_by = " GROUP BY {$wpdb->prefix}posts.ID ";
            $order_by = " ORDER BY {$wpdb->prefix}posts.ID DESC ";
            $limit = "";
            $select .= "SELECT  {$wpdb->prefix}posts.ID, {$wpdb->prefix}posts.post_title
                                FROM {$wpdb->prefix}posts
                                INNER JOIN {$wpdb->prefix}postmeta
                                ON ( {$wpdb->prefix}posts.ID = {$wpdb->prefix}postmeta.post_id )
                                INNER JOIN {$wpdb->prefix}st_hotel as tb ON {$wpdb->prefix}posts.ID = tb.post_id ";
            $where .= " WHERE 1=1 ";
            $where .= " AND {$wpdb->prefix}posts.post_type = 'st_hotel' AND {$wpdb->prefix}posts.post_status = 'publish' ";
            $sql = "
                         {$select}
                         {$where}
                         {$group_by}
                         {$order_by}
                         {$limit}
                        ";
            $res = $wpdb->get_results($sql, ARRAY_A);
            return $res;
        }
        /**
         * NOTE alter search query
         */
        function get_unavailability_hotel($check_in, $check_out, $adult_number, $children_number, $number_room = 1)
        {
            $check_in = strtotime($check_in);
            $check_out = strtotime($check_out);
            $r = [];
            $list_hotel = ST_Hotel_Room_Availability::inst()
                ->select('post_id, parent_id')
                ->where("periode_from <=", $check_in)
                ->where("periode_to >=", $check_out)
                ->where("(status = 'unavailable')", null, true)
                ->groupby('post_id')
                ->get()->result();
            if (!empty($list_hotel)) {
                foreach ($list_hotel as $k => $v) {
                    $hotel_id = $v['parent_id'];
                    //if ( !empty( $hotel_id ) )
                    $r[] = $hotel_id;
                }
            }
            $freqs = array_count_values($r);
            global $wpdb;
            $sql_count_room = "SELECT room_parent FROM {$wpdb->prefix}link_hotel_room as ht INNER JOIN {$wpdb->prefix}posts as p ON ht.room_parent = p.ID WHERE p.post_status = 'publish' GROUP By room_parent";
            $count_room_by_hotel = $wpdb->get_results($sql_count_room, ARRAY_A);
            dd($count_room_by_hotel, "count_room_by_hotel");
            $rs = [];
            if (!empty($count_room_by_hotel)) {
                foreach ($count_room_by_hotel as $kc => $vc) {
                    if (isset($freqs[$vc['room_parent']])) {
                        if ($freqs[$vc['room_parent']] >= $vc['number_room'])
                            $rs[] = $vc['room_parent'];
                    }
                }
            }
            dd($rs, "rs_hotel_res");
            return $rs;
        }
        // NOTE 1/ click the reservation the button
        function ajax_hotel_add_to_cart()
        {
            if (STInput::request('action') == 'hotel_add_to_cart') {
                $response = array();
                $response['status'] = 0;
                $response['message'] = "";
                $response['redirect'] = '';
                if ($this->do_add_to_cart()) {
                    $link = STCart::get_cart_link();
                    $response['redirect'] = $link;
                    $response['status'] = 1;
                    echo json_encode($response);
                    wp_die();
                } else {
                    $message = STTemplate::message();
                    $response['message'] = $message;
                    echo json_encode($response);
                    wp_die();
                }
            }
        }
        static function get_cart_item_total($item_id, $item) 
        {
            /* @since 1.1.3 */
            $count_sale = 0;
            $price_sale = $item['price'];
            $adult_price2 = 0;
            $child_price2 = 0;
            if (!empty($item['data']['discount'])) {
                $count_sale = $item['data']['discount'];
                $price_sale = $item['data']['price_sale'] * $item['number'];
            }
            $adult_number = $item['data']['adult_number'];
            $child_number = $item['data']['child_number'];
            $adult_price = $item['data']['adult_price'];
            $child_price = $item['data']['child_price'];
            $adult_price = round($adult_price);
            $child_price = round($child_price);
            $total_price = $adult_number * st_get_discount_value($adult_price, $count_sale, false);
            $total_price += $child_number * st_get_discount_value($child_price, $count_sale, false);
            return $total_price;
        }
        static function inst()
        {
            if (!self::$_inst) {
                self::$_inst = new self();
            }
            return self::$_inst;
        }
    }
    STHotel::inst()->init();
}
