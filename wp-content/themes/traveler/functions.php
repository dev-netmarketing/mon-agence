<?php
update_option( 'siteurl', 'https://mon-agence.webdatasolutions.net' );
update_option( 'home', 'https://mon-agence.webdatasolutions.net' );
/**
 * @package    WordPress
 * @subpackage Traveler
 * @since      1.0
 *
 * function
 *
 * Created by ShineTheme
 *
 */
if (!defined('ST_TEXTDOMAIN'))
    define('ST_TEXTDOMAIN', 'traveler');
if (!defined('ST_TRAVELER_VERSION')) {
    $theme = wp_get_theme();
    if ($theme->parent()) {
        $theme = $theme->parent();
    }
    define('ST_TRAVELER_VERSION', $theme->get('Version'));
}
define("ST_TRAVELER_DIR", get_template_directory());
define("ST_TRAVELER_URI", get_template_directory_uri());
global $st_check_session;
if (session_status() == PHP_SESSION_NONE) {
    $st_check_session = true;
    session_start();
}
$status = load_theme_textdomain(ST_TEXTDOMAIN, get_stylesheet_directory() . '/language');
get_template_part('inc/class.traveler');
if (!class_exists("Abraham\TwitterOAuth\TwitterOAuth")) {
    include_once "vendor/autoload.php";
}
add_filter('http_request_args', 'st_check_request_api', 10, 2);
//FUNCTION TO CHANGE CURRENCY SYMBOLE FROM TND TO DT
add_filter('woocommerce_currency_symbol', 'my_currency_symbol', 10, 2);
  
function my_currency_symbol( $currency_symbol, $currency ) {
     switch( $currency ) {
          case 'TND': $currency_symbol = 'DT'; 
	  break;
     }
     return $currency_symbol;
}
function st_check_request_api($parse, $url)
{
    global $st_check_session;
    if ($st_check_session) {
        session_write_close();
    }
    return $parse;
}
add_filter('upload_mimes', 'traveler_upload_types', 1, 1);
function traveler_upload_types($mime_types)
{
    $mime_types['svg'] = 'image/svg+xml';
    return $mime_types;
}
add_theme_support(
    'html5',
    array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    )
);