<?php
wp_enqueue_script( 'bootstrap-datepicker.js' ); 
wp_enqueue_script( 'bootstrap-datepicker-lang.js' );

$post_id = intval($_GET['id']); 
?>

<span class="hidden st_partner_avaiablity <?php echo STInput::get('sc') ?> "></span>
<div class="row calendar-wrapper template-user" data-post-id="<?php echo esc_html($post_id); ?>">
    <div class="col-xs-12 col-md-12 calendar-wrapper-inner calendar">
        <div class="overlay-form"><i class="fa fa-refresh text-color"></i></div>
        <div class="calendar-content" data-price-by-per-person="on"></div>
    </div>

    <div class="col-xs-12 col-md-12 calendar-wrapper-inner detail_day_av">
    
        <div class="setting">

            <div class="calendar-form">
		        <input type="hidden" name="id" value="<?php echo esc_html($post_id); ?>">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label for="calendar_check_in"><?php echo __('Check In', ST_TEXTDOMAIN); ?></label>
                                <input readonly="readonly" type="text" class="form-control date-picker" placeholder="<?php echo __('Check In', ST_TEXTDOMAIN); ?>" name="calendar_check_in" id="calendar_check_in">
                            </div>
                        </div>
                        <div class="col-sn-6 col-xs-6">
                            <div class="form-group">
                                <label for="calendar_check_out"><?php echo __('Check Out', ST_TEXTDOMAIN); ?></label>
                                <input readonly="readonly" type="text" class="form-control date-picker" placeholder="<?php echo __('Check Out', ST_TEXTDOMAIN); ?>" name="calendar_check_out" id="calendar_check_out">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="row form-group">
                                <div class="col-xs-12">
                                    <label for="calendar_adult_price"><?php echo __('Adult Price', ST_TEXTDOMAIN); ?></label>
                                    <input type="text" name="calendar_adult_price" id="calendar_adult_price" class="form-control" placeholder="<?php echo __('Adult Price', ST_TEXTDOMAIN); ?>">
                                </div>
                            </div>	
		                 
			<?php                   
            $facilities = get_the_terms($post_id, 'hotel-accommodation');     
            if ($facilities) {
                $count = count($facilities);             
			?>   
			<div class="form-group">		
                <div class="form-title">	
                    <label for="accommodationid">Type d'hébergement</label>
                </div>
			    <div class="form-content">	
			        <div class="facilities" data-toggle-section="st-facilities">    
			            <div class="row">                               
			<?php                              
			if (isset($facilities) && is_array($facilities)) {
				foreach ($facilities as $term) {
				?>														             
				    <label class="block mr10 col-xs-3">
				    <span style="margin-right:10px">
				    <?php echo esc_html($term->name); ?>
				    </span>
                    <input type="hidden" id ="acc" name="accommodationid[]" value="<?php echo esc_html($term->term_id); ?>">
                    <input type="text"  data-value='<?php echo esc_html($term->term_id); ?>'  name="accommodation[]" value="" class="form-control"></label> 
				<?php
				} // end foreach
				?>                           
				        </div>
			        </div> 
				<?php                                
				} // end if facilities                             
				?> 			
				<?php if ($count > 6) { ?>         
                <a href="#" class="st-link block" data-show-target="st-facilities"       
                    data-text-less="<?php echo esc_html__('Show Less', ST_TEXTDOMAIN) ?>"    
                    data-text-more="<?php echo esc_html__('Show All', ST_TEXTDOMAIN) ?>">
                    <span class="text"><?php echo esc_html__('Show All', ST_TEXTDOMAIN) ?></span>      
                    <i class="fa fa-caret-down ml3"></i>
                </a>                         
                    <?php
                    } // end count 6
                    } // end facilities ?>			
				</div>			
				</div>		

                <?php
                global $post;
                $hotel = new STHotel();
                $query = $hotel->st_fetch_rooms($post_id);
                    
                ?>
					 
				<div class="form-group">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th rowspan="2">Adult</th>
                                    <?php for ($i = 1; $i <= 2; $i++){ ?>
                                    
                                    <?php if (get_post_meta( $post_id, 'min_age_hot_'.$i, true ) > 0 && get_post_meta( $post_id, 'max_age_hot_'.$i, true ) > 0) { ?>
                                                    <th colspan="3"><?php echo __('Age child entre', ST_TEXTDOMAIN); ?> <strong><?php echo (get_post_meta( $post_id, 'min_age_hot_'.$i, true ) ) ;?></strong>
                                        <?php echo __(' et ', ST_TEXTDOMAIN); ?> 
                                        <strong><?php echo (get_post_meta( $post_id, 'max_age_hot_'.$i, true ) ) ;?> </strong>						
                                        <input type="hidden"  name="<?php echo __('min_age_'.$i, ST_TEXTDOMAIN); ?>" value="<?php echo (get_post_meta( $post_id, 'min_age_hot_'.$i, true ) ) ;?>" >    
                                        <input type="hidden"  name="<?php echo __('max_age_'.$i, ST_TEXTDOMAIN); ?>" value="<?php echo (get_post_meta( $post_id, 'max_age_hot_'.$i, true ) ) ;?>" > 
                                    </th>
                                        <?php  } } ?>	
                                </tr>
                                <tr>
                                    <?php if (get_post_meta( $post_id, 'min_age_hot_1', true ) > 0 && get_post_meta( $post_id, 'max_age_hot_1', true ) > 0) { ?>
                                    <?php for ($i = 1; $i <= 3; $i++){ ?>
                                        <th>Child <?php echo $i ; ?> (%)</th>
                                    <?php  }} ?>
                                        <?php if (get_post_meta( $post_id, 'min_age_hot_2', true ) > 0 && get_post_meta( $post_id, 'max_age_hot_2', true ) > 0) { ?>
                                        <?php for ($j = 1; $j <= 3; $j++){ ?>
                                        <th>Child <?php echo $j ; ?> (%)</th>
                                    <?php  }} ?>
                                </tr>
        
                            </thead>
                            <tbody>
                            <?php for ($r = 0; $r < $query; $r++){ ?>
                                        <tr>
                                            <td>
                                            <?php echo __('With '.$r.' Adult', ST_TEXTDOMAIN); ?>
                                            <input type="hidden"  name="adult_number" value="<?php echo $r ;?>" >
                                            
                                            </td>
                                            <?php for ($i = 1; $i <= 2; $i++){ ?>
                                            <?php if (get_post_meta( $post_id, 'min_age_hot_'.$i, true ) > 0 && get_post_meta( $post_id, 'max_age_hot_'.$i, true ) > 0) { ?>
                                            <?php for ($j = 1; $j <= 3; $j++){ ?>
                                            <td><input type="text"  name="<?php echo __('enf'.$i.'_'.$j.'_'.$r, ST_TEXTDOMAIN); ?>" id="<?php echo (get_post_meta( $post_id, 'enf'.$i.'_'.$j.'_'.$r, true ) ) ;?>" value="" class="form-control" placeholder=""></td>
                                            <?php } } } ?>
                                        </tr>

                                <?php } ?>    
                            </tbody>
                        </table>
                    </div>
				</div>
						
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <label for="discount_rate_p"><?php echo __('Promotion', ST_TEXTDOMAIN); ?></label>
                            <input type="text" name="discount" id="discount_rate_p" class="form-control" placeholder="<?php echo __('Discount rating', ST_TEXTDOMAIN); ?>">
                        </div>
					</div>	
					

                <div class="form-group st-field-list-item st-partner-field" data-condition="" data-operator="or">
                    <label for="st-field-extra_price">Extra pricing</label>
                    <div class="item origin">
                        <div class="listitem-title">New item</div>
                        <div class="del"></div>
                        <div class="row">
                            <div class="item-inner col-lg-12">
                                <div class="form-group st-field-text">
                                    <label for="st-field-extra[title]">Title</label>
                                    <input type="text" value="" id="st-field-extra[title]" name="extra[title][]" class="st-partner-field form-control" placeholder="" data-condition="" data-operator="or">
                                    <div class="st_field_msg"></div>
                                </div>
                            </div>
                            <div class="item-inner col-lg-12">
                                <div class="form-group st-field-text">
                                    <label for="st-field-extra[extra_name]">Name</label>
                                    <input type="text" value="extra_" id="st-field-extra[extra_name]" name="extra[extra_name][]" class="st-partner-field form-control" placeholder="" data-condition="" data-operator="or">
                                    <div class="st_field_msg"></div>
                                </div>
                            </div>
                            <div class="item-inner col-lg-12" style="display:none">
                                <div class="form-group st-field-text">
                                    <label for="st-field-extra[extra_max_number]">Max Of Number</label>
                                    <input type="text" value="" id="st-field-extra[extra_max_number]" name="extra[extra_max_number][]" class="st-partner-field form-control" placeholder="" data-condition="" data-operator="or">
                                    <div class="st_field_msg"></div>
                                </div>
                            </div>
                            <div class="item-inner col-lg-12">
                                <div class="form-group st-field-text">
                                    <label for="st-field-extra[extra_price]">Price</label>
                                    <input type="text" value="" id="st-field-extra[extra_price]" name="extra[extra_price][]" class="st-partner-field form-control" placeholder="" data-condition="" data-operator="or">
                                    <div class="st_field_msg"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="add-item">Add New</div>
                </div>

		</div>
                <div class="col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="calendar_status"><?php echo __('Status', ST_TEXTDOMAIN); ?></label>
                        <select name="calendar_status" id="calendar_status" class="form-control">
                            <option value="available"><?php echo __('Available', ST_TEXTDOMAIN); ?></option>
                            <option value="unavailable"><?php echo __('Unavailble', ST_TEXTDOMAIN); ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-message">
                    <p class="text-danger"></p>
                </div>
            </div>
            <div class="form-group">
                <input type="hidden" name="calendar_post_id" value="<?php echo esc_html($post_id); ?>">
                <input type="submit" id="calendar_submit" class="btn btn-primary" name="calendar_submit" value="<?php echo __('Update', ST_TEXTDOMAIN); ?>">
                <?php do_action('traveler_after_form_submit_hotel_calendar'); ?>
            </div>
        </div>
    </div>
	</div>
    <?php do_action('traveler_after_form_hotel_calendar'); ?>
</div>
