<?php
$options_saison     = array('basse_saison', 'moyenne_saison', 'haute_saison');
$post_id            = $periode_data->post_id;
global $post;
$hotel              = new STHotel();
$st_fetch_rooms     = $hotel->st_fetch_rooms($post_id);
$adult_enfant       = json_decode($periode_data->adult_enfant, true);
$accommodation_list = json_decode($periode_data->accommodation, true);
$extra_list         = json_decode($periode_data->extra_price, true);
$stock_rooms        = json_decode($periode_data->stock, true);
$periode_from       = date("m/d/Y", $periode_data->periode_from);
$periode_to         = date("m/d/Y", $periode_data->periode_to);
$date               = STInput::get('date', $periode_from . '-'. $periode_to);
?>

<div class="row">
    <div class="col-lg-12">
                <div class="st-field-group sf_periode_bloc sf_periode_bloc_mod">
                    <div class="row">
                        <div class="col-lg-6 st-partner-field-item">
                            <div class="form-group st-field-select">
                                <label for="st-field-sf_saison_mod">Saison</label>
                                <select id="st-field-sf_saison_mod" name="sf_saison" class="st-partner-field form-control" data-condition="" data-operator="or">
                                    <?php foreach($options_saison as $saison): ?>
                                        <?php if($periode_data->saison == $saison): ?>
                                            <option value="<?php echo $saison; ?>" selected><?php echo STHotel::get_selected_saison($saison) ?></option>
                                        <?php else: ?>
                                            <option value="<?php echo $saison; ?>"><?php echo STHotel::get_selected_saison($saison) ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 st-partner-field-item">
                            <div class="form-group st-field-text">
                                <label for="st-field-sf_periode"><?php echo "Période n° " . $periode_data->periode_id . " (PID " .$periode_data->id . ")" ?></label>
                                <div class="form-group form-date-field form-date-search clearfix has-icon" data-format="<?php echo TravelHelper::getDateFormatMoment() ?>">
                                    <input type="text" required id="sf_periode_mod" name="sf_periode_mod" class="st-partner-field form-control" value="<?php echo $date; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 st-partner-field-item">
                            <div class="form-group st-field-text">
                                <label for="st-field-sf_disponiblite_mod">Disponibilité</label>
                                <select id="st-field-sf_disponiblite_mod" name="sf_disponiblite" class="st-partner-field form-control" data-condition="" data-operator="or">
                                    <option value="-1" selected="">Sélectionnez</option>
                                    <?php if($periode_data->status == 'available'): ?>
                                        <option value="available" selected>Disponible</option>
                                        <option value="unavailable">Complet</option>
                                    <?php else: ?>
                                        <option value="available">Disponible</option>
                                        <option value="unavailable" selected>Complet</option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 st-partner-field-item">
                            <div class="form-group st-field-text">
                                <label for="st-field-sf_retrocession">Délai de retrocession (-jour)</label>
                                <input type="number" min="0" value="<?php echo $periode_data->retrocession ?>" name="sf_retrocession" id="sf_retrocession" class="form-control" placeholder="Delai de rétrocession" required>
                            </div>
                        </div>
                        <div class="col-lg-3 st-partner-field-item">
                            <div class="form-group st-field-text">
                                <label for="st-field-sf_calendar_marge_price">Marge tarif</label>
                                <input type="number" min="0" max="100" value="<?php echo $periode_data->marge ?>" name="sf_calendar_marge_price" id="sf_calendar_marge_price" class="form-control" placeholder="Valeur en %" required>
                            </div>
                        </div>
                        <div class="col-lg-3 st-partner-field-item">
                            <div class="form-group st-field-text">
                                <label for="st-field-sf_calendar_promotion_prix">Promotion</label>
                                <input type="number" min="0" max="100" value="<?php echo $periode_data->discount ?>" name="sf_calendar_promotion_prix" id="sf_calendar_promotion_prix" class="form-control" placeholder="Valeur en %" required>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row st-partner-field-item">
                    <div class="col-lg-12 st-partner-field-item"><h5 class="st-group-heading">Prix arrangement(s)</h5></div>
                        <?php                   
                        $facilities = get_the_terms($post_id, 'hotel-accommodation');
                            if ($facilities) {
                            $count = count($facilities);
                        ?>   
                            <?php                              
                            if (isset($facilities) && is_array($facilities)) {
                                foreach ($facilities as $term) {
                            ?>
                                <div class="col-lg-4 form-group st-field-text block">
                                    <label for="st-field-sf_periode"> <?php echo "Prix en <b>" . esc_html($term->name) . "</b> (RID: ".$term->term_id.")"; ?></label>
                                    <input type="hidden" id ="acc" name="sf_pension_accommodation_id[]" value="<?php echo esc_html($term->term_id); ?>">
                                    <input type="number" min="0" max="100"  data-value='<?php echo esc_html($term->term_id); ?>'  name="sf_pension_accommodation[]" value="<?php echo STHotel::get_price_pension($accommodation_list, $term->term_id); ?>" class="form-control" placeholder="Prix en DT" required></label> 
                                </div>
                                <?php } // end foreach ?>
                            <?php } // end if facilities ?> 			
                        <?php } // end facilities  ?>	
                    </div>
                    <div class="row st-partner-field-item">
                        <?php                   
                        $facilities = get_the_terms($post_id, 'extra');
                        if ($facilities) {
                        $count = count($facilities);
                        ?>   
                            <?php                              
                            if (isset($facilities) && is_array($facilities)) {
                                foreach ($facilities as $term) {
                            ?>
                                <div class="col-lg-4 form-group st-field-text block">
                                    <label for="st-field-sf_periode"> <?php echo "Supplément <b>" . esc_html($term->name) . "</b>"; ?></label>
                                    <input type="hidden" id ="extra" name="sf_extra_id[]" value="<?php echo esc_html($term->term_id); ?>">
                                    <input  type="number" min="0" max="100"  data-value='<?php echo esc_html($term->term_id); ?>'  name="sf_extra[]" 
                                            value="<?php echo STHotel::get_price_extra($extra_list, $term->term_id); ?>" 
                                            class="form-control" 
                                            placeholder="Prix en DT" 
                                            required></label> 
                                </div>
                                <?php } // end foreach ?>
                            <?php } // end if facilities ?> 			
                        <?php } // end facilities  ?>	
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-12 st-partner-field-item"><h5 class="st-group-heading">Stock/chambre(s)</h5></div>
                        <?php
                            $rooms = get_saved_rooms($post_id);
                            if ($rooms) {
                                foreach ($rooms as $room) {
                        ?>
                                <div class="col-lg-4 form-group st-field-text block">
                                    <label for="st-field-sf_stock"> <?php echo "Stock <b>" . get_the_title($room) . "</b>"; ?></label>
                                    <input type="hidden" id ="stock" name="sf_stock_id[]" value="<?= $room ?>">
                                    <input type="number" min="0" max="100"  data-value='<?= $room; ?>'  name="sf_stock[]" 
                                    value="<?php echo STHotel::get_saved_stock($stock_rooms, $room);?>" 
                                    class="form-control" 
                                    placeholder="Stock chambre" required></label> 
                                </div>
                                <?php } // end foreach rooms  ?>
                        <?php } // end rooms  ?>
                    </div>
                    <hr>
                    <div class="row">
                    <div class="col-lg-12 st-partner-field-item"><h5 class="st-group-heading">Réductions</h5></div>
                        <div class="col-lg-4 st-partner-field-item">
                            <div class="form-group st-field-select">
                                <label for="st-field-sf_prix_single">Supplément Single</label>
                                <input type="number" min="0" max="100" value="<?php echo $periode_data->prix_single ?>" class="st-partner-field form-control" value="" name="sf_prix_single" placeholder="Valeur en DT" required>
                            </div>
                        </div>
                        <div class="col-lg-4 st-partner-field-item">
                            <div class="form-group st-field-text">
                                <label for="st-field-sf_trois_lits">Réduction 3éme & 4éme lit</label>
                                <input type="number" min="0" max="100" value="<?php echo $periode_data->prix_3_lits ?>" class="st-partner-field form-control" value="" name="sf_trois_lits" placeholder="Valeur en %" required>
                            </div>
                        </div>
                        <div class="col-lg-4 st-partner-field-item">
                            <div class="form-group st-field-text">
                                <label for="st-field-sf_min_stay">Minimum stay</label>
                                <input type="number" min="0" max="100" value="<?php echo $periode_data->min_stay ?>" class="st-partner-field form-control" value="" name="sf_min_stay" placeholder="Nombre de jour" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-lg-12 st-partner-field-item"><h5 class="st-group-heading">Réductions</h5></div>
                        <div class="col-lg-4 st-partner-field-item">
                            <div class="form-group st-field-select">
                                <label for="st-field-sf_prix_single">Supplément Single</label>
                                <input type="number" min="0" max="100" value="<?php echo $periode_data->prix_single ?>" class="st-partner-field form-control" value="" name="sf_prix_single" placeholder="Valeur en DT" required>
                            </div>
                        </div>
                        <div class="col-lg-4 st-partner-field-item">
                            <div class="form-group st-field-text">
                                <label for="st-field-sf_trois_lits">Réduction 3éme & 4éme lit</label>
                                <input type="number" min="0" max="100" value="<?php echo $periode_data->prix_3_lits ?>" class="st-partner-field form-control" value="" name="sf_trois_lits" placeholder="Valeur en %" required>
                            </div>
                        </div>
                        <div class="col-lg-4 st-partner-field-item">
                            <div class="form-group st-field-text">
                                <label for="st-field-sf_min_stay">Minimum stay</label>
                                <input type="number" min="0" max="100" value="<?php echo $periode_data->min_stay ?>" class="st-partner-field form-control" value="" name="sf_min_stay" placeholder="Nombre de jour" required>
                            </div>
                        </div>
                        <div class="col-lg-12 st-partner-field-item">
                            <div class="form-group st-field-select">
                                <label for="st-field-sf_msg_promotion">Message promotionnel</label>
                                <input type="text" class="st-partner-field form-control" value="<?php echo $periode_data->msg_promotion; ?>"  name="sf_msg_promotion" placeholder="Votre message de promotion ici">
                            </div>
                        </div>
                    </div>
                    
                    <?php if($st_fetch_rooms) :  $array_age = array();?>
                    <div class="row sf_adult">
                        <div class="col-lg-12 st-partner-field-item"><h5 class="st-group-heading">Réduction Adulte(s)/Enfant(s)</h5></div>
                        <div class="col-lg-12 st-partner-field-item">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th rowspan="2">Avec Adulte</th>
                                                <?php for ($i = 1; $i <= 2; $i++){ ?>
                                            
                                                <?php 
                                                    if (get_post_meta( $post_id, 'min_age_hot_'.$i, true ) > 0 && get_post_meta( $post_id, 'max_age_hot_'.$i, true ) > 0) { 
                                                        $array_age[$i] = get_post_meta( $post_id, 'min_age_hot_'.$i, true ) . '_' . get_post_meta( $post_id, 'max_age_hot_'.$i, true );
                                                    ?>
                                                <th colspan="3"><?php echo __('Prix enfant', ST_TEXTDOMAIN); ?> <strong><?php echo (get_post_meta( $post_id, 'min_age_hot_'.$i, true ) ) ;?></strong>
                                                    <?php echo __(' et ', ST_TEXTDOMAIN); ?> 
                                                    <strong><?php echo (get_post_meta( $post_id, 'max_age_hot_'.$i, true ) ) ;?> </strong>						
                                                    <input type="hidden"  name="<?php echo 'min_age_'.$i; ?>" value="<?php echo (get_post_meta( $post_id, 'min_age_hot_'.$i, true ) ) ;?>" >
                                                    <input type="hidden"  name="<?php echo 'max_age_'.$i; ?>" value="<?php echo (get_post_meta( $post_id, 'max_age_hot_'.$i, true ) ) ;?>" >
                                                </th>
                                            <?php  } } ?>	
                                        </tr>
                                        <tr>
                                            <?php if (get_post_meta( $post_id, 'min_age_hot_1', true ) > 0 && get_post_meta( $post_id, 'max_age_hot_1', true ) > 0) { ?>
                                            <?php for ($i = 1; $i <= 3; $i++){ ?>
                                                <th>Avec <?php echo $i; ?>&nbsp;enfant(s)</th>
                                            <?php  }} ?>
                                                <?php if (get_post_meta( $post_id, 'min_age_hot_2', true ) > 0 && get_post_meta( $post_id, 'max_age_hot_2', true ) > 0) { ?>
                                                <?php for ($j = 1; $j <= 3; $j++){ ?>
                                                    <th>Avec <?php echo $j; ?>&nbsp;enfant(s)</th>
                                            <?php  }} ?>
                                        </tr>
                
                                    </thead>
                                    <tbody>
                                        <?php for ($r = 0; $r < $st_fetch_rooms; $r++){ ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo __($r.' Adulte', ST_TEXTDOMAIN); ?>
                                                            <input type="hidden"  name="adult_number" value="<?php echo $r ;?>" >
                                                        </td>
                                                        <?php for ($i = 1; $i <= 2; $i++){ ?>
                                                            <?php if (get_post_meta( $post_id, 'min_age_hot_'.$i, true ) > 0 && get_post_meta( $post_id, 'max_age_hot_'.$i, true ) > 0) { ?>
                                                                <?php for ($j = 1; $j <= 3; $j++){ ?>
                                                                    <td>
                                                                        <input  style="font-size: 11px" type="number" min="0" max="100" 
                                                                                name="<?php echo 'adult_enfant['. $r .'][age-'.$array_age[$i].'-enf-'.$j.'-adult-'.$r .']' ?> " 
                                                                                id="<?php echo (get_post_meta( $post_id, 'age-'.$array_age[$i].'-enf-'.$j.'-adult-'.$r, true ) ) ;?>" 
                                                                                value="<?php echo STHotel::get_adult_child($adult_enfant, 'age-'.$array_age[$i].'-enf-'.$j.'-adult-'.$r) ?>" 
                                                                                class="form-control" 
                                                                                placeholder="<?php echo 'age-'.$array_age[$i].'-enf-'.$j.'-adult-'.$r; ?>"></td>
                                                        <?php } } } ?>
                                                    </tr>

                                        <?php } ?>    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="hidden" name="periode_from_mod" id="periode_from_mod" value="<?php echo $periode_from ?>">
                            <input type="hidden" name="periode_to_mod" id="periode_to_mod" value="<?php echo $periode_to ?>">
                            <input type="hidden" name="calendar_check_in_mod" id="calendar_check_in_mod" value="">
                            <input type="hidden" name="calendar_check_out_mod" id="calendar_check_out_mod" value="">
                            <input type="hidden" name="post_id_mod" value="<?php echo esc_html($periode_data->post_id); ?>">
                            <input type="hidden" name="periode_id_mod" value="<?php echo $periode_data->id; ?>">
                            <input type="hidden" name="periode_num_mod" value="<?php echo $periode_data->periode_id; ?>">
                            <input type="button" value="Modifier cette période" class="do_modifier_periode"/>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

<script type="text/javascript">
jQuery(document).ready(function ($) {
    // on change availibility
    $("#st-field-sf_disponiblite_mod").change(function(){
        if(this.value == "unavailable"){
            $(".sf_periode_bloc_mod :input:not([type=hidden])").prop("disabled", true);
            $("#st-field-sf_saison_mod").prop("disabled", false);
            $("#sf_periode_mod").prop("disabled", false);

            $(this).prop("disabled", false);
            $(".add_periode").prop("disabled", false);
            $('input[name="periode_id_mod"]').prop("disabled", false);
            $('input[name="calendar_post_id_mod"]').prop("disabled", false);

        } else {
            $(".sf_periode_bloc_mod :input").prop("disabled", false);
        }
      });
      
    // on change periode input
    function setInputDates(start, end) {
        $('#calendar_check_in_mod').val(start.format('DD/MM/YYYY'));
        $('#calendar_check_out_mod').val(end.format('DD/MM/YYYY'));
    }

    function clearPeriodeForm(){
        $('.sf_periode_bloc_mod :input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
        $('.sf_periode_bloc_mod :checkbox, :radio').prop('checked', false);
    }

    var t = $(this);
    var dateToday = new Date("MM/DD/YYYY");
    var periodeDateInput = t.find('#sf_periode_mod');
    periodeDateInput.val('');
    var locale_daterangepicker = {
        direction: 'ltr',
        applyLabel: 'Apply',
        cancelLabel: 'Cancel',
        daysOfWeek: ['LU', 'MA', 'ME', 'JE', 'VE', 'SA','DI'],
        monthNames: ['Janvier', 'Févrie', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        firstDay: 1,
        today: 'Today',
        format: 'MM/DD/YYYY'
    }; 
    var DateRangeOptions = {
        singleDatePicker: false,
        autoApply: true,
        disabledPast: false,
        widthSingle: 500,
        startDate: $("#periode_from_mod").val(),
        endDate: $("#periode_to_mod").val(),
        dateFormat: 'MM/DD/YYYY',
        format: 'MM/DD/YYYY',
        showDropdowns: true,
        timePicker: false,
        locale: locale_daterangepicker, 
    };

    periodeDateInput.daterangepicker(DateRangeOptions, setInputDates);

    $('input[name="sf_periode_mod"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });
  
    $('input[name="sf_periode_mod"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    /** 
     * NOTE handling the new feature adding periods to database
     */
    $( ".do_modifier_periode" ).click(function(e){
        console.log('in hotel partner');
        $(".sf_periode_bloc_mod").addClass("loading__"); 
        e.preventDefault();
        var PeriodeFormData = $('input, select', '.sf_periode_bloc_mod').serializeArray();
        var flag_submit = !1;

        PeriodeFormData.push({
            name: 'action',
            value: 'st_mod_custom_price_hotel'
        });
        console.log('PeriodeFormData', PeriodeFormData);
        
        if (flag_submit) return !1;
        flag_submit = !0;	
        $.post(
            ajaxurl, 
            PeriodeFormData, 
            function(data, status) {
                console.log('do_modifier_periode click');
                console.log('data', data);
                console.log('status', status);
                flag_submit = !1
        }, 'json')
        .done(function() {
            $(".sf_periode_bloc_mod").removeClass("loading__"); 
            clearPeriodeForm();
            $("#info-periode-modal").modal('hide');
        })
        .fail(function() {
            alert( "Erreur lors de l'insertion, merci de ré-essayer" );
        })
        .always(function() {
            clearPeriodeForm();
            $(".sf_periode_bloc_mod").removeClass("loading__"); 
        });
        // This return prevents the submit event to refresh the page.
        return false;

    })

})
</script>