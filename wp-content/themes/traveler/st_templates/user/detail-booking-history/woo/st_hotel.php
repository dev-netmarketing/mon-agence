<?php

$woo_order_id = $order_data['order_item_id'];

$order = wc_get_order( $order_id );

$room_id = wc_get_order_item_meta( $woo_order_id, '_st_room_id', true );
$date_format = TravelHelper::getDateFormat();
$price_by_per_person = get_post_meta( $room_id, 'price_by_per_person', true );
// old sol.to fetch data order
$order_row              = json_decode($order_data['raw_data'],true);
$results                = $order_row["result"];
$room_id                = get_post_meta( $order_id, 'room_id', true );
$hotel_id               = $order_data['st_booking_id'];
$date_format            = TravelHelper::getDateFormat();
$price_by_per_person    = get_post_meta( $room_id, 'price_by_per_person', true );
$hotel                  = $order_data['st_booking_id'];
$room_num_search        = $order_row['room_num_search'];
$check_in               = $order_row['check_in'];
$check_out              = $order_row['check_out'];
$date_diff              = STDate::dateDiff($check_in,$check_out);
$extra_prices           = 0;

?>
<div class="st_tab st_tab_order tabbable">
    <ul class="nav nav-tabs tab_order">
        <li class="active">
            <?php
            $post_type = get_post_type( $service_id );
            $obj = get_post_type_object( $post_type ); ?>
            <a data-toggle="tab" href="#tab-booking-detail" aria-expanded="true"> <?php echo sprintf(esc_html__("%s Details",ST_TEXTDOMAIN),$obj->labels->singular_name) ?></a>
        </li>
        <li class="">
            <a data-toggle="tab" href="#tab-customer-detail" aria-expanded="false"> <?php esc_html_e("Customer Details",ST_TEXTDOMAIN) ?></a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent973">
        <div id="tab-booking-detail" class="tab-pane fade active in">
            <div class="info">
                <div class="row">
                    <div class="col-md-6">
                        <div class="item_booking_detail">
                            <strong><?php esc_html_e("Réservation ID",ST_TEXTDOMAIN) ?>:  </strong>
                            #<?php echo esc_html($order_id) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="item_booking_detail">
                            <strong><?php esc_html_e("Payment Method: ",ST_TEXTDOMAIN) ?> </strong>
                            <?php 
                            $payment_gateway =  wc_get_payment_gateway_by_order( $order_id );
                            echo esc_html($payment_gateway->get_title());
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="item_booking_detail">
                            <strong><?php esc_html_e("Order Date",ST_TEXTDOMAIN) ?>:  </strong>
                            <?php echo esc_html(date_i18n($date_format, strtotime($order_data['created']))) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="item_booking_detail">
                            <strong><?php esc_html_e("Booking Status",ST_TEXTDOMAIN) ?>:  </strong>
                            <?php
                            $data_status =  STUser_f::_get_all_order_statuses();
                            $status = $order_data['status'];
                            
                            if(!empty($status_string = $data_status[$status])){
                                //$status_string = $data_status[$status];
    	                        $status_string = $data_status[get_post_meta($order_id, 'status', true)];
                                if( isset( $order_data['cancel_refund_status'] ) && $order_data['cancel_refund_status'] == 'pending'){
                                    $status_string = __('Cancelling', ST_TEXTDOMAIN);
                                }
                            }
                            ?>
                            <span class=""> <?php 
                            
                            switch ($status) {
                                case "wc-processing":
                                  echo  esc_html("EN COURS");
                                  break;
                                  case "wc-on-hold":
                                    echo  esc_html("EN ATTENTE");
                                    break;
                                case "wc-completed":
                                  echo  esc_html("TERMINÉE");
                                  break;
                                case "wc-cancelled":
                                  echo  esc_html("ANNULÉE");
                                  break;
                                default:
                                  echo "uncategorized";
                              }
                            echo esc_html($status_string); ?></span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="item_booking_detail">
                            <strong><?php esc_html_e("Hotel Name",ST_TEXTDOMAIN) ?>:  </strong>
                            <a href="<?php echo get_the_permalink($service_id) ?>"><?php echo get_the_title($service_id) ?></a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="item_booking_detail">
                            <strong><?php esc_html_e("Address: ",ST_TEXTDOMAIN) ?>:  </strong>
                            <?php  echo get_post_meta( $service_id, 'address', true); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="item_booking_detail">
                            <strong><?php esc_html_e("Check In:",ST_TEXTDOMAIN) ?> </strong>
                            <?php
                            $check_in = date( $date_format, $order_data['check_in_timestamp'] );
                            echo esc_html($check_in);
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <div class="item_booking_detail">
                            <strong><?php esc_html_e("Check Out:",ST_TEXTDOMAIN) ?> </strong>
                            <?php
                            $check_out = date( $date_format, $order_data['check_out_timestamp'] );
                            echo esc_html($check_out);
                            ?>
                        </div>
                    </div>
                    <?php if(!empty(st_print_order_item_guest_name(json_decode($order_data['raw_data'],true)))){?>
                    <div class="col-md-12">
                        <div class="item_booking_detail">
                            <?php st_print_order_item_guest_name(json_decode($order_data['raw_data'],true)) ?>
                        </div>
                    </div>
                    <?php }?>
                    <div class="line col-md-12"></div>
                    <div class="col-md-12">
                        <div class="item_booking_detail">
                            <strong>Détail de réservation</strong>
                        </div>
                    </div>
                    <div class="col-md-12 room-list">
                        <?php for ($i = 1; $i <= $room_num_search; $i++) :
                        $room_id                    = $results[$i]['room_id'];
                        $arrangement_id             = $results[$i]['arrangement_id'];
                        $result_adult_child         = $results[$i]['result_adult_child'];
                        $result_adult_child_calc    = $results[$i]['result_adult_child_calc'];
                        $extras                     = isset($results[$i]['extra_price']) ? $results[$i]['extra_price'] : array();
                        $adult_number               = "adult_number_".$i;
                        $child_number               = "child_number_".$i;
                        ?>
                        <div class="single-room">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="item_booking_detail">
                                        <span class="label"><?php echo __('Chambre n° : ' .$i , ST_TEXTDOMAIN); ?></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="item_booking_detail">
                                        <span class="label"><?php echo __('Chambre : ' , ST_TEXTDOMAIN); ?></span>
                                        <span class="value"><?php echo get_the_title($room_id)?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="item_booking_detail">
                                        <span class="label"><?php echo __('Arragement : ' , ST_TEXTDOMAIN); ?></span>
                                        <span class="value"><?php echo get_term_by('id', $arrangement_id, 'hotel-accommodation')->name ?></span>
                                    </div>
                                </div>
                            </div>
                            <?php if($result_adult_child[$i][$adult_number]) {?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="item_booking_detail">
                                        <span class="label"><?php echo __('Nombre d\'adultes : ' , ST_TEXTDOMAIN); ?></span>
                                        <span class="value"><?php echo esc_attr($result_adult_child[$i][$adult_number])." adulte(s)"; ?></span>                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if($result_adult_child[$i][$child_number]) {?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="item_booking_detail">
                                        <span class="label"><?php echo __('Nombre d\'enfants : ' , ST_TEXTDOMAIN); ?></span>
                                        <span class="value"><?php echo esc_attr($result_adult_child[$i][$child_number])." enfant(s)"; ?></span>
                                        <br>
                                    <?php for ($j = 1; $j <= $result_adult_child[$i][$child_number]; $j++){ 
                                            $age_child = 'age_number_'.$i.'_'.$j;
                                    ?>
                                        <div class="age-detail child-<?php echo $j ?>">
                                            <span class="label"><?php echo __('Age enfant ' .$j . ' : ' , ST_TEXTDOMAIN); ?></span>
                                            <span class="value"><?php echo esc_attr($result_adult_child[$i][$age_child])." an(s)"; ?></span>                                    
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php
                            if(isset($extras) && is_array($extras) && count($extras)):  
                                $extra_price = (float) $extras['price'];
                               // $extra_price = $extra_price * $date_diff;
                                $extra_prices += $extra_price;    
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="item_booking_detail">
                                        <span class="label"><?php echo __('Supplément : ' , ST_TEXTDOMAIN); ?></span>
                                        <span class="value"><?php  echo esc_html($extras['title'] ) . ' (' . TravelHelper::format_money($extras['price'] ) . ') x ' . $date_diff . ' ' . __('nuit(s)', ST_TEXTDOMAIN); ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="item_booking_detail">
                                        <span class="label"><?php echo __('Prix Supp. : ' , ST_TEXTDOMAIN); ?></span>
                                        <span class="value"><?php echo TravelHelper::format_money($extra_price); ?></span>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="item_booking_detail">
                                        <span class="label"><?php echo __('Prix : ' , ST_TEXTDOMAIN); ?></span>
                                        <span class="value"><?php echo TravelHelper::format_money($results[$i]['sale_price']); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endfor; // end for ?>
                    </div>
				<?php if(!empty($discount = get_post_meta($order_id , 'discount_rate' , true))) {?>
                        <div class="col-md-12">
                            <div class="item_booking_detail">
                                <strong><?php esc_html_e("Promotion :",ST_TEXTDOMAIN) ?> </strong>
                                <?php echo esc_html($discount); ?> %
                            </div>
                        </div>
                    <?php } ?>
                    <?php echo st()->load_template('user/detail-booking-history/detail-price',false,
                        array(
                            'order_data'    => $order_data,
                            'order_id'      => $order_id,
                            'service_id'    => $service_id,
                        )
                    ) ?>
                </div>
            </div>
        </div>
        <div id="tab-customer-detail" class="tab-pane fade">
            <div class="container-customer">
                <?php echo apply_filters( 'st_customer_info_booking_history', st()->load_template('user/detail-booking-history/customer',false,array("order_id"=>$order_id)),$order_id ); ?>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <?php do_action("st_after_body_order_information_table",$order_data['order_item_id']); ?>
    <button data-dismiss="modal" class="btn btn-default" type="button"><?php esc_html_e("Close",ST_TEXTDOMAIN) ?></button>
</div>
