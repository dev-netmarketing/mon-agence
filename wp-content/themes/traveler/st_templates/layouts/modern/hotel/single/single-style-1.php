<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 16-11-2018
 * Time: 8:47 AM
 * Since: 1.0.0
 * Updated: 1.0.0
 */
while (have_posts()): the_post();
    
	
    $post_id = get_the_ID(); 
    $price = STHotel::get_hotel_price($post_id);
    $today        = strtotime( date( 'Y/m/d' ) );
    $discount      = STHotel::get_discount_by_period($post_id, $today);
    $msg_promotion = STHotel::get_msg_promotion($post_id, $today);
    $hotel_star = (int)get_post_meta($post_id, 'hotel_star', true);
    $address = get_post_meta($post_id, 'address', true);
    $review_rate = STReview::get_avg_rate();
    $count_review = get_comment_count($post_id)['approved'];
    $lat = get_post_meta($post_id, 'map_lat', true);
    $lng = get_post_meta($post_id, 'map_lng', true);
    $zoom = get_post_meta($post_id, 'map_zoom', true);

    $gallery = get_post_meta($post_id, 'gallery', true);
    $gallery_array = explode(',', $gallery);
    $marker_icon = st()->get_option('st_hotel_icon_map_marker', '');

    

    ?>
    <div id="st-content-wrapper">
        <?php st_breadcrumbs_new() ?>
        <div class="container">
            <div class="st-hotel-content">
                <div class="hotel-target-book-mobile">
                    
                    <div class="price-wrapper">
                        <?php  echo wp_kses(sprintf(__('from <span class="price">%s</span>', ST_TEXTDOMAIN), TravelHelper::format_money($price)), ['span' => ['class' => []]]) ?>
                    </div>
                    <a href=""
                       class="btn btn-mpopup btn-green"><?php echo esc_html__('Tarifs & Disponibilités', ST_TEXTDOMAIN) ?></a>
                </div>
            </div>
            <div class="st-hotel-header">
                <div class="left">
                    <?php echo st()->load_template('layouts/modern/common/star', '', ['étoiles' => $hotel_star]); ?>
                    <h2 class="st-heading"><?php the_title(); ?></h2>
                    <div class="sub-heading">
                        <?php if ($address) {
                            echo TravelHelper::getNewIcon('Ico_maps', '#5E6D77', '16px', '16px');
                            echo esc_html($address);
                        }
                        ?>
                        <a href="" class="st-link font-medium" data-toggle="modal"
                           data-target="#st-modal-show-map"> <?php echo esc_html__('Voir sur la carte', ST_TEXTDOMAIN) ?></a>
                        <div class="modal fade modal-map" id="st-modal-show-map" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <?php echo TravelHelper::getNewIcon('Ico_close'); ?></button>
                                        <h4 class="modal-title"><?php the_title(); ?></h4>
                                    </div>
                                    <div class="modal-body">
                                    <?php
                                    $default = array(
                                        'number'      => '12' ,
                                        'range'       => '20' ,
                                        'show_circle' => 'no' ,
                                    );
                                    extract($default);
                                    $hotel = new STHotel();
                                    $location_center  = '[' . $lat . ',' . $lng . ']';
                                    $map_lat_center = $lat;
                                    $map_lng_center = $lng;
                                    $map_icon = st()->get_option('st_hotel_icon_map_marker', '');
                                    $map_icon = get_template_directory_uri() . '/v2/images/markers/ico_mapker_hotel.png';
                                    if (empty($map_icon)){
                                        $map_icon = get_template_directory_uri() . '/v2/images/markers/ico_mapker_hotel.png';
                                    }

                                    $data_map = array();
                                    $stt  =  1;
                                    global $post;
                                    $properties = $hotel->properties_near_by(get_the_ID(), $lat, $lng, $range);
                                    if( !empty($properties) ){
                                        foreach($properties as $key => $val){
                                            $data_map[] = array(
                                                'id' => get_the_ID(),
                                                'name' => $val['name'],
                                                'post_type' => 'st_hotel',
                                                'lat' => (float)$val['lat'],
                                                'lng' => (float)$val['lng'],
                                                'icon_mk' => (empty($val['icon']))? 'http://maps.google.com/mapfiles/marker_black.png': $val['icon'],
                                                'content_html' => preg_replace('/^\s+|\n|\r|\s+$/m', '', st()->load_template('layouts/modern/hotel/elements/property',false,['data' => $val])),

                                            );
                                        }
                                    }

                                    $data_map_origin = array();
                                    $data_map_origin = array(
                                        'id' => $post_id,
                                        'post_id' => $post_id,
                                        'name' => get_the_title(),
                                        'description' => "",
                                        'lat' => $lat,
                                        'lng' => $lng,
                                        'icon_mk' => $map_icon,
                                        'featured' => get_the_post_thumbnail_url($post_id),
                                    );
                                    $data_map[] = array(
                                        'id' => $post_id,
                                        'name' => get_the_title(),
                                        'post_type' => 'st_hotel',
                                        'lat' => $lat,
                                        'lng' => $lng,
                                        'icon_mk' => $map_icon,
                                        'content_html' => preg_replace('/^\s+|\n|\r|\s+$/m', '', st()->load_template('layouts/modern/hotel/elements/property',false,['data' => $data_map_origin])),

                                    );

                                    $data_map       = json_encode( $data_map , JSON_FORCE_OBJECT );
                                    ?>
                                    <?php $google_api_key = st()->get_option('st_googlemap_enabled');
                                    if ($google_api_key === 'on') { ?>
                                        <div class="st-map mt30">
                                            <div class="google-map gmap3" id="list_map"
                                                data-data_show='<?php echo str_ireplace(array("'"),'\"',$data_map) ;?>'
                                                data-lat="<?php echo trim($lat) ?>"
                                                data-lng="<?php echo trim($lng) ?>"
                                                data-icon="<?php echo esc_url($marker_icon); ?>"
                                                data-zoom="<?php echo (int)$zoom; ?>" data-disablecontrol="true"
                                                data-showcustomcontrol="true"
                                                data-style="normal">
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="st-map-box mt30">
                                            <div class="google-map-mapbox" data-lat="<?php echo trim($lat) ?>"
                                                 data-lng="<?php echo trim($lng) ?>"
                                                 data-icon="<?php echo esc_url($marker_icon); ?>"
                                                 data-zoom="<?php echo (int)$zoom; ?>" data-disablecontrol="true"
                                                 data-showcustomcontrol="true"
                                                 data-style="normal">
                                                <div id="st-map">
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    </div>
                                    <script type="text/javascript">

                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-9">
                    <?php
                    if (!empty($gallery_array)) { ?>
                    <?php if($msg_promotion) { ?>
                        <div class="service-tag bestseller msg_promotion">
                            <div class="feature_class st_featured featured"><?php echo $msg_promotion; ?></div>
                        </div>
                    <?php } // end msg_promotion ?>
                        <div class="st-gallery" data-width="100%"
                             data-nav="thumbs" data-allowfullscreen="true">
                            <div class="fotorama" data-auto="false">
                                <?php
                                foreach ($gallery_array as $value) {
                                    ?>
                                    <img src="<?php echo wp_get_attachment_image_url($value, [870, 555]) ?>">
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="shares dropdown">
                                <?php
                                $video_url = get_post_meta(get_the_ID(), 'video', true);
                                if (!empty($video_url)) {
                                    ?>
                                    <a href="<?php echo esc_url($video_url); ?>"
                                       class="st-video-popup share-item"><?php echo TravelHelper::getNewIcon('video-player', '#FFFFFF', '20px', '20px') ?></a>
                                    <?php
                                } ?>
                                <a href="#" class="share-item social-share">
                                    <?php echo TravelHelper::getNewIcon('ico_share', '', '20px', '20px') ?>
                                </a>
                                <ul class="share-wrapper">
                                    <li><a class="facebook"
                                           href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink() ?>&amp;title=<?php the_title() ?>"
                                           target="_blank" rel="noopener" original-title="Facebook"><i
                                                    class="fa fa-facebook fa-lg"></i></a></li>
                                    <li><a class="twitter"
                                           href="https://twitter.com/share?url=<?php the_permalink() ?>&amp;title=<?php the_title() ?>"
                                           target="_blank" rel="noopener" original-title="Twitter"><i
                                                    class="fa fa-twitter fa-lg"></i></a></li>
                                    <li><a class="google"
                                           href="https://plus.google.com/share?url=<?php the_permalink() ?>&amp;title=<?php the_title() ?>"
                                           target="_blank" rel="noopener" original-title="Google+"><i
                                                    class="fa fa-google-plus fa-lg"></i></a></li>
                                    <li><a class="no-open pinterest"
                                           href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','https://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"
                                           target="_blank" rel="noopener" original-title="Pinterest"><i
                                                    class="fa fa-pinterest fa-lg"></i></a></li>
                                    <li><a class="linkedin"
                                           href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink() ?>&amp;title=<?php the_title() ?>"
                                           target="_blank" rel="noopener" original-title="LinkedIn"><i
                                                    class="fa fa-linkedin fa-lg"></i></a></li>
                                </ul>
                                <?php echo st()->load_template('layouts/modern/hotel/loop/wishlist'); ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
					 


					 <form id="single-room-form" class="form single-room-form hotel-room-booking-form" method="post">
					 	
				 <input name="action" value="hotel_add_to_cart" type="hidden">
				<input name="item_id" value="<?php echo esc_attr($post_id); ?>" type="hidden">
                    <div class="st-list-rooms relative stoped-scroll-section" data-toggle-section="st-list-rooms">
                        <?php echo st()->load_template('layouts/modern/common/loader'); ?>
                        <div class="fetch">
                        <?php
						$result =  STInput::request('result', 0);
						$room_num = 1;
						if ($result){
						$room_num = $result['room_num_search'];
						 
						}
												
 						for ($i = 1; $i <= $room_num; $i++) { 
                                        $resultRoom = array();						
                                        $max_size = $result['adult_number'.$i] +  $result['child_number'.$i];
                                        global $varmaxsize;
                                        $varmaxsize=$max_size; 
                                        $adult_number = 'adult_number'.$i;
                                        $child_number = 'child_number'.$i;
                                        $resultRoom['room_num_search']= $result['room_num_search'];
                                        $resultRoom[$adult_number] = $result['adult_number'.$i];
                                        $resultRoom[$child_number] = $result['child_number'.$i];

                                        if($result['child_number'.$i]){
                                            for ($j = 1; $j <= $result['child_number'.$i]; $j++) {
                                                $age_child = 'age_number_'.$i.'_'.$j;
                                                $resultRoom[$age_child] = $result['age_number_'.$i.'_'.$j]; 
                                            }	
                                        }
                                        
                                    global $post;
                                    $hotel = new STHotel();
                                    $query = $hotel->search_room();
            
                                        while ($query->have_posts()) {  
                                            $query->the_post();
                                                    $data = array(
                                                    'post_id' => $post_id,
                                                    'result'=> $resultRoom,
                                                    'i' => $i,
                                                    'num' => $room_num
            
                                                );
                                            
                                            echo st()->load_template('layouts/modern/hotel/loop/room_item',false,['data' => $data]);

                                        }  
                                    wp_reset_postdata();
		 	            }
                            ?>
                        </div>
                    </div>
				<div class="submit-group">
				<button class="btn btn-green btn-large btn-full upper font-medium btn_hotel_booking btn-book-ajax"
				type="submit"
				name="submit"
				value="">
				<?php echo __( 'Réserver', ST_TEXTDOMAIN ) ?>
				<i class="fa fa-spinner fa-spin hide"></i>
				</button>
				</div>
				<div class="mt30 message-wrapper">
				<?php echo STTemplate::message() ?>
				</div>
				</form>			
					
                    <div class="st-hr"></div>
                    <h2 class="st-heading-section"><?php echo esc_html__('Description', ST_TEXTDOMAIN) ?>
                        <a href="#" class="pull-right toggle-section"
                           data-target="st-description">
                            <i class="fa fa-angle-up"></i>
                        </a>
                    </h2>
                    <?php
                    global $post;
                    $content = $post->post_content;
                    $count = str_word_count($content);
                    ?>
                    <div class="st-description" data-toggle-section="st-description" <?php if ($count >= 120) {
                        echo 'data-show-all="st-description"
                             data-height="120"';
                    } ?>>
                        <?php the_content(); ?>
                        <?php if ($count >= 120) { ?>
                            <div class="cut-gradient"></div>
                        <?php } ?>
                    </div>
                    <?php if ($count >= 120) { ?>
                        <a href="#" class="st-link block" data-show-target="st-description"
                           data-text-less="<?php echo esc_html__('Voir moins', ST_TEXTDOMAIN) ?>"
                           data-text-more="<?php echo esc_html__('Voir plus', ST_TEXTDOMAIN) ?>"><span
                                    class="text"><?php echo esc_html__('Voir plus', ST_TEXTDOMAIN) ?></span><i
                                    class="fa fa-caret-down ml3"></i></a>
                    <?php } ?>
                    <div class="st-hr large"></div>
                    <h2 class="st-heading-section"><?php echo esc_html__('Installations', ST_TEXTDOMAIN) ?>
                        <a href="#" class="pull-right toggle-section"
                           data-target="st-facilities">
                            <i class="fa fa-angle-up"></i>
                        </a>
                    </h2>
                    <?php
                    $facilities = get_the_terms($post_id, 'hotel_facilities');
                    if ($facilities) {
                        $count = count($facilities);
                        ?>
                        <div class="facilities" data-toggle-section="st-facilities"
                            <?php if ($count > 6) echo 'data-show-all="st-facilities"
                                     data-height="150"'; ?>>
                            <div class="row">
                                <?php
                                if (isset($facilities) && is_array($facilities)) {
                                    foreach ($facilities as $term) {
                                        $icon = TravelHelper::handle_icon(get_tax_meta($term->term_id, 'st_icon', true));
                                        $icon_new = TravelHelper::handle_icon(get_tax_meta($term->term_id, 'st_icon_new', true));
                                        if (!$icon) $icon = "fa fa-cogs";
                                        ?>
                                        <div class="col-xs-6 col-sm-4">
                                            <div class="item has-matchHeight">
                                                <?php
                                                if (!$icon_new) {
                                                    echo '<i class="' . esc_attr($icon) . '"></i>' . esc_html($term->name);
                                                } else {
                                                    echo TravelHelper::getNewIcon($icon_new, '#5E6D77', '24px', '24px') . esc_html($term->name);
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    <?php }
                                }

                                ?>
                            </div>
                        </div>
                        <?php if ($count > 6) { ?>
                            <a href="#" class="st-link block" data-show-target="st-facilities"
                               data-text-less="<?php echo esc_html__('Voir moins', ST_TEXTDOMAIN) ?>"
                               data-text-more="<?php echo esc_html__('Tout montrer', ST_TEXTDOMAIN) ?>"><span
                                        class="text"><?php echo esc_html__('Tout montrer', ST_TEXTDOMAIN) ?></span>
                                <i class="fa fa-caret-down ml3"></i></a>
                            <?php
                        }
                    }
                    ?>
                    <div class="st-hr large"></div>
                    <h2 class="st-heading-section"><?php echo esc_html__('Régles', ST_TEXTDOMAIN) ?>
                        <a href="#" class="pull-right toggle-section"
                           data-target="st-properties"><i
                                    class="fa fa-angle-up"></i>
                        </a>
                    </h2>
                    <table class="table st-properties" data-toggle-section="st-properties">
                        <tr>
                            <th><?php echo esc_html__('Check In', ST_TEXTDOMAIN) ?></th>
                            <td>
                                <?php echo get_post_meta($post_id, 'check_in_time', true); ?>
                            </td>
                        </tr>
                        <tr>
                            <th><?php echo esc_html__('Check Out', ST_TEXTDOMAIN) ?></th>
                            <td>
                                <?php echo get_post_meta($post_id, 'check_out_time', true); ?>
                            </td>
                        </tr>
                        <?php
                        $policies = get_post_meta($post_id, 'hotel_policy', true);
                        if ($policies) {
                            ?>
                            <tr>
                                <th><?php echo esc_html__('Politiques de l\'hôtel', ST_TEXTDOMAIN) ?></th>
                                <td>
                                    <div data-show-all="st-policies"
                                         data-height="100">
                                        <?php
                                        foreach ($policies as $policy) {
                                            ?>
                                            <h4 class="f18"><?php echo esc_html($policy['title']); ?></h4>
                                            <div><?php echo balanceTags($policy['policy_description']) ?></div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <a href="#" class="st-link block mt20 " data-show-target="st-policies"
                                       data-text-less="<?php echo esc_html__('Voir moins', ST_TEXTDOMAIN) ?>"
                                       data-text-more="<?php echo esc_html__('Montrer tout', ST_TEXTDOMAIN) ?>"><span
                                                class="text"><?php echo esc_html__('Tout montrer', ST_TEXTDOMAIN) ?></span>
                                        <i class="fa fa-caret-down ml3"></i></a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
 
                    <?php 
                  
                    //if (comments_open() and st()->get_option('Avis hôtel') == 'on') { ?>
                        <div class="st-hr large"></div>
                        <h2 class="st-heading-section"><?php echo esc_html__('Reviews', ST_TEXTDOMAIN) ?>
                            <a href="#" class="pull-right toggle-section" data-target="st-reviews">
                                <i class="fa fa-angle-up"></i></a></h2>
                        <div id="reviews" data-toggle-section="st-reviews">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <div class="review-box has-matchHeight">
                                        <h2 class="heading"><?php echo __('Avis', ST_TEXTDOMAIN) ?></h2>
                                        <div class="review-box-score">
                                            <?php
                                            $avg = STReview::get_avg_rate();
                                            ?>
                                            <div class="review-score">
                                                <?php echo esc_attr($avg); ?><span class="per-total">/5</span>
                                            </div>
                                            <div class="review-score-text"><?php echo TravelHelper::get_rate_review_text($avg, $count_review); ?></div>
                                            <div class="review-score-base">
                                                <?php echo __('Basé sur', ST_TEXTDOMAIN) ?>
                                                <span><?php comments_number(__('0 avis', ST_TEXTDOMAIN), __('1 avis', ST_TEXTDOMAIN), __('% avis', ST_TEXTDOMAIN)); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <div class="review-box has-matchHeight">
                                        <h2 class="heading"><?php echo __('Traveler rating', ST_TEXTDOMAIN) ?></h2>
                                        <?php $total = get_comments_number(); ?>
                                        <?php $rate_exe = STReview::count_review_by_rate(null, 5); ?>
                                        <div class="item">
                                            <div class="progress">
                                                <div class="percent green"
                                                     style="width: <?php echo TravelHelper::cal_rate($rate_exe, $total) ?>%;"></div>
                                            </div>
                                            <div class="label">
                                                <?php echo esc_html__('Excellent', ST_TEXTDOMAIN) ?>
                                                <div class="number"><?php echo esc_html($rate_exe); ?></div>
                                            </div>
                                        </div>
                                        <?php $rate_good = STReview::count_review_by_rate(null, 4); ?>
                                        <div class="item">
                                            <div class="progress">
                                                <div class="percent darkgreen"
                                                     style="width: <?php echo TravelHelper::cal_rate($rate_good, $total) ?>%;"></div>
                                            </div>
                                            <div class="label">
                                                <?php echo __('Very Good', ST_TEXTDOMAIN) ?>
                                                <div class="number"><?php echo esc_html($rate_good); ?></div>
                                            </div>
                                        </div>
                                        <?php $rate_avg = STReview::count_review_by_rate(null, 3); ?>
                                        <div class="item">
                                            <div class="progress">
                                                <div class="percent yellow"
                                                     style="width: <?php echo TravelHelper::cal_rate($rate_avg, $total) ?>%;"></div>
                                            </div>
                                            <div class="label">
                                                <?php echo __('Average', ST_TEXTDOMAIN) ?>
                                                <div class="number"><?php echo esc_html($rate_avg); ?></div>
                                            </div>
                                        </div>
                                        <?php $rate_poor = STReview::count_review_by_rate(null, 2); ?>
                                        <div class="item">
                                            <div class="progress">
                                                <div class="percent orange"
                                                     style="width: <?php echo TravelHelper::cal_rate($rate_poor, $total) ?>%;"></div>
                                            </div>
                                            <div class="label">
                                                <?php echo __('Poor', ST_TEXTDOMAIN) ?>
                                                <div class="number"><?php echo esc_html($rate_poor); ?></div>
                                            </div>
                                        </div>
                                        <?php $rate_terible = STReview::count_review_by_rate(null, 1); ?>
                                        <div class="item">
                                            <div class="progress">
                                                <div class="percent red"
                                                     style="width: <?php echo TravelHelper::cal_rate($rate_terible, $total) ?>%;"></div>
                                            </div>
                                            <div class="label">
                                                <?php echo __('Terrible', ST_TEXTDOMAIN) ?>
                                                <div class="number"><?php echo esc_html($rate_terible); ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <div class="review-box has-matchHeight">
                                        <h2 class="heading"><?php echo __('Summary', ST_TEXTDOMAIN) ?></h2>
                                        <?php
                                        $stats = STReview::get_review_summary();
                                        if ($stats) {
                                            foreach ($stats as $stat) {
                                                ?>
                                                <div class="item">
                                                    <div class="progress">
                                                        <div class="percent"
                                                             style="width: <?php echo esc_attr($stat['percent']); ?>%;"></div>
                                                    </div>
                                                    <div class="label">
                                                        <?php echo esc_html($stat['name']); ?>
                                                        <div class="number"><?php echo esc_html($stat['summary']) ?>
                                                             
                                                        </div>
                                                    </div>
                                                </div>
                                           <?php }
                                       // }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="review-pagination">
                                <div class="summary">
                                    <?php
                                    $comments_count = wp_count_comments(get_the_ID());
                                    $total = (int)$comments_count->approved;
                                    $comment_per_page = (int)get_option('comments_per_page', 10);
                                    $paged = (int)STInput::get('comment_page', 1);
                                    $from = $comment_per_page * ($paged - 1) + 1;
                                    $to = ($paged * $comment_per_page < $total) ? ($paged * $comment_per_page) : $total;
                                    ?>
                                    <?php comments_number(__('0 review on this Hotel', ST_TEXTDOMAIN), __('1 review on this Hotel', ST_TEXTDOMAIN), __('% reviews on this Hotel', ST_TEXTDOMAIN)); ?>
                                    - <?php echo sprintf(__('Showing %s to %s', ST_TEXTDOMAIN), $from, $to) ?>
                                </div>
                                <div id="reviews" class="review-list">
                                    <?php
                                    $offset = ($paged - 1) * $comment_per_page;
                                    $args = [
                                        'number' => $comment_per_page,
                                        'offset' => $offset,
                                        'post_id' => get_the_ID(),
                                        'status' => ['approve']
                                    ];
                                    $comments_query = new WP_Comment_Query;
                                    $comments = $comments_query->query($args);

                                    if ($comments):
                                        foreach ($comments as $key => $comment):
                                            echo st()->load_template('layouts/modern/common/reviews/review', 'list', ['comment' => (object)$comment]);
                                        endforeach;
                                    endif;
                                    ?>
                                </div>
                            </div>
                            <?php TravelHelper::pagination_comment(['total' => $total]) ?>
                            <?php

                                $check_hotel=false;
                                // Get orders by customer with ID and status and date completed.
                                $args = array(
                                    'customer_id' => get_current_user_id(),
                                    'status' => "wc-completed",
                                    'date_completed' => '<' . ( time() - MINUTE_IN_SECONDS ),
                                );
                                $orders = wc_get_orders( $args );

                                    foreach($orders as $or){
                                        $current_data=$or->get_items();
                                        foreach($current_data as $item)
                                        { 
                                    if($item->get_name() == get_the_title())
                                    
                                    {
                                        $check_hotel=true;
                                        break; 
                                    }
                                }
                                }
                            if (comments_open($post_id) && is_user_logged_in() && $check_hotel) {
                                


                                ?>
                                <div id="write-review">
                                    <h4 class="heading">
                                        <a href="" class="toggle-section c-main f16"
                                           data-target="st-review-form"><?php echo __('Write a review', ST_TEXTDOMAIN) ?>
                                            <i class="fa fa-angle-down ml5"></i></a>
                                    </h4>
                                    <?php
                                    TravelHelper::comment_form();
                                    ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="widgets">
                        <div class="fixed-on-mobile" data-screen="992px">
                            <div class="close-icon hide">
                                <?php echo TravelHelper::getNewIcon('Ico_close'); ?>
                            </div>
                            
                            <div class="form-book-wrapper relative">
                            
                            <?php if($discount): ?>
                                <div class="tour-sale-box">
                                        <span class="st_sale_class box_sale sale_small"><?php echo $discount."%"; ?></span>
                                </div>
                            <?php endif; ?>
                                <div class="form-head">
                                    
                                     <?php if($discount): ?>
                                            <div class="final-price">À partir de
                                            <?php echo wp_kses(sprintf(__(' <span class="price">%s</span> <span class="unit"> /nuit</span>', ST_TEXTDOMAIN), TravelHelper::format_money(STHotel::discount_calculation($price, $discount))), ['span' => ['class' => []]]) ?>
                                            </div>
                                            <div class="old-price">Au lieu de <span class="text-small lh1em item onsale "><?php echo TravelHelper::format_money($price); ?></span></div>
                                    <?php else: ?>
                                            <div class="final-price">À partir de
                                            <?php echo wp_kses(sprintf(__(' <span class="price">%s</span> <span class="unit"> /nuit</span>', ST_TEXTDOMAIN), TravelHelper::format_money($price)), ['span' => ['class' => []]]) ?>
                                            </div>
                                    <?php endif; ?>
                                </div>
                                <nav>
                                    <ul class="nav nav-tabs nav-fill-st" id="nav-tab" role="tablist">
                                        <li class="active"><a id="nav-book-tab" data-toggle="tab" href="#nav-book"
                                                              role="tab" aria-controls="nav-home"
                                                              aria-selected="true"><?php echo esc_html__('Recherche', ST_TEXTDOMAIN) ?></a>
                                        </li>
                                        <li><a id="nav-inquirement-tab" data-toggle="tab" href="#nav-inquirement"
                                               role="tab" aria-controls="nav-profile"
                                               aria-selected="false"><?php echo esc_html__('Contact', ST_TEXTDOMAIN) ?></a>
                                        </li>
                                    </ul>
                                </nav>
                                <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                                    <div class="tab-pane fade in active" id="nav-book" role="tabpanel"
                                         aria-labelledby="nav-book-tab">
                                        <?php echo st()->load_template('layouts/modern/common/loader'); ?>
                                        <form class="form form-check-availability-hotel">
                                            <input type="hidden" name="action" value="ajax_search_room">
                                            <input type="hidden" name="room_search" value="1">
                                            <input type="hidden" name="is_search_room" value="1">
                                            <input type="hidden" name="room_parent" value="<?php echo esc_attr(get_the_ID()); ?>">
                                            <?php echo st()->load_template('layouts/modern/hotel/elements/search/date-enquire', ''); ?>
                                            <?php echo st()->load_template('layouts/modern/hotel/elements/search/guest-chambre'); ?>
                                            <?php echo st()->load_template('layouts/modern/hotel/elements/search/guest', ''); ?>
											 
                                            <div class="submit-group">
                                                <input class="btn btn-green btn-large btn-full upper"
                                                       type="submit"
                                                       name="submit"
                                                       value="<?php echo esc_html__('Tarifs & Disponibilités', ST_TEXTDOMAIN) ?>">
                                                <input style="display:none;" type="submit"
                                                       class="btn btn-default btn-send-message"
                                                       data-id="<?php echo get_the_ID(); ?>" name="st_send_message"
                                                       value="<?php echo __('Envoyer', ST_TEXTDOMAIN); ?>">
                                            </div>
			
                                            <div class="message-wrapper mt30"></div>
                                        </form>
                                    </div>

                                    <div class="tab-pane fade " id="nav-inquirement" role="tabpanel"
                                         aria-labelledby="nav-inquirement-tab">
                                        <?php echo st()->load_template('email/email_single_service'); ?>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php endwhile;