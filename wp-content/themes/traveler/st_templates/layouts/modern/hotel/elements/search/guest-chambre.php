<div class="form-group form-extra-field dropdown clearfix field-guest has-icon search_right_form">
<?php
$result_page = st()->get_option( 'hotel_search_result_page' );
    $has_icon = ( isset( $has_icon ) ) ? $has_icon : false;
    if($has_icon)
        echo TravelHelper::getNewIcon( 'ico_guest_search_box' );
    ?> 
    <ul class="guest-menu">
        <li class="item search-room-item">
            <label><?php echo esc_html__( 'Chambre(s)', ST_TEXTDOMAIN ) ?></label>
            <div class="select-wrapper" style="width: 50px;">
            <select name="room_num_search" class="form-control app" onchange="afficher_masquer_select_ch(this.value)" id="select_ch">
            <?php	for ($countch = 1; $countch <= 5; $countch++) { ?>
            <option value="<?php echo esc_attr($countch); ?>" <?php if($result["room_num_search"] == $countch){echo'selected';} ?>><?php echo esc_attr($countch); ?></option>
            <?php } ?>
        </select> 
        </div>
        </li>
    </ul>
</div>